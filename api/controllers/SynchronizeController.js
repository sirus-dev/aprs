/**
 * SynchronizeController
 *
 * @description :: Server-side logic for managing synchronizes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function (req, res) {
    Synchronize
      .find()
      .then(function (founds) {
        return res.json(founds);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Synchronize@find#1].");
      });
  },

  create: function (req, res) {
    // Assign values
    var crypto = require('crypto');

    var values = {
             domain: 'http://'+req.param('domain'),
             key: crypto.randomBytes(Math.ceil(24/2))
        					.toString('hex') // convert to hexadecimal format
        					.slice(0,24)
    };
    // Assign validate
    var validate = ['', 'null', 'undefined', null, undefined];
    // Validate `name`
    if (validate.indexOf(values.domain) > -1) {
      return res.badRequest("Oops! Nama Domain harus diisi.");
    }
    // Find clinic based on name of the provided values to make sure that the clinic didn't exist
    Synchronize
      .findOne({
        domain: values.domain
      })
      .then(function (found) {
        if (found) {
          // Send a 400 response back down to the client indicating that the request is failed
          return res.badRequest("Oops! Nama Domain sudah ada.");
        }
        // Save Synchronize with the provided values
    		Synchronize
          .create(values)
          .then(function (created) {
    				if (!created) {
    					// Send a 400 response back down to the client indicating that the request is failed
    					return res.badRequest("Oops! Data gagal disimpan. Silahkan coba lagi.");
    				}
            // Send a 200 response back down to the client with the provided data
            return res.json(created);
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Synchronize@create#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Synchronize@create#1].");
      });
  }
};

