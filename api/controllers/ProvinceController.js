/**
 * ProvinceController
 *
 * @description :: Server-side logic for managing provinces
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	find: function (req, res) {
	  Province
      .find()
			.populateAll()
      .then(function (founds) {
        return res.json(founds);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Province@find#1].");
      });
	}

};
