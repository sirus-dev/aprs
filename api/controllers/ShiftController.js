/**
 * ShiftController
 *
 * @description :: Server-side logic for managing shifts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	/**
	 * ShiftController.find()
	 *
	 * --> GET shift
	 */
	find: function (req, res) {
		// Find all shift along with the doctor
		Shift
			.find()
			.populateAll()
			.then(function (shifts) {
				// Send a 200 response back down to the client with the provided data
				return res.json(shifts);
			})
			.catch(function (errno) {
				sails.log.error(errno);
				// Send an appropriate error response back down to the client
				return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@find#1].");
			});
	},

	notif: function (req, res) {
		// Find all shift along with the doctor
		Shift
			.find()
			.then(function (shifts) {
				// Send a 200 response back down to the client with the provided data
				return res.json(shifts);
			})
			.catch(function (errno) {
				sails.log.error(errno);
				// Send an appropriate error response back down to the client
				return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@find#1].");
			});
	},

	/**
	 * ShiftController.findOne()
	 *
	 * --> GET shift/:id
	 */
	findOne: function (req, res) {
		// Assign criteria
		var criteria = {
			id: req.param('id')
		};

		if (isNaN(criteria.id)) {
			criteria.id = 0;
		}
		// Find shift based on criteria along with doctor of the shift
		Shift
			.findOne(criteria)
			.populate('doctor')
			.then(function (shift) {
				if (!shift) {
					// Send a 200 response back down to the client
					return res.json(shift);
				}
				// Find nurse(s) shift(s)
				NurseShift
					.find({
						shift: shift.id
					})
					.populate('nurse')
					.then(function (nurses) {
						var clinic = [shift.doctor.clinic];

						if (nurses.length > 0) {
							clinic.push(_.pluck(_.pluck(nurses, 'nurse'), 'clinic'));
						}

						shift.nurse = _.pluck(_.pluck(nurses, 'nurse'));

						Clinic
							.find({
								id: _.flatten(_.union(clinic))
							})
							.then(function (clinics) {
								if (clinics.length > 0) {
									_.map(clinics, function (v) {
										if (shift.doctor.clinic === v.id) {
											shift.doctor.clinic = v;
										}
									});

									_.map(shift.nurse, function (v) {
										_.map(clinics, function (vv) {
											if (v.clinic === vv.id) {
												v.clinic = vv;
											}
										});
									});
								}

								if (shift.nurse.length > 0) {
									User
										.find({
											id: _.pluck(shift.nurse, 'user')
										})
										.then(function (users) {
											if (users.length > 0) {
												_.map(shift.nurse, function (v) {
													_.map(users, function (vv) {
														if (v.user === vv.id) {
															v.user = vv;
														}
													});
												});
											}

											return res.json(shift);
										})
										.catch(function (errno) {
											sails.log.error(errno);
											// Send an appropriate error response back down to the client
											return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@findOne#4].");
										});
								} else {
									return res.json(shift);
								}
							})
							.catch(function (errno) {
								sails.log.error(errno);
								// Send an appropriate error response back down to the client
								return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@findOne#3].");
							});
					})
					.catch(function (errno) {
						sails.log.error(errno);
						// Send an appropriate error response back down to the client
						return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@findOne#2].");
					});
			})
			.catch(function (errno) {
				sails.log.error(errno);
				// Send an appropriate error response back down to the client
				return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@findOne#1].");
			});
	},

	/**
   * ShiftController.doctor()
   *
   * --> POST /shift/doctor
   */
	 doctor: function (req, res) {
		var criteria = {
			id: req.param('id')
		};
		// Validate request method
		if (req.method.toUpperCase() !== 'POST') {
			return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
		}

		if (isNaN(criteria.id)) {
			criteria.id = 0;
		}

		Shift
			.findOne(criteria)
			.then(function (found) {
				if (!found) {
					return res.badRequest("Oops! Data shift dokter tidak ditemukan.");
				}

				Shift
					.destroy(criteria)
					.then(function (destroyed) {
						if (!destroyed) {
							return res.badRequest("Oops! Data shift dokter tidak dapat dihapus. Silahkan coba lagi.");
						}

						return res.json(destroyed);
					})
					.catch(function (errno) {
						sails.log.error(errno);
						// Send an appropriate error response back down to the client
						return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@doctor#2].");
					});
			})
			.catch(function (errno) {
				sails.log.error(errno);
				// Send an appropriate error response back down to the client
				return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@doctor#1].");
			});
	},

	/**
   * ShiftController.nurse()
   *
   * --> POST /shift/nurse
   */
	 nurse: function (req, res) {
		var criteria = {
			nurse: req.param('nurse'),
			shift: req.param('shift')
		};
		// Validate request method
		if (req.method.toUpperCase() !== 'POST') {
			return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
		}

		if (isNaN(criteria.nurse)) {
			criteria.nurse = 0;
		}

		if (isNaN(criteria.shift)) {
			criteria.shift = 0;
		}

		NurseShift
			.findOne(criteria)
			.then(function (found) {
				if (!found) {
					return res.badRequest("Oops! Data shift perawat tidak ditemukan.");
				}

				NurseShift
					.destroy(criteria)
					.then(function (destroyed) {
						if (!destroyed) {
							return res.badRequest("Oops! Data shift perawat tidak dapat dihapus. Silahkan coba lagi.");
						}

						return res.json(destroyed);
					})
					.catch(function (errno) {
						sails.log.error(errno);
						// Send an appropriate error response back down to the client
						return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@nurse#2].");
					});
			})
			.catch(function (errno) {
				sails.log.error(errno);
				// Send an appropriate error response back down to the client
				return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@nurse#1].");
			});
	},

	/**
   * ShiftController.search()
   *
   * --> POST /shift/search
   */
	search: function (req, res) {
		var moment = require('moment');

		var criteria = {
			      day: Number(req.param('day')),
			startHour: moment(req.param('startHour')).format('YYYY-MM-DD HH:mm:ss'),
			  endHour: moment(req.param('endHour')).format('YYYY-MM-DD HH:mm:ss')
		};

		// Validate request method
		if (req.method.toUpperCase() !== 'POST') {
			return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
		}

		if (isNaN(criteria.day)) {
			return res.badRequest("Oops! Hari tidak ditemukan.");
		}

		if (!moment(criteria.startHour).isValid()) {
			return res.badRequest("Oops! Jam mulai tidak ditemukan.");
		}

		if (!moment(criteria.endHour).isValid()) {
			return res.badRequest("Oops! Jam selesai tidak ditemukan.");
		}

		// Find shift based on criteria along with doctor of the shift
		Shift
			.find(criteria)
			.populate('doctor')
			.populate('nurses')
			.then(function (shift) {
				if (shift.length === 0) {
					// Send a 200 response back down to the client
					return res.json(shift);
				}

				var doctors = _.pluck(_.pluck(shift, 'doctor'), 'id'),
				     nurses = _.pluck(_.flatten(_.pluck(shift, 'nurses')), 'nurse'),
						  shift = {
								      day: shift[0].day,
								startHour: shift[0].startHour,
								  endHour: shift[0].endHour
							};

				if (doctors.length > 0) {
					Doctor
						.find(doctors)
						.populate('clinic')
						.then(function (doctors) {
							shift.doctors = doctors;

							if (nurses.length > 0) {
								Nurse
									.find(nurses)
									.populate('clinic')
									.populate('user')
									.then(function (nurses) {
										shift.nurses = nurses;

										return res.json(shift);
									})
									.catch(function (errno) {
										sails.log.error(errno);
										// Send an appropriate error response back down to the client
										return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@search#3].");
									});
							} else {
								return res.json(shift)
							}
						})
						.catch(function (errno) {
							sails.log.error(errno);
							// Send an appropriate error response back down to the client
							return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@search#2].");
						});
				} else {
					return res.json(shifts);
				}
			})
			.catch(function (errno) {
				sails.log.error(errno);
				// Send an appropriate error response back down to the client
				return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@search#1].");
			});
	}

};
