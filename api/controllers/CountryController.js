/**
 * CountryController
 *
 * @description :: Server-side logic for managing countries
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	find: function (req, res) {
	  Country
      .find()
      .then(function (founds) {
        return res.json(founds);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Country@find#1].");
      });
	}

};
