/**
 * NurseController
 *
 * @description :: Server-side logic for managing nurses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	/**
   * NurseController.find()
   *
   * --> GET /nurse
   */
  find: function (req, res) {
    Nurse
      .find()
      .populate('clinic')
      .populate('user')
      .then(function (nurses) {
        return res.json(nurses);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@find#1].");
      });
  },

  /**
   * NurseController.findOne()
   *
   * --> GET /nurse/:id
   */
  findOne: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Find doctor based on criteria along with clinic and shift(s) of the doctor
    Nurse
      .findOne(criteria)
      .populate('clinic')
      .populate('user')
      .then(function (nurse) {
        if (!nurse) {
          return res.json(nurse);
        }

        NurseShift
          .find({
            nurse: nurse.id
          })
          .populate('shift')
          .then(function (shifts) {
            if (shifts.length === 0) {
              return res.json(nurse);
            }

            Doctor
              .find({
                id: _.pluck(_.pluck(shifts, 'shift'), 'doctor')
              })
              .then(function (doctors) {
                if (doctors.length === 0) {
                  return res.json(nurse);
                }

                nurse.shift = _.pluck(shifts, 'shift');

                _.map(nurse.shift, function (v) {
                  _.map(doctors, function (vv) {
                    if (v.doctor === vv.id) {
                      v.doctor = vv;
                    }
                  });
                });

                return res.json(nurse);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@findOne#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@findOne#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@findOne#1].");
      });
  },

/*
Find clinic nurse

*/
  clinic: function (req, res) {

    Nurse
      .findOne({
          user: req.session.passport.user
        })
      .populate('clinic')
      .then(function (nurse) {
        if (!nurse) {
          return res.json(nurse);
        }
        return res.json(nurse);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@findOne#1].");
      });
  },

  /**
   * NurseController.create()
   *
   * --> POST /nurse
   */
  create: function (req, res) {
    var values = {
             email: req.param('email'),
              name: req.param('name'),
      isRegistered: true,
              role: 'nurse',
            clinic: req.param('clinic')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (validate.indexOf(values.email) > -1) {
      return res.badRequest("Oops! Email harus diisi.");
    }

    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }

    if (validate.indexOf(values.clinic) > -1) {
      return res.badRequest("Oops! Klinik harus diisi.");
    }

    User
      .findOne({
        email: values.email
      })
      .then(function (found) {
        if (found) {
          var roles = ['admin','nurse','patient','admin','perawat','pasien'];
          return res.badRequest("Oops! Email telah terdaftar sebagai " + roles[roles.indexOf(found.role)+3] + ".");
        }

        Config
          .find()
          .then(function (config) {
            if (config.length === 0) {
              return res.badRequest("Oops! Konfigurasi email tidak ditemukan.");
            }

            User
              .create({
                       email: values.email,
                        name: values.name,
                isRegistered: values.isRegistered,
                        role: values.role
              })
              .then(function (user) {
                if (!user) {
                  return res.badRequest("Oops! Data perawat tidak dapat disimpan. Silahkan coba lagi.");
                }

                Nurse
                  .create({
                      user: user.id,
                    clinic: values.clinic
                  })
                  .then(function (nurse) {
                    if (!nurse) {
                      return res.badRequest("Oops! Data perawat tidak dapat disimpan. Silahkan coba lagi.");
                    }

                    return res.json(nurse);
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@create#4].");
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@create#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@create#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@create#1].");
      });
	},

  /**
   * NurseController.update()
   *
   * --> PUT /nurse
   */
   update: function (req, res) {
     var criteria = {
         id: req.param('id'),
       role: 'nurse'
     };

     var values = {
         name: req.param('name'),
        email: req.param('email')
     };

     var validate = ['', 'null', 'undefined', null, undefined];

     if (isNaN(criteria.id)) {
       criteria.id = 0;
     }

     if (validate.indexOf(values.email) > -1) {
       return res.badRequest("Oops! Email harus diisi.");
     }

     if (validate.indexOf(values.name) > -1) {
       return res.badRequest("Oops! Nama harus diisi.");
     }

     if (validate.indexOf(req.param('clinic')) > -1) {
       return res.badRequest("Oops! Klinik harus diisi.");
     }

     User
      .findOne(criteria)
      .then(function (user) {
        if (!user) {
          return res.badRequest("Oops! Data perawat tidak ditemukan.");
        }

        User
          .update(criteria, values)
          .then(function (updated) {
            if (!updated) {
              return res.badRequest("Oops! Data perawat tidak dapat diperbarui. Silahkan coba lagi.");
            }

        Nurse
          .findOne({user : criteria.id})
          .populate('clinic')
          .then(function (nurs) {
          if (!nurs) {
            return res.badRequest("Oops! Data perawat tidak ditemukan.");
          }
            Nurse
              .update({
                user: user.id
              },{
                clinic: req.param('clinic')
              })
              .then(function (nurse) {
                if (!nurse) {
                  return res.badRequest("Oops! Data perawat tidak dapat diperbarui. Silahkan coba lagi.");
                }
                if (nurs.clinic.id.toString() !== req.param('clinic').toString())
                {
                  NurseShift
                    .destroy({nurse : nurs.id})
                    .then(function (destroyed) {
                      if (!destroyed) {
                        return res.badRequest("Oops! Data Shift perawat sebelumnya tidak dapat dihapus. Silahkan coba lagi.");
                      }
                      return res.json(destroyed);
                    })
                    .catch(function (errno) {
                      sails.log.error(errno);
                      return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@destroy#2].");
                    });
                }
                else{
                  return res.json(nurse);
                }
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@update#4].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@update#3].");
          });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@update#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@update#1].");
      });
   },

   /**
    * NurseController.destroy()
    *
    * --> DELETE /nurse/:id
    */
   destroy: function (req, res) {
     var criteria = {
       id: req.param('id')
     };

     if (isNaN(criteria.id)) {
       criteria.id = 0;
     }

     Nurse
      .findOne(criteria)
      .then(function (found) {
        if (!found) {
          return res.badRequest("Oops! Data perawat tidak ditemukan.");
        }

        Nurse
          .destroy(criteria)
          .then(function (destroyed) {
            if (!destroyed) {
              return res.badRequest("Oops! Data perawat tidak dapat dihapus. Silahkan coba lagi.");
            }

            return res.json(destroyed);
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@destroy#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@destroy#1].");
      });
   },

   /**
    * NurseController.shift()
    *
    * --> POST /nurse/shift
    */
   shift: function (req, res) {
     // Assign criteria
 		var criteria = {
 			id: req.param('nurse')
 		};
 		// Assign values
 		var values = {
      nurse: req.param('nurse'),
      shift: req.param('shift')
 		};

    var validate = ['', 'null', 'undefined', null, undefined];

    // Validate request method
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }

    if (validate.indexOf(values.nurse) > -1) {
      return res.badRequest("Oops! Perawat harus diisi.");
    }

    if (validate.indexOf(values.shift) > -1) {
      return res.badRequest("Oops! Shift harus diisi.");
    }
 		// Find nurse based on criteria
 		Nurse
 			.findOne(criteria)
 			.then(function (nurse) {
 				if (!nurse) {
 					// Send a 400 response back down to the client indicating that the request is invalid
 					return res.badRequest("Oops! Data perawat tidak ditemukan.");
 				}

        NurseShift
          .findOrCreate(values)
          .then(function (created) {
            if (!created) {
              // Send a 400 response back down to the client indicating that the request is failed
							return res.badRequest('Oops! Data shift perawat tidak dapat disimpan. Silahkan coba lagi.');
            }
            // Send a 200 response back down to the client
						return res.json(created);
          })
          .catch(function (errno) {
						sails.log.error(errno);
						// Send an appropriate error response back down to the client
						return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@shift#2].");
					});
 			})
 			.catch(function (errno) {
 				sails.log.error(errno);
 				// Send an appropriate error response back down to the client
 				return res.negotiate("Oops! Terjadi kesalahan sistem [Nurse@shift#1].");
 			});
   },

   /**
    * `ScheduleController.queue()`
    *
    * --> POST /nurse/queue
    */
    queue: function (req, res) {
      var moment = require('moment');

      if (!moment(req.param('date')).isValid()) {
        return res.badRequest("Oops! Tanggal tidak valid.");
      }
      // Validate request method
      if (req.method.toUpperCase() !== 'POST') {
        return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
      }
      Nurse
        .findOne({
          user: req.session.passport.user
        })
        .populate('clinic')
        .populate('shifts')
        .then(function (nurse) {
          if (!nurse) {
            return res.json(nurse);
          }

          Shift
            .find({
              id: _.pluck(nurse.shifts, 'shift')
            })
            .populate('doctor')
            .then(function (shifts) {
              if (shifts.length === 0) {
                return res.json(shifts);
              }

              Schedule
                .find({
                   date: moment(req.param('date')).format('YYYY-MM-DD'),
                  shift: _.pluck(shifts, 'id')
                })
                .populate('notification')
                .then(function (schedules) {
                  if (schedules.length === 0) {
                    return res.json(schedules);
                  }

                  Queue
                    .find({
                      schedule: _.pluck(schedules, 'id')
                    })
                    .populate('users')
                    .then(function (queues) {
                      if (queues.length === 0) {
                        return res.json(queues);
                      }

                      _.map(queues, function (vv) {
                        if (moment(vv.timeIn).isValid() && moment(vv.timeOut).isValid()) {
                          vv.duration = (moment.utc(moment(moment(vv.timeOut), 'DD/MM/YYYY HH:mm:ss').diff(moment(moment(vv.timeIn), 'DD/MM/YYYY HH:mm:ss'))).format('HH:mm:ss'));
                        }
                      });

                      nurse.shifts = null;

                      _.map(shifts, function (v) {
                        _.map(schedules, function (vv) {
                          if (v.id === vv.shift) {
                            v.schedule = vv;
                            nurse.shifts.push(v);
                          }
                        });
                      });

                      _.map(_.compact(nurse.shifts), function (v) {
                        _.map(queues, function (vv) {
                          if (v.schedule.id === vv.schedule) {
                            v.schedule.queues.push(vv);
                          }
                        });
                      });

                      return res.json(nurse);
                    })
                    .catch(function (errno) {
                      sails.log.error(errno);
                      return res.negotiate("Oops! Terjadi kesalahan sistem [Schedule@queue#4].");
                    });
                })
                .catch(function (errno) {
                  sails.log.error(errno);
                  return res.negotiate("Oops! Terjadi kesalahan sistem [Schedule@queue#3].");
                });
            })
            .catch(function (errno) {
              sails.log.error(errno);
              return res.negotiate("Oops! Terjadi kesalahan sistem [Schedule@queue#2].");
            });
        })
        .catch(function (errno) {
          sails.log.error(errno);
          return res.negotiate("Oops! Terjadi kesalahan sistem [Schedule@queue#1].");
        });
     }

};
