/**
 * HospitalController
 *
 * @description :: Server-side logic for managing hospitals
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  find: function (req, res) {
    Hospital
      .find()
      .then(function (founds) {
        return res.json(founds);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [HospitalDescription@find#1].");
      });
  },

  meta: function (req, res) {
    Hospital
      .find()
      .then(function (hospital) {
        return res.view('patient', {
          title : hospital[0].name,
           desc : hospital[0].description,
        favicon : hospital[0].favicon,
           logo : hospital[0].logo,
        address : hospital[0].address,
          email : hospital[0].email,
          phone : hospital[0].phone,
         banner : hospital[0].banner
        });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [HospitalDescription@find#1].");
      });
  },

  update: function (req, res) {

      var config = {
      maxBytes: sails.config.files.size,
       dirname: sails.config.files.path
      };

      var values = {
                  name: req.param('name'),
                  email: req.param('email'),
                  phone: req.param('phone'),
                  address: req.param('address'),
                  description: req.param('description'),
                  favicon: req.param('favicon'),
                  logo: req.param('logo'),
                  banner: req.param('banner')
      };

    Hospital
      .find()
      .then(function (hospital) {
           // Set timeout to unlimited
           res.setTimeout(0);
           // Start uploading file
           req.file('banner')
             .upload(config, function (err, banner) {
               if (err) {
                 // Send an appropriate error response back down to the client
                 sails.log.error(err);
                 return res.negotiate("Oops! Terjadi kesalahan sistem [Error_banner@update].");
               }

                   try {
                    if (hospital[0].banner === req.param('banner')){
                        values.banner = req.param('banner');                      
                    }else{
                      // Get filename of currently uploaded picture
                      values.banner = require('path').basename(banner[0].fd);
                     // Copy the file to the '.tmp' folder so that it becomes available immediately
                      require('fs').createReadStream(sails.config.files.path + values.banner)
                       .pipe(require('fs').createWriteStream(sails.config.files.temp + values.banner));
                    }
                   } catch (e) {
                     sails.log.warn(e);
                   } finally {
                        // Start uploading file
           req.file('logo')
             .upload(config, function (err, logo) {
               if (err) {
                 // Send an appropriate error response back down to the client
                 sails.log.error(err);
                 return res.negotiate("Oops! Terjadi kesalahan sistem [Error_Logo@update].");
               }

                   try {
                    if (hospital[0].logo === req.param('logo')){
                        values.logo = req.param('logo');                      
                    }else{
                      // Get filename of currently uploaded picture
                    values.logo = require('path').basename(logo[0].fd);
                     // Copy the file to the '.tmp' folder so that it becomes available immediately
                    require('fs').createReadStream(sails.config.files.path + values.logo)
                       .pipe(require('fs').createWriteStream(sails.config.files.temp + values.logo));
                    }
                   } catch (e) {
                     sails.log.warn(e);
                   } finally {

              // Start uploading file
           req.file('favicon')
             .upload(config, function (err, favicon) {
               if (err) {
                 // Send an appropriate error response back down to the client
                 sails.log.error(err);
                 return res.negotiate("Oops! Terjadi kesalahan sistem [Error_favicon@update].");
               }

                   try {
                    if (hospital[0].favicon === req.param('favicon')){
                        values.favicon = req.param('favicon');                      
                    }else{
                      // Get filename of currently uploaded picture
                    values.favicon = require('path').basename(favicon[0].fd);
                     // Copy the file to the '.tmp' folder so that it becomes available immediately
                     require('fs').createReadStream(sails.config.files.path + values.favicon)
                       .pipe(require('fs').createWriteStream(sails.config.files.temp + values.favicon));
                    }
                   } catch (e) {
                     sails.log.warn(e);
                   } finally {
            Hospital
              .update({id: req.param('id')},values)
              .then(function (updated) {
                if (!updated) {
                  // Send a 400 response back down to the client indicating that the request is failed
                  return res.badRequest('Oops! Data gagal diperbaharui. Silahkan coba lagi.');
                }
                // Send a 200 response back down to the client
                return res.json(updated);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Hospital@update#1].");
              });
              }
          });
              }
          });
              }
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [HospitalDescription@find#1].");
      });           
  }
};