/**
 * HelpController
 *
 * @description :: Server-side logic for managing helps
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

/**
   * HelpController.create()
   *
   * --> POST /help
   */
  create: function (req, res) {
    // Assign values

    User
      .findOne({
        id: req.session.passport.user
      })
      .then(function (user) {
        if (!user) {
          return res.json(user);
        }

        if (user.email === '-'){
          return res.badRequest("Oops! Silahkan lengkapi data email anda.");
        }
        var values = {
             title: req.param('title'),
           content: req.param('content'),
             email: user.email
        };

        Help
          .create(values)
          .then(function (created) {
    				if (!created) {
    					// Send a 400 response back down to the client indicating that the request is failed
    					return res.badRequest("Oops! Data gagal disimpan. Silahkan coba lagi.");
    				}
            // Send a 200 response back down to the client with the provided data
            return res.json(created);
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Help@create#2].");
          });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Find@User#1].");
          });
  },

  find: function (req, res) {
	  Help
      .find()
      .then(function (founds) {
        return res.json(founds);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Help@find#1].");
      });
	},

  answer: function (req, res) {
    Help
      .find({
        answer: { '!': null },
        status: 'tampil'
      })
      .then(function (founds) {
        return res.json(founds);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Help@find#1].");
      });
  },

findOne: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }

    Help
      .findOne(criteria)
      .then(function (help) {
        return res.json(help);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Help@findOne#1].");
      });
  },

  update: function (req, res) {
    var values = {
        title: req.param('title'),
      content: req.param('content'),
       answer: req.param('answer'),
       status: req.param('status')
    };

    Help
      .findOne({id: req.param('id')})
      .then(function (help){

    // Assign validate
    var validate = ['', 'null', 'undefined', null, undefined];
    // Validate id
    if (validate.indexOf(values.title) > -1) {
        values.title = help.title;
    }
    if (validate.indexOf(values.content) > -1) {
        values.content = help.content;
    }
    if (validate.indexOf(values.status) > -1) {
        values.status = help.status;
    }
            Help
              .update({id: req.param('id')}, values)
              .then(function (updated) {
                if (!updated) {
                  // Send a 400 response back down to the client indicating that the request is failed
                  return res.badRequest('Oops! Data gagal diperbaharui. Silahkan coba lagi.');
                }
                // Send a 200 response back down to the client
                return res.json(updated);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Help@update#3].");
              });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Help@find#3].");
              });
  },

  destroy: function (req, res) {
     var criteria = {
       id: req.param('id')
     };

     if (isNaN(criteria.id)) {
       criteria.id = 0;
     }
        Help
          .destroy(criteria)
          .then(function (destroyed) {
            if (!destroyed) {
              return res.badRequest("Oops! Data pesan tidak dapat dihapus. Silahkan coba lagi.");
            }

            return res.json(destroyed);
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Pesan@destroy#1].");
          });
   }

};

