/**
 * DoctorController
 *
 * @description :: Server-side logic for managing doctors
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * DoctorController.find()
   *
   * --> GET /doctor
   */
  find: function (req, res) {
    // Find all doctor along with clinic and shift(s) of the doctor
    Doctor
      .find()
      .populate('clinic')
      .then(function (doctors) {
        // Send a 200 response back down to the client with the provided data
        return res.json(doctors);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@find#1].");
      });
  },

  /**
   * DoctorController.findOne()
   *
   * --> GET /doctor/:id
   */
  findOne: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Find doctor based on criteria along with clinic and shift(s) of the doctor
    Doctor
      .findOne(criteria)
      .populate('clinic')
      .populate('shifts')
      .then(function (doctor) {
        return res.json(doctor);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@findOne#1].");
      });
  },

  /**
   * DoctorController.create()
   *
   * --> POST /doctor
   */
  create: function (req, res) {
    // Assign config
    var config = {
      maxBytes: sails.config.file.size,
       dirname: sails.config.file.path
    };
    // Assign values
    var values = {
        title: req.param('title'),
         name: req.param('name'),
         desc: req.param('desc'),
       clinic: req.param('clinic'),
      picture: req.param('picture')
    };
    // Assign validate
    var validate = ['', null, 'undefined'];
    // Validate title
    if (validate.indexOf(values.title) > -1) {
      return res.badRequest("Oops! Gelar harus diisi.");
    }
    // Validate name
    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }
    // Validate clinic
    if (validate.indexOf(values.clinic) > -1) {
      return res.badRequest("Oops! Klinik harus diisi.");
    }
    // Validate picture
    if (validate.indexOf(values.picture) > -1) {
      return res.badRequest("Oops! Foto harus diisi.");
    }
    // Find doctor based on title & name of the provided values to make sure that the doctor didn't exist
    Doctor
     .findOne({
       title: values.title,
        name: values.name
     })
     .then(function (found) {
       if (found) {
         // Send a 400 response back down to the client indicating that the request is failed
         return res.badRequest("Oops! Nama dokter sudah ada.");
       }
       // Set timeout to unlimited
       res.setTimeout(0);
       // Start uploading file
       req.file('picture')
         .upload(config, function (err, uploaded) {
           if (err) {
             sails.log.error(err);
             // Send an appropriate error response back down to the client
             return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@create#2].");
           }
           // If no files were uploaded, respond with an error.
           if (uploaded.length === 0) {
             return res.badRequest("Oops! Foto tidak dapat di unggah. Silahkan coba lagi.");
           }
           // Get filename of currently uploaded picture
           values.picture = require('path').basename(uploaded[0].fd);
            // Copy the file to the '.tmp' folder so that it becomes available immediately
            try {
              require('fs').createReadStream(sails.config.file.path + values.picture)
                .pipe(require('fs').createWriteStream(sails.config.file.temp + values.picture));
            } catch (e) {
              sails.log.warn(e);
            } finally {
               // Save doctor with the provided values
               Doctor
                 .create(values)
                 .then(function (created) {
       						if (!created) {
                     // Send a 400 response back down to the client indicating that the request is failed
       							return res.badRequest("Oops! Data tidak dapat disimpan. Silahkan coba lagi.");
       						}
                   // Send a 200 response back down to the client with the provided data
                   return res.json(created);
                 })
                 .catch(function (errno) {
                   sails.log.error(errno);
                   // Send an appropriate error response back down to the client
                   return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@create#3].");
                 });
            }
         });
     })
     .catch(function (errno) {
       sails.log.error(errno);
       // Send an appropriate error response back down to the client
       return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@create#1].");
     });
  },

	/**
	 * DoctorController.update()
   *
   * --> PUT /doctor/:id
	 */
	update: function (req, res) {
    // Assign config
    var config = {
      maxBytes: sails.config.file.size,
       dirname: sails.config.file.path
    };
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };
    // Assign values
    var values = {
        title: req.param('title'),
         name: req.param('name'),
         desc: req.param('desc'),
       clinic: req.param('clinic'),
      picture: req.param('picture')
    };

    // Assign validate
    var validate = ['', 'null', 'undefined', null, undefined];

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Validate title
    if (validate.indexOf(values.title) > -1) {
      return res.badRequest("Oops! Gelar harus diisi.");
    }
    // Validate name
    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }
    // Validate clinic
    if (validate.indexOf(values.clinic) > -1) {
      return res.badRequest("Oops! Klinik harus diisi.");
    }
    // Find doctor based on criteria to make sure that doctor is exist
    Doctor
      .findOne(criteria)
      .then(function (doctor) {
        if (!doctor) {
          // Send a 400 response back down to the client indicating that the request is failed
          return res.badRequest("Oops! Data dokter tidak ditemukan.");
        }
        // Find doctor based on title & name of the provided values to make sure that the doctor didn't exist
        Doctor
         .findOne({
           title: values.title,
            name: values.name
         })
         .then(function (found) {
           if (found && found.id !== criteria.id) {
             // Send a 400 response back down to the client indicating that the request is failed
             return res.badRequest("Oops! Nama dokter sudah ada.");
           }
           // Set timeout to unlimited
           res.setTimeout(0);
           // Start uploading file
           req.file('picture')
             .upload(config, function (err, uploaded) {
               if (err) {
                 // Send an appropriate error response back down to the client
                 sails.log.error(err);
                 return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@update#2].");
               }

               if (uploaded.length === 0 && doctor.picture !== values.picture) {
                 // If no files were uploaded, respond with an error.
                 return res.badRequest("Oops! Foto tidak dapat di unggah. Silahkan coba lagi.");
               }

               values.picture = doctor.picture;

               if (uploaded.length !== 0) {
                 // If files were uploaded, delete old files to free the momory
                 try {
                   require('fs').unlinkSync(sails.config.file.path + values.picture);
                   require('fs').unlinkSync(sails.config.file.temp + values.picture);
                 } catch (e) {
                   // Handle error exception
                   sails.log.warn(e);
                 } finally {
                   // Get filename of currently uploaded picture
                   values.picture = require('path').basename(uploaded[0].fd);

                   try {
                     // Copy the file to the '.tmp' folder so that it becomes available immediately
                     require('fs').createReadStream(sails.config.file.path + values.picture)
                       .pipe(require('fs').createWriteStream(sails.config.file.temp + values.picture));
                   } catch (e) {
                     sails.log.warn(e);
                   }
                 }
               }

               Doctor
                 .update(criteria, values)
                 .then(function (updated) {
   								if (!updated) {
   									// Send a 400 response back down to the client indicating that the request is failed
   									return res.badRequest("Oops! Data tidak dapat diperbarui. Silahkan coba lagi.");
   								}
                   // Send a 200 response back down to the client
                   return res.json(updated);
                 })
                 .catch(function (errno) {
                   // Send an appropriate error response back down to the client
                   sails.log.error(errno);
                   return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@update#3].");
                 });
              });
         })
         .catch(function (errno) {
           sails.log.error(errno);
           // Send an appropriate error response back down to the client
           return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@update#2].");
         });
      })
      .catch(function (errno) {
        // Send an appropriate error response back down to the client
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@update#1].");
      });
	},

  /**
   * DoctorController.destroy()
   *
   * --> DELETE /doctor/:id
   */
  destroy: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Find doctor based on criteria to make sure that doctor is exist
    Doctor
      .findOne(criteria)
      .then(function (doctor) {
        if (!doctor) {
          // Send a 400 response back down to the client indicating that the request is invalid
          return res.badRequest('Oops! Data dokter tidak ditemukan.');
        }

        try {
          // Delete picture of doctor to free the memory from `assets` folder
					require('fs')
						.unlinkSync(sails.config.file.path + doctor.picture);
          // Delete picture of doctor to free the memory from `tmp` folder
          require('fs')
						.unlinkSync(sails.config.file.temp + doctor.picture);
				} catch (e) {
					// Handle error exception
          sails.log.warn(e);
				} finally {
          // Delete doctor
  				Doctor
  					.destroy(criteria)
  					.then(function (destroyed) {
  						if (!destroyed) {
                // Send a 400 response back down to the client indicating that the request is failed
  							return res.badRequest('Oops! Data tidak dapat dihapus. Silahkan coba lagi.');
  						}
              // Send a 200 response back down to the client
              return res.json(destroyed);
  					})
  					.catch(function (errno) {
              sails.log.error(errno);
  						// Send an appropriate error response back down to the client
  						return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@destroy#2].");
  					});
        }
      })
      .catch(function (errno) {
        // Send an appropriate error response back down to the client
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@destroy#1].");
      });
  },

  /**
   * DoctorController.shift()
   *
   * --> POST /doctor/shift
   */
  shift: function (req, res) {
    // Assign criteria
		var criteria = {
			id: req.param('doctor')
		};
		// Assign values
		var values = {
			      day: req.param('day'),
			startHour: new Date(req.param('startHour')),
			  endHour: new Date(req.param('endHour')),
         doctor: req.param('doctor')
		};
		// Assign moment and validate
		var moment = require('moment');
    // Validate request method
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
		// Validate number of the day
		if (isNaN(values.day)) {
			return res.badRequest("Oops! Hari tidak valid.");
		}
		// Validate startHour
		if (!moment(values.startHour).isValid()) {
			return res.badRequest("Oops! Jam mulai tidak valid.");
		}
		// Validate endHour
		if (!moment(values.endHour).isValid()) {
			return res.badRequest("Oops! Jam selesai tidak valid.");
		}
		if (values.startHour >= values.endHour) {
			return res.badRequest("Oops! Jam selesai harus lebih besar dari jam mulai.");
		}
		// Find doctor based on criteria
		Doctor
			.findOne(criteria)
      .populate('shifts')
			.then(function (doctor) {
				if (!doctor) {
					// Send a 400 response back down to the client indicating that the request is invalid
					return res.badRequest("Oops! Data dokter tidak ditemukan.");
				}

        var available = false;

        _.map(doctor.shifts, function (v) {
          // Check if the shift with the same values is already exist
          if (v.day == values.day) {
            if (moment(values.startHour).isBetween(moment(v.startHour),moment(v.endHour)) || moment(values.endHour).isBetween(moment(v.startHour),moment(v.endHour)) || moment(v.startHour).isSame(values.startHour) || moment(v.endHour).isSame(values.endHour) || (moment(values.startHour).isBefore(moment(v.startHour)) && moment(values.endHour).isAfter(moment(v.endHour)))) {
              available = true;
            }
          }
        });

        if (available) {
          return res.badRequest("Oops! Data shift dokter sudah ada.");
        }

        // Save shift with the provided values
        Shift
          .create(values)
          .then(function (shift) {
            if (!shift) {
              // Send a 400 response back down to the client indicating that the request is failed
              return res.badRequest('Oops! Data shift tidak dapat disimpan. Silahkan coba lagi.');
            }

            return res.json(shift);
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@shift#2].");
          });
			})
			.catch(function (errno) {
				sails.log.error(errno);
				// Send an appropriate error response back down to the client
				return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@shift#1].");
			});
  },

  /**
   * DoctorController.schedule()
   *
   * --> POST /doctor/schedule
   */
  schedule: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };
    // Assign values
    var values = {
      start: req.param('start'),
        end: req.param('end')
    };
    // Assign moment
    var moment = require('moment');
    // Validate request method
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Validate start of the date
    if (!moment(values.start).isValid()) {
      return res.badRequest("Oops! Tanggal awal tidak valid.");
    }
    // Validate end of the date
    if (!moment(values.end).isValid()) {
      return res.badRequest("Oops! Tanggal akhir tidak valid.");
    }
    // Find doctor based on criteria along with clinic of the doctor
    Doctor
      .findOne(criteria)
      .populate('clinic')
      .then(function (doctor) {
        if (!doctor) {
          return res.json(doctor);
        }

        Shift
          .find({
            doctor: doctor.id
          })
          .then(function (shifts) {
            if (shifts.length === 0) {
              return res.json(doctor);
            }

            var start = moment(values.start).format('YYYY-MM-DD'),
                  end = moment(values.end).add(1, 'days').format('YYYY-MM-DD'),
                dates = [],
                shift = [];

            do {
              dates.push(start);
              start = moment(start).add(1, 'days').format('YYYY-MM-DD');
            } while (moment(start).isBefore(end, 'day'));

            _.map(dates, function (v) {
              _.map(shifts, function (vv) {
                if (moment(v).day() === vv.day) {
                  var tmp = {};
                      tmp['date'] = v;
                      tmp['queue'] = 0;
                      tmp['time'] = vv.startHour;
                  _.extend(tmp, vv);
                  shift.push(tmp);
                }
              });
            });

            doctor.shift = shift;

            Schedule
              .find({
                shift: _.pluck(shifts, 'id')
              })
              .populate('notification')
              .then(function (schedule) {
                if (schedule.length === 0) {
                  return res.json(doctor);
                }

                Queue
                  .find({
                    schedule: _.pluck(schedule, 'id')
                  })
                  .populate('users')
                  .then(function (queue) {
                    if (queue.length === 0) {
                      return res.json(doctor);
                    }

                    var range = [],
                          sum = 0;

                    _.map(queue, function (v) {
                      if (moment(v.timeIn).isValid() && moment(v.timeOut).isValid()) {
                        range.push(moment.utc(moment(moment(v.timeOut), 'DD/MM/YYYY HH:mm:ss').diff(moment(moment(v.timeIn), 'DD/MM/YYYY HH:mm:ss'))).format('HH:mm:ss'));
                      }
                    });

                    if (range.length === 0) {
                      _.map(queue, function (v) {
                        range.push('00:22:22');
                      });
                    }

                    _.map(range, function (v) {
                      sum += moment.duration(v).asSeconds();
                    });

                    _.map(doctor.shift, function (v) {
                      _.map(schedule, function (vv) {
                        var moment = require('moment');
                        if (v.id === vv.shift && v.date === moment(vv.date).format('YYYY-MM-DD')) {
                          v.queue = _.where(queue, {schedule: vv.id}).length;
                          v.time = moment(v.startHour).add(((v.queue)*(Math.ceil(sum/range.length))), 'seconds').format('HH:mm');
                          v.users = _.flatten(_.pluck(_.flatten(_.pluck(_.where(queue, {schedule: vv.id}), 'users')), 'id'));

                        if(moment(v.startHour).add(((v.queue)*(Math.ceil(sum/range.length))), 'seconds').isAfter(moment(v.endHour))){
                          v.estimate = 'over';
                        }

                          if (vv.notification.length > 0) {
                            v.notif = vv.notification[vv.notification.length-1].topic;
                            if(vv.notification[vv.notification.length-1].topic.toString() === 'delay'){
                           v.time = moment(v.startHour).add(((v.queue)*(Math.ceil(sum/range.length))), 'seconds').add(vv.notification[vv.notification.length-1].duration, 'minutes').format('HH:mm');
                              }
                            else{
                            v.time = moment(v.startHour).add(((v.queue)*(Math.ceil(sum/range.length))), 'seconds').format('HH:mm');
                              }
                          }
                        }

                      });
                    });
                    //console.log(schedule);
                    return res.json(doctor);
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@schedule#4].")
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@schedule#3].")
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@schedule#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@schedule#1].");
      });
  },

  analysis: function (req, res) {
    Doctor
      .find()
      .populate('clinic')
      .then(function (doctors) {
        if (doctors.length === 0) {
          return res.json(doctors);
        }

        _.map(doctors, function (v) {
          v.time = new Date('10','10','10','00','00','00');
        });

        Shift
          .find({
            doctor: _.flatten(_.pluck(doctors, 'id'))
          })
          .then(function (shifts) {
            if (shifts.length === 0) {
              return res.json(doctors);
            }

            Schedule
              .find({
                shift: _.flatten(_.pluck(shifts, 'id'))
              })
              .populate('queues')
              .then(function (schedules) {
                if (schedules.length === 0) {
                  return res.json(doctors);
                }

                _.map(shifts, function (v) {
                  v.schedule = [];
                });

                _.map(schedules, function (v) {
                  _.map(shifts, function (vv) {
                    if (v.shift === vv.id) {
                      vv.schedule.push(v);
                    }
                  });
                });

                _.map(doctors, function (v) {
                  var  queue = _.flatten(_.pluck(_.flatten(_.pluck(_.where(shifts, {doctor: v.id}), 'schedule')), 'queues')),
                       range = [],
                         sum = 0,
                      moment = require('moment');

                  if (queue.length > 0) {
                    _.map(queue, function (vv) {
                      if (moment(vv.timeIn).isValid() && moment(vv.timeOut).isValid()) {
                        range.push(moment.utc(moment(moment(vv.timeOut), 'DD/MM/YYYY HH:mm:ss').diff(moment(moment(vv.timeIn), 'DD/MM/YYYY HH:mm:ss'))).format('HH:mm:ss'));
                      }
                    });

                    _.map(range, function (vv) {
                      sum += moment.duration(vv).asSeconds();
                    });

                    v.time = moment(v.time).add(Math.ceil(sum/range.length), 'seconds');
                  }

                  v.time = moment(v.time).format('HH:mm:ss');
                });

                return res.json(doctors);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@analysis#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@analysis#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@analysis#1].");
      });
  },

  notif: function (req, res) {
    // Find all doctor along with clinic and shift(s) of the doctor
    Doctor
      .find()
      .then(function (doctors) {
        // Send a 200 response back down to the client with the provided data
        return res.json(doctors);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Doctor@find#1].");
      });
  }

};
