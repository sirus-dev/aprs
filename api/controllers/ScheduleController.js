/**
 * ScheduleController
 *
 * @description :: Server-side logic for managing schedules
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


  find: function (req, res) {
    // Find all clinic
    Schedule
      .find()
      .then(function (schedule) {
        // Send a 200 response back down to the client with the provided data
        return res.json(schedule);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Schedule@find#1].");
      });
  },

  findOne: function (req, res) {
     // Find all queue along with user(s) and schedule(s) of queue
     var criteria = {
      id: req.param('id')
    };
    User
      .findOne(criteria)
      .populateAll()
      .then(function (user) {
     Queue
       .find({
        id : _.pluck(user.queues,'id')
       })
       .populateAll()
       .then(function (queue) {
         if (queue.length === 0) {
           // If no queue found, send an empty object
           return res.json(queue);
         }
         // Find shift(s) based on schedule(s) of queue along with doctor of the shift(s)
         Shift
           .find({
             id: _.pluck(_.pluck(queue, 'schedule'), 'shift')
           })
           .populate('doctor')
           .then(function (shift) {
             if (shift.length === 0) {
               // If no shift found, send only queue object
               return res.json(queue);
             }
             // Find clinic(s) based on doctor(s) of shift(s)
             Clinic
               .find({
                 id: _.pluck(_.pluck(shift, 'doctor'), 'clinic')
               })
               .then(function (clinic) {
                 if (clinic.length === 0) {
                   // If no doctor found, send only queue object
                   return res.json(queue);
                 }
                 // Add clinic(s) to doctor(s) of shift(s) object
                 _.map(shift, function (v) {
                   _.map(clinic, function (vv) {
                     if (v.doctor.clinic === vv.id) {
                       v.doctor.clinic = vv;
                     }
                   });
                 });
                 // Add shift(s) of schedule(s) to queue object
                 _.map(queue, function (v) {
                   _.map(shift, function (vv) {
                     if (v.schedule.shift === vv.id) {
                       v.schedule.shift = vv;
                     }
                   });
                 });
                 // Send a 200 response back down to the client with the provided data
                 return res.json(queue);
               })
               .catch(function (errno) {
                 sails.log.error(errno);
                 // Send an appropriate error response back down to the client
                 return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#3].");
               });
           })
           .catch(function (errno) {
             sails.log.error(errno);
             // Send an appropriate error response back down to the client
             return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#2].");
           });
       })
       .catch(function (errno) {
         sails.log.error(errno);
         // Send an appropriate error response back down to the client
         return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#1].");
       });
       })
        .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [User@findOne#1]");
      });
   }

};
