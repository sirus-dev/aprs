/**
 * NotificationController
 *
 * @description :: Server-side logic for managing notifications
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * NotificationController.find()
   *
   * --> GET /notif
   */
  find: function (req, res) {
    // Find all notif
    Notification
       .find()
       .populateAll()
       .then(function (notification) {
         if (notification.length === 0) {
           // If no notification found, send an empty object
           return res.json(notification);
         }
         // Find shift(s) based on schedule(s) of notification along with doctor of the shift(s)
         Shift
           .find({
             id: _.pluck(_.pluck(notification, 'schedule'), 'shift')
           })
           .populate('doctor')
           .then(function (shift) {
             if (shift.length === 0) {
               // If no shift found, send only notification object
               return res.json(notification);
             }
             // Find clinic(s) based on doctor(s) of shift(s)
             Clinic
               .find({
                 id: _.pluck(_.pluck(shift, 'doctor'), 'clinic')
               })
               .then(function (clinic) {
                 if (clinic.length === 0) {
                   // If no doctor found, send only notification object
                   return res.json(notification);
                 }
                //console.log(clinic);
                 // Add clinic(s) to doctor(s) of shift(s) object
                 _.map(shift, function (v) {
                   _.map(clinic, function (vv) {
                     if (v.doctor.clinic === vv.id) {
                       v.doctor.clinic = vv;
                     }
                   });
                 });
                 // Add shift(s) of schedule(s) to notification object
                 _.map(notification, function (v) {
                   _.map(shift, function (vv) {
                     if (v.schedule.shift === vv.id) {
                       v.schedule.shift = vv;
                     }
                   });
                 });
                 // Send a 200 response back down to the client with the provided data
                 return res.json(notification);
               })
               .catch(function (errno) {
                 sails.log.error(errno);
                 // Send an appropriate error response back down to the client
                 return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#3].");
               });
           })
           .catch(function (errno) {
             sails.log.error(errno);
             // Send an appropriate error response back down to the client
             return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#2].");
           });
       })
       .catch(function (errno) {
         sails.log.error(errno);
         // Send an appropriate error response back down to the client
         return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#1].");
       });
  },


  patient: function (req, res) {
    var moment = require('moment');
    // Assign values
        var cri = {
          date: moment(req.param('date')).format("YYYY-MM-DD"),
          shift: req.param('shift')
        };

    Schedule
      .findOne(cri)
      .populate('notification')
      .then(function (schedule) {

        var crit = {
          schedule: schedule.id
        };

    Queue
      .find(crit)
      .populateAll()
      .then(function (queue) {
          return res.json(queue);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#1].");
      });

      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Schedule@find#1].");
      });

  },

    /**
   * NotificationController.create()
   *
   * --> POST /notification
   */
  create: function (req, res) {

    var moment = require('moment');
    // Assign values
    var criteria = {
      day: moment(req.param('date')).format("d"),
      doctor: req.param('doctor')
    };

    if (criteria.day !== req.param('day')){
      return res.badRequest("Oops! Tanggal dan Hari tidak sesuai");
    }
    Shift
      .findOne(criteria)
      .then(function (shift) {
        if (!shift) {
         // Send a 400 response back down to the client indicating that the request is failed
         return res.badRequest("Oops! Tanggal tidak terdaftar");
       }

        var cri = {
          date: moment(req.param('date')).format("YYYY-MM-DD"),
          shift: req.param('shift')
        };

    Schedule
      .findOne(cri)
      .then(function (schedule) {
        if (!schedule) {
         // Send a 400 response back down to the client indicating that the request is failed
         return res.badRequest("Oops! Tidak ada jadwal dokter, Mohon periksa kembali");
       }

        var values = {
         creator: req.param('creator'),
         topic: req.param('topic'),
         content: req.param('content'),
         duration: req.param('duration'),
         schedule: schedule.id
        };

        if (isNaN(values.duration)) {
          values.duration = null;
        }
        if (values.topic === 'delay')
        {
          values.content = 'Keterlambatan ' + values.duration + ' Menit, ' + values.content;
        }

        Notification
          .create(values)
          .then(function (created) {
            if (!created) {
              // Send a 400 response back down to the client indicating that the request is failed
              return res.badRequest("Oops! Data gagal disimpan. Silahkan coba lagi.");
            }
            var crit = {
                schedule: schedule.id
            };
            // Send a 200 response back down to the client with the provided data
            Queue
              .update(crit,{
                isPresent: true
              })
              .then(function (updated){
                if (!updated) {
                     // Send a 400 response back down to the client indicating that the request is failed
                    return res.badRequest("Oops! update gagal");
                }

                Queue
                  .find(crit)
                  .populateAll()
                  .then(function (queue) {

                            Hospital
                                  .findOne({id : 1})
                                  .then(function (hospital) {
                                  Config
                                    .find()
                                    .then(function (config) {
                                      if (config.length === 0) {
                                        return res.badRequest("Oops! Konfigurasi email tidak ditemukan.");
                                      }
                                      
                                      if (values.topic === 'cancel'){
                                        message = 'Informasi Pembatalan Jadwal Konsultasi Anda';
                                      } else if (values.topic === 'delay'){
                                        message = 'Informasi Keterlambatan Jadwal Konsultasi Anda';
                                      } else {
                                        message = 'Informasi Jadwal Konsultasi Anda';
                                      }
                                      
                                      _.map(queue, function (a) {

                                      if (values.topic === 'delay'){
                                        schedule_estimate = 'Maaf ada keterlambatan ' + values.duration + ' Menit, Sehingga Jadwal Anda menjadi ' + moment(a.estimate).add(values.duration, 'minutes').format('HH:mm') + ' WIB. ';
                                      }
                                      else {
                                        schedule_estimate = '';
                                      }
                                      // console.log('Email');
                                      // console.log(_.flatten(_.pluck(_.flatten(a.users)), 'email').toString());
                                      // console.log(message);
                                      // console.log(schedule_estimate + '' + values.content);
                                      // console.log('SMS');
                                      // console.log(_.flatten(_.pluck(_.flatten(a.users)), 'phone'));
                                      // console.log("Topik : " + message + ", " + schedule_estimate + '' + values.content + " Tks, ");
                                      require('nodemailer')
                                        .createTransport().sendMail({
                                               from: config[0].emailName +' <'+ config[0].emailUserDomain + '>', // sender address
                                                 to: _.flatten(_.pluck(_.flatten(a.users)), 'email').toString(), // list of receivers
                                            subject: message, // Subject line
                                               html: schedule_estimate + '' + req.param('content') +
                                                     "<br><br>Terima Kasih, <br><br>" + hospital.name
                                        }, function (err, info) {
                                          if (err) {
                                            sails.log.error(err);
                                          } else {
                                            sails.log.info(info);
                                          }
                                        });

                                      // Send message with zenziva (https://zenziva.net/dokumentasi/)
                                        require('request')
                                         .post({
                                            url: config[0].smsUrl,
                                           form: {
                                             userkey: config[0].smsUsername,
                                             passkey: config[0].smsPassword,
                                                nohp: _.flatten(_.pluck(_.flatten(a.users)), 'phone').toString(),
                                               pesan: "Topik : " + message + ", " + schedule_estimate + '' + req.param('content') + " Tks, " + hospital.name
                                           }
                                         }, function (error, response, body) {
                                           if (error || response.statusCode != 200) {
                                             sails.log.error(error);
                                           }

                                           sails.log.info(body);
                                         });

                                      });

                                        return res.json(queue);

                                      })
                                      .catch(function (errno) {
                                        sails.log.error(errno);
                                        // Send an appropriate error response back down to the client
                                        return res.negotiate("Oops! Terjadi kesalahan sistem [User@find#1].");
                                      });
                                      })
                                      .catch(function (errno) {
                                        sails.log.error(errno);
                                        // Send an appropriate error response back down to the client
                                        return res.negotiate("Oops! Terjadi kesalahan sistem [Hospital@findOne].");
                                      });

                })
                .catch(function (errno) {
                  sails.log.error(errno);
                  // Send an appropriate error response back down to the client
                  return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@Find].");
                });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@Update].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Notif@Created#1].");
          });


      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Schedule@find#1].");
      });

      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Shift@findOne#1].");
      });

        // Save notif with the provided values
  },

/**
  * NotificationController.nurse()
  *
  * --> POST /notification/nurse
  */
  nurse: function (req, res) {
   var moment = require('moment');

   if (!moment(req.param('date')).isValid()) {
     return res.badRequest('Oops! Tanggal tidak valid.');
   }

   if (req.method.toUpperCase() !== 'POST') {
		 return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
	 }

   Nurse
    .findOne({
      user: req.session.passport.user
    })
    .populate('clinic')
    .populate('shifts')
    .then(function (nurse) {
      if (!nurse || _.pluck(nurse.shifts, 'shift').length === 0) {
        return res.json(nurse);
      }

      Shift
        .find({
          id: _.pluck(nurse.shifts, 'shift')
        })
        .populate('doctor')
        .then(function (shifts) {
          if (shifts.length === 0) {
            return res.json(shifts);
          }

          Schedule
            .find({
                date: moment(req.param('date')).format('YYYY-MM-DD'),
               shift: _.pluck(shifts, 'id')
             })
             .populate('notification')
             .then(function (schedules) {
               if (schedules.length === 0) {
                 return res.json(schedules);
               }

               _.map(schedules, function (v) {
                if (v.notification.length !== 0) {
                v.notification = v.notification[v.notification.length-1];
                }
                 _.map(shifts, function (vv) {
                   if (v.shift === vv.id) {
                     v.shift = vv;
                   }
                 });
               });

               nurse.shifts = null;
               nurse.schedules = [];

               _.map(schedules, function (v) {
                 if (v.notification.length !== 0) {
                   nurse.schedules.push(v);
                 }
               });

               return res.json(nurse);
             })
             .catch(function (errno) {
               sails.log.error(errno);
               return res.negotiate("Oops! Terjadi kesalahan sistem [Notification@nurse#3].");
             });
        })
        .catch(function (errno) {
          sails.log.error(errno);
          return res.negotiate("Oops! Terjadi kesalahan sistem [Notification@nurse#2].");
        });
    })
    .catch(function (errno) {
      sails.log.error(errno);
      return res.negotiate("Oops! Terjadi kesalahan sistem [Notification@nurse#1].");
    });
  },

/**
  * NotificationController.user()
  *
  * --> POST /notification/user
  */
 user: function (req, res) {
	 var moment = require('moment');

   if (!moment(req.param('date')).isValid()) {
     return res.badRequest('Oops! Tanggal tidak valid.');
   }

	 if (req.method.toUpperCase() !== 'POST') {
		 return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
	 }

	 User
	 	.findOne({
			id: req.session.passport.user
		})
		.populate('queues')
		.then(function (user) {
			if (!user || user.queues.length === 0) {
				return res.json(user);
			}

			Schedule
				.find({
					  id: _.flatten(_.pluck(user.queues, 'schedule')),
					date: moment(req.param('date')).format('YYYY-MM-DD')
				})
				.populate('shift')
				.populate('notification')
				.then(function (schedules) {
					if (schedules.length === 0 || _.flatten(_.pluck(_.flatten(_.pluck(schedules, 'shift')), 'doctor')).length === 0) {
						return res.json(schedules);
					}

					Doctor
						.find({
							id: _.flatten(_.pluck(_.flatten(_.pluck(schedules, 'shift')), 'doctor'))
						})
						.populate('clinic')
						.then(function (doctors) {
							if (doctors.length === 0) {
								return res.json(doctors);
							}

							_.map(_.flatten(_.pluck(schedules, 'shift')), function (v) {
								_.map(doctors, function (vv) {
									if (v.doctor === vv.id) {
										v.doctor = vv;
									}
								});
							});

							user.queues = null;
							user.schedules = [];

							_.map(schedules, function (v) {
								if (v.notification.length !== 0) {
									user.schedules.push(v);
								}
							});

							return res.json(user);
						})
						.catch(function (errno) {
							sails.log.error(errno);
							return res.negotiate("Oops! Terjadi kesalahan sistem [Notification@user#3].");
						});
				})
				.catch(function (errno) {
					sails.log.error(errno);
					return res.negotiate("Oops! Terjadi kesalahan sistem [Notification@user#2].");
				});
		})
		.catch(function (errno) {
			sails.log.error(errno);
			return res.negotiate("Oops! Terjadi kesalahan sistem [Notification@user#1].");
		});
 }

};
