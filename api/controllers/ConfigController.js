/**
 * ConfigController
 *
 * @description :: Server-side logic for managing configs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	/**
   * `ConfigController.find()`
	 *
	 * --> GET /config
   */
	find: function (req, res) {
		Config
			.find()
			.then(function (found) {
				return res.json(found);
			})
			.catch(function (errno) {
				sails.log.error(errno);
				return res.negotiate("Oops! Terjadi kesalahan sistem [Config@find#1].")
			})
	},

	/**
   * `ConfigController.update()`
	 *
	 * --> PUT /config/:id
   */
	update: function (req, res) {
		// Assign criteria
		var criteria = {
			id: req.param('id')
		};
		// Assign values
		var values = {
			         smsUrl: req.param('smsUrl'),
					smsUsername: req.param('smsUsername'),
					smsPassword: req.param('smsPassword'),
						emailName: req.param('emailName'),
			emailUserDomain: req.param('emailUserDomain')
		};
		// Assign validate
		var validate = ['', 'null', 'undefined', null, undefined];

		if (isNaN(criteria.id)) {
			criteria.id = 0;
		}
		// Validate id
		if (validate.indexOf(criteria.id) > -1) {
			return res.badRequest("Oops! ID Konfigurasi harus diisi.");
		}
		// Validate smsUrl
		if (validate.indexOf(values.smsUrl) > -1) {
			return res.badRequest("Oops! API Url harus diisi.");
		}
		// Validate smsUsername
		if (validate.indexOf(values.smsUsername) > -1) {
			return res.badRequest("Oops! Username harus diisi.");
		}
		// Validate smsPassword
		if (validate.indexOf(values.smsPassword) > -1) {
			return res.badRequest("Oops! Password harus diisi.");
		}
		// Validate emailName
		if (validate.indexOf(values.emailName) > -1) {
			return res.badRequest("Oops! Nama harus diisi.");
		}
		// Validate emailUserDomain
		if (validate.indexOf(values.emailUserDomain) > -1) {
			return res.badRequest("Oops! Email harus diisi.");
		}
		// Find config based on criteria
		Config
			.findOne(criteria)
			.then(function (found) {
				if (!found) {
					// Send a 400 response back down to the client indicating that the request is invalid
					return res.badRequest("Oops! Data tidak ditemukan.");
				}
				// Update selected data
				Config
					.update(criteria, values)
					.then(function (updated) {
						if (!updated) {
							// Send a 400 response back down to the client indicating that the request is failed
							return res.badRequest("Oops! Data gagal diperbarui. Silahkan coba lagi.");
						}
						 // Send a 200 response back down to the client
						 return res.json(updated);
					})
					.catch(function (errno) {
						sails.log.error(errno);
						return res.negotiate("Oops! Terjadi kesalahan sistem [Config@update#2].");
					});
			})
			.catch(function (errno) {
				sails.log.error(errno);
				return res.negotiate("Oops! Terjadi kesalahan sistem [Config@update#1].");
			});
	},

	/**
   * `ConfigController.email()`
	 *
	 * --> POST /config/:id
   */
	email: function (req, res) {
		// Assign criteria
		var criteria = {
			id: req.param('id')
		};
		// Assign values
		var values = {
			     to: req.param('to'),
		  subject: req.param('subject'),
			content: req.param('contents') + "  Terima kasih, RS MARY Cileungsi Hijau"
		};
		// Assign validate
		var validate = ['', 'null', 'undefined', null, undefined];

		if (isNaN(criteria.id)) {
			criteria.id = 0;
		}
		// Validate id
		if (validate.indexOf(criteria.id) > -1) {
			return res.badRequest("Oops! ID Konfigurasi harus diisi.");
		}
		// Validate receiver
		if (validate.indexOf(values.to) > -1) {
			return res.badRequest("Oops! Email penerima harus diisi.");
		}
		// Validate content
		if (validate.indexOf(values.content) > -1) {
			return res.badRequest("Oops! Isi email harus diisi.");
		}
		// Validate request method
		if (req.method.toUpperCase() !== 'POST') {
			return res.badRequest("Oops! Permintaan tidak diperbolehkan.");
		}
		// Find config based on criteria
		Config
			.findOne(criteria)
			.then(function (config) {
				if (!config) {
					return res.badRequest("Oops! Konfigurasi email tidak ditemukan.");
				}
				// send mail with defined transport object
				require('nodemailer')
					.createTransport().sendMail({
					       from: config.emailName +' <'+ config.emailUserDomain + '>', // sender address
					         to: values.to, // list of receivers
					    subject: values.subject, // Subject line
					       text: values.content // plaintext body
					}, function(error, info){
						if (error) {
							sails.log.error(error);
							return res.badRequest("Oops! Email gagal dikirim. Silahkan coba lagi.");
						}

						return res.json(info);
				});
			})
			.catch(function (errno) {
				sails.log.error(errno);
				return res.negotiate("Oops! Terjadi kesalahan sistem [Config@email#1].");
			});
	},

	/**
   * `ConfigController.sms()`
	 *
	 * --> POST /config/sms
   */
	sms: function (req, res) {
		// Assign criteria
		var criteria = {
			id: req.param('id')
		};
		// Assign values
		var values = {
			     to: req.param('to'),
			content: req.param('content') + "  Tks, RS MARY Cileungsi Hijau"
		};
		// Assign validate
		var validate = ['', 'null', 'undefined', null, undefined];

		if (isNaN(criteria.id)) {
			criteria.id = 0;
		}
		// Validate id
		if (validate.indexOf(criteria.id) > -1) {
			return res.badRequest("Oops! ID Konfigurasi harus diisi.");
		}
		// Validate receiver
		if (validate.indexOf(values.to) > -1) {
			return res.badRequest("Oops! Nomor telepon harus diisi.");
		}
		// Validate receiver
		if (req.param('to').toString().charAt(0) !== '0' || req.param('to').toString().charAt(1) !== '8') {
			return res.badRequest("Oops! Nomor tujuan tidak valid");
		}
		// Validate content
		if (validate.indexOf(values.content) > -1) {
			return res.badRequest("Oops! Isi pesan harus diisi.");
		}
		// Validate request method
		if (req.method.toUpperCase() !== 'POST') {
			return res.badRequest("Oops! Permintaan tidak diperbolehkan.");
		}
		// Find config based on criteria
		Config
			.findOne(criteria)
			.then(function (config) {
				if (!config) {
					return res.badRequest("Oops! Konfigurasi sms tidak ditemukan.");
				}
				// Send message with zenziva (https://zenziva.net/dokumentasi/)
				require('request')
					.post({
						 url: config.smsUrl,
						form: {
							userkey: config.smsUsername,
							passkey: config.smsPassword,
							   nohp: values.to,
							  pesan: values.content
						}
					}, function (error, response, body) {
						if (error || response.statusCode != 200) {
							sails.log.error(error);
							return res.negotiate("Oops! Terjadi kesalahan sistem [Config@sms#2].");
						}
						sails.log.info(body);
						return res.json(body);
					});
			})
			.catch(function (errno) {
				sails.log.error(errno);
				return res.negotiate("Oops! Terjadi kesalahan sistem [Config@sms#1].");
			});
	},

	setting: function (req, res) {
		Hospital
			.find()
			.then(function (found) {
				return res.json(found);
			})
			.catch(function (errno) {
				sails.log.error(errno);
				return res.negotiate("Oops! Terjadi kesalahan sistem [Config@find#1].")
			})
	}

};
