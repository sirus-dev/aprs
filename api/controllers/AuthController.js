/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  register: function (req, res) {
    var values = {
             phone: req.param('phone'),
              name: req.param('name'),
             email: '-',
            gender: 'F',
         birthdate: new Date(1980,0,1),
      isRegistered: true,
              role: req.param('role')
    };

        var avalues = {
       password: req.param('password'),
      passwordd: req.param('passwordd')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    if (validate.indexOf(values.phone) > -1) {
      return res.badRequest("Oops! No hp harus diisi.");
    }

    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }

    if (validate.indexOf(values.role) > -1) {
      return res.badRequest("Oops! Role harus diisi.");
    }

    if (validate.indexOf(avalues.password) > -1) {
      return res.badRequest("Oops! Password baru harus diisi.");
    }

    if (validate.indexOf(avalues.passwordd) > -1) {
      return res.badRequest("Oops! Konfirmasi password baru harus diisi.");
    }

    if (avalues.password != avalues.passwordd) {
      return res.badRequest("Oops! Konfirmasi password tidak sesuai dengan password.");
    }

    User
      .findOne({
        phone: values.phone
      })
      .populate('auth')
      .then(function (user) {
        var roles = ['admin', 'nurse', 'admin', 'suster'];

        if (user) {
        if (roles.indexOf(user.role) > -1) {
          return res.badRequest("Oops! Anda telah terdaftar sebagai " + roles[roles.indexOf(user.role)+2] + ".");
        }
        else if (user.isRegistered === false) {
          User
              .update({ id : user.id}, { isRegistered : true })
              .then(function (updated) {
                if (!updated) {
                  // Send a 400 response back down to the client indicating that the request is failed
                  return res.badRequest('Oops! Data gagal diperbaharui. Silahkan coba lagi.');
                }
      Hospital
      .findOne({id : 1})
      .then(function (hospital) {
      Config
        .find()
        .then(function (config) {
          if (config.length > 0) {
            Auth
              .create({
                activationCode: Math.floor((Math.random() * Math.pow(10, 5) + 1)),
                          user: user.id,
                      password: avalues.password
              })
              .then(function (auth) {
                if (auth) {
                  var roles = ['admin','nurse','patient','Admin','Suster','Pasien'];

                require('request')
                  .post({
                       url: config[0].smsUrl,
                       form: {
                       userkey: config[0].smsUsername,
                       passkey: config[0].smsPassword,
                       nohp: values.phone,
                       pesan: "Kode aktivasi anda adalah " + auth.activationCode + ". Tks, " + hospital.name
                       }
                       }, function (error, response, body) {
                       if (error || response.statusCode != 200) {
                       sails.log.error(error);
                       }
                       sails.log.info(body);
                       });

                  return res.ok();

                }
              })
              .catch(function (errno) {
                sails.log.error(errno);
              });
          }
        })
        .catch(function (errno) {
          sails.log.error(errno);
        });
        })
        .catch(function (errno) {
          sails.log.error(errno);
        });
        
              })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Pattient@update].");
          });
        }
        else{
          return res.badRequest("Oops! Anda telah terdaftar sebagai pasien. Silahkan " + (user.auth[0].activationCode ? 'aktivasi' : 'login') + ".");
        }
        }

        else{
        User
          .create(values)
          .then(function (created) {
            if (!created) {
              return res.badRequest("Oops! Pendaftaran tidak bisa dilakukan. Silahkan coba lagi.");
            }
      Hospital
      .findOne({id : 1})
      .then(function (hospital) {
      Config
        .find()
        .then(function (config) {
          if (config.length > 0) {
            Auth
              .create({
                activationCode: Math.floor((Math.random() * Math.pow(10, 5) + 1)),
                          user: created.id,
                      password: avalues.password
              })
              .then(function (auth) {
                if (auth) {
                  var roles = ['admin','nurse','patient','Admin','Suster','Pasien'];

                require('request')
                  .post({
                       url: config[0].smsUrl,
                       form: {
                       userkey: config[0].smsUsername,
                       passkey: config[0].smsPassword,
                       nohp: values.phone,
                       pesan: "Kode aktivasi anda adalah " + auth.activationCode + ". Tks, " + hospital.name
                       }
                       }, function (error, response, body) {
                       if (error || response.statusCode != 200) {
                       sails.log.error(error);
                       }
                       sails.log.info(body);
                       });

                  return res.json(created);

                }
              })
              .catch(function (errno) {
                sails.log.error(errno);
              });
          }
        })
        .catch(function (errno) {
          sails.log.error(errno);
        });
        })
        .catch(function (errno) {
          sails.log.error(errno);
        });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@register#2].");
          });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@register#1].");
      });
  },

  reset_admin: function (req, res) {
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    var values = {
              email: req.param('email'),
      resetPassCode: req.param('resetPassCode'),
               role: req.param('role')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (validate.indexOf(values.email) > -1) {
      return res.badRequest("Oops! Email harus diisi.");
    }

    if (validate.indexOf(values.resetPassCode) > -1) {
      return res.badRequest("Oops! Kode Reset Password harus diisi.");
    }

    if (validate.indexOf(values.role) > -1) {
      return res.badRequest("Oops! Role harus diisi.");
    }

    User
      .findOne({
        email: values.email
      })
      .populate('auth')
      .then(function (user) {
        if (!user || !user.isRegistered) {
          return res.badRequest("Oops! Anda belum terdaftar. Silahkan daftar.");
        }

        if (['',null,'undefined'].indexOf(user.auth[0].activationCode) === -1) {
          return res.badRequest("Oops! Akun anda belum diaktivasi. Silahkan aktivasi.");
        }

        if (user.auth[0].resetPassCode != values.resetPassCode) {
          return res.badRequest("Oops! Kode reset password tidak sesuai.");
        }

        var roles = ['admin', 'nurse', 'patient', 'admin', 'perawat', 'pasien'];

        if (roles.indexOf(roles[values.role]) === -1) {
          return res.badRequest("Oops! Role tidak ditemukan.");
        }

        if (user.role != roles[values.role]) {
          return res.badRequest("Oops! Anda tidak terdaftar sebagai " + roles[Number(values.role)+3] + ".");
        }

        if (validate.indexOf(req.param('password')) > -1) {
          return res.ok();
        }

        if (req.param('password') != req.param('passwordd')) {
          return res.badRequest("Oops! Konfirmasi password tidak sesuai dengan password.");
        }

        Auth
          .update({
            user: user.id
          },{
                 password: req.param('password'),
            resetPassCode: null
          })
          .then(function (updated) {
            if (!updated) {
              return res.badRequest("Oops! Reset password tidak bisa dilakukan. Silahkan coba lagi.");
            }

            return res.ok();
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@reset#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@reset#1].");
      });
  },

  forget_admin: function (req, res) {
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    var values = {
      email: req.param('email'),
       role: req.param('role')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (validate.indexOf(values.email) > -1) {
      return res.badRequest("Oops! Email harus diisi.");
    }

    if (validate.indexOf(values.role) > -1) {
      return res.badRequest("Oops! Role harus diisi.");
    }

    User
      .findOne({
        email: req.param('email')
      })
      .populate('auth')
      .then(function (user) {
        if (!user || !user.isRegistered) {
          return res.badRequest("Oops! Anda belum terdaftar. Silahkan daftar.");
        }

        if (['',null,'undefined'].indexOf(user.auth[0].activationCode) === -1) {
          return res.badRequest("Oops! Akun anda belum diaktivasi. Silahkan aktivasi.");
        }

        var roles = ['admin', 'nurse', 'patient', 'admin', 'perawat', 'pasien'];

        if (roles.indexOf(roles[values.role]) === -1) {
          return res.badRequest("Oops! Role tidak ditemukan.");
        }

        if (user.role != roles[values.role]) {
          return res.badRequest("Oops! Anda tidak terdaftar sebagai " + roles[Number(values.role)+3] + ".");
        }

        Config
          .find()
          .then(function (config) {
            if (config.length === 0) {
              return res.badRequest("Oops! Konfigurasi email tidak ditemukan.");
            }

            var code = Math.floor((Math.random() * Math.pow(10, 5) + 1));

            Auth
              .update({
                user: user.id
              },{
                resetPassCode: code
              })
              .then(function (auth) {
                if (!auth) {
                  return res.badRequest("Oops! Autentifikasi tidak dapat diperbarui. Silahkan coba lagi.");
                }
                // send mail with defined transport object
                require('nodemailer')
                  .createTransport().sendMail({
                       from: config[0].emailName +' <'+ config[0].emailUserDomain + '>', // sender address
                         to: user.email, // list of receivers
                    subject: "Akun Pemulihan", // Subject line
                       html: "<br>Untuk me-reset password akun Anda, silahkan masukkan kode berikut :" +
                             "<br><br><strong>Kode Reset Password: " + code + "</strong>"
                  }, function(error, info){
                    if (error) {
                      sails.log.error(error);
                    } else {
                      sails.log.info(info);
                    }
                  });
                  
                return res.json(user);

              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@forget#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@forget#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@forget#1].");
      });
  },

  activation: function (req, res) {
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    var values = {
               phone: req.param('phone'),
      activationCode: req.param('activationCode'),
                role: req.param('role')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (validate.indexOf(values.phone) > -1) {
      return res.badRequest("Oops! No hp tidak ditemukan.");
    }

    if (validate.indexOf(values.activationCode) > -1) {
      return res.badRequest("Oops! Kode Aktivasi tidak ditemukan.");
    }

    if (validate.indexOf(values.role) > -1) {
      return res.badRequest("Oops! Role tidak ditemukan.");
    }

    User
      .findOne({
        phone: values.phone
      })
      .populate('auth')
      .then(function (user) {
        if (!user || !user.isRegistered) {
          return res.badRequest("Oops! Anda belum terdaftar. Silahkan daftar.");
        }

        if (['',null,'undefined'].indexOf(user.auth[0].activationCode) > -1) {
          return res.badRequest("Oops! Akun anda telah diaktivasi. Silahkan login.");
        }

        if (user.auth[0].activationCode != values.activationCode) {
          return res.badRequest("Oops! Kode aktivasi tidak sesuai.");
        }

        var roles = ['admin', 'nurse', 'patient', 'admin', 'perawat', 'pasien'];

        if (roles.indexOf(roles[values.role]) === -1) {
          return res.badRequest("Oops! Role tidak ditemukan.");
        }

        if (user.role != roles[values.role]) {
          return res.badRequest("Oops! Anda tidak terdaftar sebagai " + roles[Number(values.role)+3] + ".");
        }

        Auth
          .update({
            user: user.id
          },{
            activationCode: null
          })
          .then(function (auth) {
            if (!auth) {
              return res.badRequest("Oops! Aktivasi tidak dapat dilakukan. Silahkan coba lagi.");
            }

            return res.json(user);
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@activation#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@activation#1].");
      });
  },

  forget: function (req, res) {
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    var values = {
      phone: req.param('phone'),
       role: req.param('role')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (validate.indexOf(values.phone) > -1) {
      return res.badRequest("Oops! No hp harus diisi.");
    }

    if (validate.indexOf(values.role) > -1) {
      return res.badRequest("Oops! Role harus diisi.");
    }

    User
      .findOne({
        phone: req.param('phone')
      })
      .populate('auth')
      .then(function (user) {
        if (!user || !user.isRegistered) {
          return res.badRequest("Oops! Anda belum terdaftar. Silahkan daftar.");
        }

        if (['',null,'undefined'].indexOf(user.auth[0].activationCode) === -1) {
          return res.badRequest("Oops! Akun anda belum diaktivasi. Silahkan aktivasi.");
        }

        var roles = ['admin', 'nurse', 'patient', 'admin', 'perawat', 'pasien'];

        if (roles.indexOf(roles[values.role]) === -1) {
          return res.badRequest("Oops! Role tidak ditemukan.");
        }

        if (user.role != roles[values.role]) {
          return res.badRequest("Oops! Anda tidak terdaftar sebagai " + roles[Number(values.role)+3] + ".");
        }

    Hospital
      .findOne({id : 1})
      .then(function (hospital) {
        Config
          .find()
          .then(function (config) {
            if (config.length === 0) {
              return res.badRequest("Oops! Konfigurasi tidak ditemukan.");
            }

            var code = Math.floor((Math.random() * Math.pow(10, 5) + 1));

            Auth
              .update({
                user: user.id
              },{
                resetPassCode: code
              })
              .then(function (auth) {
                if (!auth) {
                  return res.badRequest("Oops! Autentifikasi tidak dapat diperbarui. Silahkan coba lagi.");
                }
                  require('request')
                    .post({
                     url: config[0].smsUrl,
                     form: {
                     userkey: config[0].smsUsername,
                     passkey: config[0].smsPassword,
                     nohp: values.phone,
                     pesan: "Kode reset password Anda adalah " + code + ". Tks, " + hospital.name
                     }
                     }, function (error, response, body) {
                     if (error || response.statusCode != 200) {
                         sails.log.error(error);
                     }
                     sails.log.info(body);
                     });                

                return res.json(user);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@forget#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@forget#2].");
          });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@findHospital].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@forget#1].");
      });
  },

  reset: function (req, res) {
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    var values = {
              phone: req.param('phone'),
      resetPassCode: req.param('resetPassCode'),
               role: req.param('role')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (validate.indexOf(values.phone) > -1) {
      return res.badRequest("Oops! No hp harus diisi.");
    }

    if (validate.indexOf(values.resetPassCode) > -1) {
      return res.badRequest("Oops! Kode Reset Password harus diisi.");
    }

    if (validate.indexOf(values.role) > -1) {
      return res.badRequest("Oops! Role harus diisi.");
    }

    User
      .findOne({
        phone: values.phone
      })
      .populate('auth')
      .then(function (user) {
        if (!user || !user.isRegistered) {
          return res.badRequest("Oops! Anda belum terdaftar. Silahkan daftar.");
        }

        if (['',null,'undefined'].indexOf(user.auth[0].activationCode) === -1) {
          return res.badRequest("Oops! Akun anda belum diaktivasi. Silahkan aktivasi.");
        }

        if (user.auth[0].resetPassCode != values.resetPassCode) {
          return res.badRequest("Oops! Kode reset password tidak sesuai.");
        }

        var roles = ['admin', 'nurse', 'patient', 'admin', 'perawat', 'pasien'];

        if (roles.indexOf(roles[values.role]) === -1) {
          return res.badRequest("Oops! Role tidak ditemukan.");
        }

        if (user.role != roles[values.role]) {
          return res.badRequest("Oops! Anda tidak terdaftar sebagai " + roles[Number(values.role)+3] + ".");
        }

        if (validate.indexOf(req.param('password')) > -1) {
          return res.ok();
        }

        if (req.param('password') != req.param('passwordd')) {
          return res.badRequest("Oops! Konfirmasi password tidak sesuai dengan password.");
        }

        Auth
          .update({
            user: user.id
          },{
                 password: req.param('password'),
            resetPassCode: null
          })
          .then(function (updated) {
            if (!updated) {
              return res.badRequest("Oops! Reset password tidak bisa dilakukan. Silahkan coba lagi.");
            }

            return res.ok();
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@reset#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@reset#1].");
      });
  },

  login: function(req, res) {
    require('passport')
      .authenticate('local', function(err, user, info) {
        if (err) {
          sails.log.error(err);
          return res.badRequest("Oops! Terjadi kesalahan sistem [Auth@login#1].");
        }

        if (!user) {
          return res.badRequest(info);
        }

        req.logIn(user, function (err) {
          if (err) {
            return res.badRequest("Oops! Terjadi kesalahan sistem [Auth@login#2].");
          }

          var random;

          do {
            random = Math.floor((Math.random() * Math.pow(10, 15) + 1));
          } while (random.length < 15);

          return res.json({
            token: ChiperService.createToken(user),
             user: {
                  id: String(random) + String(user.id),
               email: user.email,
                name: user.name
             }
           });
         });
       })(req, res);
   },

   logout: function (req, res) {
     req.logout();

     return res.ok();
   },

   change: function (req, res) {
     if (req.method.toUpperCase() !== 'POST') {
       return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
     }

     var criteria = {
       id: req.session.passport.user
     };

     var values = {
             role: req.param('role'),
         password: req.param('current'),
        passwordd: req.param('password'),
       passworddd: req.param('confirm')
     };

     var validate = ['', 'null', 'undefined', null, undefined];

     if (validate.indexOf(values.role) > -1) {
       return res.badRequest("Oops! Role harus diisi.");
     }

     if (validate.indexOf(values.password) > -1) {
       return res.badRequest("Oops! Password sekarang harus diisi.");
     }

     if (validate.indexOf(values.passwordd) > -1) {
       return res.badRequest("Oops! Password baru harus diisi.");
     }

     if (validate.indexOf(values.passworddd) > -1) {
       return res.badRequest("Oops! Konfirmasi password baru harus diisi.");
     }

     if (values.passwordd != values.passworddd) {
       return res.badRequest("Oops! Konfirmasi password tidak sesuai dengan password baru.");
     }

     User
      .findOne(criteria)
      .populate('auth')
      .then(function (user) {
        if (!user || user.role != values.role) {
          return res.badRequest("Oops! Profil tidak ditemukan.");
        }

        require('bcrypt')
          .compare(values.password, user.auth[0].password, function (error, response) {
            if (error) {
              sails.log.error(error);
              return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@change#2].");
            }

            if (!response) {
              return res.badRequest("Oops! Password salah. Silahkan coba lagi.");
            }

            Auth
              .update({
                user: criteria.id
              },{
                password: values.passwordd
              })
              .then(function (updated) {
                if (!updated) {
                  return res.badRequest("Oops! Password tidak dapat diperbarui. Silahkan coba lagi.");
                }

                return res.json(updated);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@change#3].");
              });
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Auth@change#1].");
      });
   }

};
