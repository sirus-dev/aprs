/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  /**
   * UserController.create()
   *
   * --> POST /user
   */
  create: function (req, res) {
    var values = {
             phone: req.param('phone'),
              name: req.param('name'),
             phone: '-',
            gender: 'F',
         birthdate: new Date(1980,0,1),
      isRegistered: true,
              role: req.param('role')
    };

    var avalues = {
       password: req.param('password'),
      passwordd: req.param('passwordd')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    if (validate.indexOf(values.phone) > -1) {
      return res.badRequest("Oops! No hp harus diisi.");
    }

    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }

    if (validate.indexOf(values.role) > -1) {
      return res.badRequest("Oops! Role harus diisi.");
    }

    if (validate.indexOf(avalues.password) > -1) {
      return res.badRequest("Oops! Password baru harus diisi.");
    }

    if (validate.indexOf(avalues.passwordd) > -1) {
      return res.badRequest("Oops! Konfirmasi password baru harus diisi.");
    }

    if (avalues.password != avalues.passwordd) {
      return res.badRequest("Oops! Konfirmasi password tidak sesuai dengan password.");
    }

    User
      .findOne({
        phone: values.phone
      })
      .populate('auth')
      .then(function (found) {
        var roles = ['admin', 'nurse', 'admin', 'perawat'];

        if (found && roles.indexOf(found.role) > -1) {
          return res.badRequest("Oops! Anda telah terdaftar sebagai " + roles[roles.indexOf(found.role)+2] + ".");
        }

        if (found && found.isRegistered) {
          return res.badRequest("Oops! Anda telah terdaftar sebagai pasien. Silahkan " + (found.auth[0].activationCode ? 'aktivasi' : 'login') + ".");
        }

        if (found && !found.isRegistered) {
          var criteria = {
            id: found.id
          };

          User
            .update(criteria, values)
            .then(function (updated) {
              if (!updated) {
                return res.badRequest("Oops! Pendaftaran tidak bisa dilakukan. Silahkan coba lagi.");
              }

              setTimeout(function () {
                Auth
                  .create({
                        user: found.id,
                    password: avalues.password
                  })
                  .then(function (auth) {
                    if (!auth) {
                      return res.badRequest("Oops! Pendaftaran tidak bisa dilakukan. Silahkan coba lagi.");
                    }

                    return res.json(updated);
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    return res.negotiate("Oops! Terjadi kesalahan sistem [User@create#5].");
                  });
              }, 1500);
            })
            .catch(function (errno) {
              sails.log.error(errno);
              return res.negotiate("Oops! Terjadi kesalahan sistem [User@create#4].");
            });
        } else {
          User
            .create(values)
            .then(function (created) {
              if (!created) {
                return res.badRequest("Oops! Pendaftaran tidak bisa dilakukan. Silahkan coba lagi.");
              }

              setTimeout(function () {
                Auth
                  .create({
                        user: created.id,
                    password: avalues.password
                  })
                  .then(function (auth) {
                    if (!auth) {
                      return res.badRequest("Oops! Pendaftaran tidak bisa dilakukan. Silahkan coba lagi.");
                    }

                    return res.json(created);
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    return res.negotiate("Oops! Terjadi kesalahan sistem [User@create#3].");
                  });
              }, 1500);
            })
            .catch(function (errno) {
              sails.log.error(errno);
              return res.negotiate("Oops! Terjadi kesalahan sistem [User@create#2].");
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [User@create#1].");
      });

  },

  /**
   * UserController
   *
   * --> GET /patient
   */
  find: function (req, res) {
    // Find all user role patient
    var criteria = {
      role: 'patient'
    };
    User
      .find(criteria)
      .then(function (user) {
        // Send a 200 response back down to the client with the provided data
        return res.json(user);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [User@find#1].");
      });
  },

	/**
   * PatientController.findOne()
   *
   * --> GET /patient/:id
   */
  findOne: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };
    User
      .findOne(criteria)
      .then(function (user) {

	      return res.json(user);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Patient@findOne#1]");
      });
  },

  /**
   * PatientController.update()
   *
   * --> PUT /patient/:id
   */
  update: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };

    // Assign values
    var values = {
             name: req.param('name'),
            email: req.param('email'),
              age: req.param('age'),
           gender: req.param('gender'),
        birthdate: req.param('birthdate'),
            phone: req.param('phone')
    };
    // Assign validate
    var validate = ['', 'null', 'undefined', null, undefined];
    // Validate `name`
    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }
    // Validate `email`
    if (validate.indexOf(values.email) > -1) {
      return res.badRequest("Oops! email harus diisi.");
    }
    // Validate `age`
    if (validate.indexOf(values.age) > -1) {
      return res.badRequest("Oops! Umur harus diisi.");
    }
    // Validate `gender`
    if (validate.indexOf(values.gender) > -1) {
      return res.badRequest("Oops! Jenis Kelamin harus diisi.");
    }
    // Validate `birthdate`
    if (validate.indexOf(values.birthdate) > -1) {
      return res.badRequest("Oops! Tanggal Lahir harus diisi.");
    }

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Find patient based on criteria to make sure that the patient is exist
    User
      .findOne(criteria)
      .then(function (found) {
        if (!found) {
          // Send a 400 response back down to the client indicating that the request is invalid
          return res.badRequest('Oops! Data pasien tidak ditemukan.');
        }
        // Find patient based on name of the provided values to make sure that the patient didn't exist
        User
          .findOne({
            email: values.email
          })
          .then(function (found) {
            if (found && found.id !== criteria.id) {
              // Send a 400 response back down to the client indicating that the request is failed
              return res.badRequest("Oops! Email pasien sudah ada.");
            }
            // Update patient based on criteria with the provided values
            User
              .update(criteria, values)
              .then(function (updated) {
                if (!updated) {
                  // Send a 400 response back down to the client indicating that the request is failed
                  return res.badRequest('Oops! Data gagal diperbaharui. Silahkan coba lagi.');
                }
                // Send a 200 response back down to the client
                return res.ok();
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Jenis Kelamin harus di isi.");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Pattient@update#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Pattient@update#1].");
      });
  },

  /**
   * PatientController.destroy()
   * delete patient
   * --> DELETE /patient/:id
   */
  destroy: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Find patient based on criteria to make sure that the patient is exist
    User
      .findOne(criteria)
      .then(function (found) {
        if (!found) {
          // Send a 400 response back down to the client indicating that the request is invalid
          return res.badRequest('Oops! Data Pasien tidak ditemukan.');
        }
        // Delete pasien based on criteria
        User
          .destroy(criteria)
          .then(function (destroyed) {
            if (!destroyed) {
              // Send a 400 response back down to the client indicating that the request is failed
              return res.badRequest('Oops! Data gagal dihapus. Silahkan coba lagi.')
            }
            // Send a 200 response back down to the client
            return res.ok();
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@destroy#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@destroy#1].");
      });
  },


  profile: function (req, res) {
    User
      .findOne({
        id: req.session.passport.user
      })
      .populate('auth')
      .populate('security')
      .populate('address')
      .then(function (user) {
        if (!user) {
          return res.json(user);
        }

        SecurityType
          .find({
            id: _.flatten(_.pluck(user.security), 'securityType')
          })
          .then(function (type) {
            if (type.length > 0) {
              _.map(user.security, function (v) {
                _.map(type, function (vv) {
                  if (v.securityType === vv.id) {
                    v.securityType = vv;
                  }
                });
              });
            }

            City
              .findOne({
                id: _.pluck(user.address, 'city')
              })
              .then(function (city) {
                if (!city) {
                  return res.json(user);
                }

                Province
                  .findOne({
                    id: city.province
                  })
                  .populate('country')
                  .then(function (province) {
                    if (!province) {
                      return res.json(user);
                    }

                    _.map(user.address, function (v) {
                      if (v.city === city.id) {
                        city.province = province;
                        v.city = city;
                      }
                    });

                    return res.json(user);
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    return res.negotiate("Oops! Terjadi kesalahan sistem [User@profile#4].");
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [User@profile#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [User@profile#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [User@profile#1].");
      });
  },

  change: function (req, res) {
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    var criteria = {
      id: req.session.passport.user
    };

    var values = {
           name: req.param('name'),
          email: req.param('email'),
      birthdate: req.param('birthdate'),
          phone: req.param('phone'),
         gender: req.param('gender'),
           role: req.param('role')
    };

    var avalues = {
      address: req.param('address'),
         city: req.param('city'),
          ktp: req.param('ktp')
    }

    var validate = ['', 'null', 'undefined', null, undefined];

    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }

    if (validate.indexOf(values.email) > -1) {
      return res.badRequest("Oops! Email harus diisi.");
    }

    if (validate.indexOf(values.role) > -1) {
      return res.badRequest("Oops! Role harus diisi.");
    }

    if (validate.indexOf(values.birthdate) > -1) {
      values.birthdate = new Date(1980,0,1);
    }

    if (validate.indexOf(values.phone) > -1) {
      values.phone = '-';
    }

    if (validate.indexOf(values.gender) > -1) {
      values.gender = 'F';
    }

    if (validate.indexOf(avalues.ktp) > -1) {
      avalues.ktp = '-';
    }

    User
      .findOne(criteria)
      .then(function (found) {
        if (!found || found.role != values.role) {
          return res.badRequest("Oops! Profil tidak ditemukan.");
        }

        User
          .findOne({
            email: values.email
          })
          .then(function (user) {
            if (user && user.id !== found.id) {
              var roles = ['admin','nurse','patient','admin','perawat','pasien'];
              return res.badRequest("Oops! Email telah terdaftar sebagai " + roles[roles.indexOf(user.role)+3] + ".");
            }

            User
              .update(criteria, values)
              .then(function (updated) {
                if (!updated) {
                  return res.badRequest("Oops! Profil tidak dapat diperbarui. Silahkan coba lagi.");
                }

                SecurityType
                  .findOrCreate({
                    type: 'ktp'
                  })
                  .then(function (type) {
                    if (!type) {
                      return res.badRequest("Oops! KTP tidak dapat diperbarui. Silahkan coba lagi.");
                    }

                    Security
                      .update({
                        user: criteria.id
                      },{
                              number: avalues.ktp,
                        securityType: type.id
                      })
                      .then(function (security) {
                        if (!security) {
                          return res.badRequest("Oops! KTP tidak dapat diperbarui. Silahkan coba lagi.");
                        }

                        City
                          .findOne({
                            id: avalues.city
                          })
                          .then(function (city) {
                            if (city) {
                              Address
                                .update({
                                  user: criteria.id
                                },{
                                  address: avalues.address,
                                     city: city.id
                                })
                                .then(function (address) {
                                  if (!address) {
                                    return res.badRequest("Oops! Alamat tidak dapat diperbarui. Silahkan coba lagi.");
                                  }

                                  return res.json(updated);
                                })
                                .catch(function (errno) {
                                  sails.log.error(errno);
                                  return res.negotiate("Oops! Terjadi kesalahan sistem [User@change#7].");
                                });
                            } else {
                              return res.json(updated);
                            }
                          })
                          .catch(function (errno) {
                            sails.log.error(errno);
                            return res.negotiate("Oops! Terjadi kesalahan sistem [User@change#6].");
                          });
                      })
                      .catch(function (errno) {
                        sails.log.error(errno);
                        return res.negotiate("Oops! Terjadi kesalahan sistem [User@change#5].");
                      });
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    return res.negotiate("Oops! Terjadi kesalahan sistem [User@change#4].");
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [User@change#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [User@change#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [User@change#1].");
      });
  },

  queue: function (req, res) {
    User
      .findOne({
        id: req.session.passport.user
      })
      .populate('queues')
      .then(function (user) {
        if (!user || user.queues.length === 0) {
          return res.json(user);
        }

        Schedule
          .find({
            id: _.flatten(_.pluck(user.queues, 'schedule'))
          })
          .populate('shift')
          .then(function (schedules) {
            if (schedules.length === 0) {
              return res.json(schedules);
            }

            Doctor
              .find({
                id: _.flatten(_.pluck(_.flatten(_.pluck(schedules, 'shift')), 'doctor'))
              })
              .populate('clinic')
              .then(function (doctors) {
                if (doctors.length === 0) {
                  return res.json(doctors);
                }

                _.map(_.flatten(_.pluck(schedules, 'shift')), function (v) {
                  _.map(doctors, function (vv) {
                    if (v.doctor === vv.id) {
                      v.doctor = vv;
                    }
                  });
                });

                _.map(user.queues, function (v) {
                  _.map(schedules, function (vv) {
                    if (v.schedule === vv.id) {
                      v.schedule = vv;
                    }
                  });
                });

                var moment = require('moment'),
                        qr = require('qrcode-npm').qrcode(5, 'M');

                _.map(user.queues, function (v) {
                  qr.addData(moment(v.schedule.date).format('DDMMYY').concat(v.schedule.shift.doctor.clinic.id.toString().length < 3 ? v.schedule.shift.doctor.clinic.id.toString().length < 2 ? '00' : '0' : '').concat(v.schedule.shift.doctor.clinic.id).concat(v.schedule.shift.doctor.id.toString().length < 3 ? v.schedule.shift.doctor.id.toString().length < 2 ? '00' : '0' : '').concat(v.schedule.shift.doctor.id).concat(v.number.toString().length < 2 ? '0' : '').concat(v.number));
                  qr.make();
                  v.code = qr.createImgTag();
                });

                return res.json(user);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [User@queue#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [User@queue#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [User@queue#1].");
      });
  }

};
