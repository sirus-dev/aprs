/**
 * QueueController
 *
 * @description :: Server-side logic for managing queues
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * `QueueController.find()`
   *
   * --> GET /queue
   */
  find: function (req, res) {
     // Find all queue along with user(s) and schedule(s) of queue
     Queue
       .find()
       .populateAll()
       .then(function (queue) {
         if (queue.length === 0) {
           // If no queue found, send an empty object
           return res.json(queue);
         }
         // Find shift(s) based on schedule(s) of queue along with doctor of the shift(s)
         Shift
           .find({
             id: _.pluck(_.pluck(queue, 'schedule'), 'shift')
           })
           .populate('doctor')
           .then(function (shift) {
             if (shift.length === 0) {
               // If no shift found, send only queue object
               return res.json(queue);
             }
             // Find clinic(s) based on doctor(s) of shift(s)
             Clinic
               .find({
                 id: _.pluck(_.pluck(shift, 'doctor'), 'clinic')
               })
               .then(function (clinic) {
                 if (clinic.length === 0) {
                   // If no doctor found, send only queue object
                   return res.json(queue);
                 }
                 // Add clinic(s) to doctor(s) of shift(s) object
                 _.map(shift, function (v) {
                   _.map(clinic, function (vv) {
                     if (v.doctor.clinic === vv.id) {
                       v.doctor.clinic = vv;
                     }
                   });
                 });
                 // Add shift(s) of schedule(s) to queue object
                 _.map(queue, function (v) {
                   _.map(shift, function (vv) {
                     if (v.schedule.shift === vv.id) {
                       v.schedule.shift = vv;
                     }
                   });
                 });
                 // Send a 200 response back down to the client with the provided data
                 return res.json(queue);
               })
               .catch(function (errno) {
                 sails.log.error(errno);
                 // Send an appropriate error response back down to the client
                 return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#3].");
               });
           })
           .catch(function (errno) {
             sails.log.error(errno);
             // Send an appropriate error response back down to the client
             return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#2].");
           });
       })
       .catch(function (errno) {
         sails.log.error(errno);
         // Send an appropriate error response back down to the client
         return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@find#1].");
       });
   },

   // riset registration_code in queue
   riset: function (req, res) {
     var moment = require('moment'),
     start = moment().subtract(3, "months").format("YYYY-MM-DD HH:mm"),
     end = moment().subtract(2, "months").format("YYYY-MM-DD HH:mm");

      Queue
        .update({estimate : {'>=': start, '<=': end}}, {registration_code : null})
        .then(function (updated) {
          if (!updated) {
             // Send a 400 response back down to the client indicating that the request is failed
             return res.badRequest('Oops! Data gagal diperbaharui. Silahkan coba lagi.');
          }
             // Send a 200 response back down to the client
              return res.json(updated);
        })
        .catch(function (errno) {
           sails.log.error(errno);
           // Send an appropriate error response back down to the client
           return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@update#1].");
        });
   },

   queue : function (req, res) {
 
    //Assign values
    var values = {
         doctor: req.param('doctor'),
          shift: req.param('shift'),
           date: req.param('date'),
           name: req.param('name')
    };
    var express = require('express')
         , cors = require('cors')
          , app = express();

  Synchronize
    .find()
    .then(function (syn) {
    if (!syn) {
          // Send a 400 response back down to the client indicating that the request is invalid
          return res.badRequest('Domain Tidak ditemukan, harap daftarkan domain anda');
    } 
    var corsOptions = {
        origin: _.pluck(syn, 'domain')
    };

    if(corsOptions.origin.indexOf(req.header('Origin')) == -1){
      return res.badRequest('Maaf Domain anda tidak terdaftar !');
    }else{
    // Assign validate
    var validate = ['', null, undefined, 'null', 'undefined'];

    // Validate doctor
    if (validate.indexOf(values.doctor) > -1) {
      return res.badRequest("Oops! Dokter harus diisi.");
    }
    // Validate shift
    if (validate.indexOf(values.shift) > -1) {
      return res.badRequest("Oops! Dokter shift harus diisi.");
    }
    // Validate date
    if (validate.indexOf(values.date) > -1) {
      return res.badRequest("Oops! Tanggal harus diisi.");
    }
    // Validate name
    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }
    Doctor
      .findOne({
        id: values.doctor
      })
      .populate('clinic')
      .then(function (doctor) {
        if (!doctor) {
          // Send a 400 response back down to the client indicating that the request is invalid
          return res.badRequest("Oops! Data dokter tidak ditemukan.");
        }
        // Find shift of the doctor to make sure that shift of doctor is exist
        Shift
          .findOne({
                id: values.shift,
            doctor: doctor.id
          })
          .then(function (shift) {
            if (!shift) {
              // Send a 400 response back down to the client indicating that the request is invalid
              return res.badRequest("Oops! Data shift tidak ditemukan.");
            }
            // Find or create new schedule based on shift of doctor
            Schedule
              .findOrCreate({
                 date: values.date,
                shift: shift.id
              })
              .then(function (schedule) {
                if (!schedule) {
                  // Send a 400 response back down to the client indicating that the request is failed
                  return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                }
                // Find queue(s) based on schedule along with user(s) of queue(s)
                Queue
                  .find()
                  .populate('users')
                  .populate('schedule')
                  .then(function (queue) {
                    // Assign registered
                    var     moment = require('moment'),
                        registered = false;
                    // If queue found, check if the user is already registered based on email address of user
                    if (queue.length > 0) {
                      _.map(queue, function (v) {
                        if (v.users[0].name == values.name && !moment(v.timeIn).isValid() && !moment(v.timeOut).isValid() && moment(v.schedule.date).isAfter(moment().subtract(1, 'days'))) {
                          registered = true;
                        }
                      });
                    }
                    // If user have been registered
                    if (registered) {
                      // Send a 400 response back down to the client indicating that the request is failed
                      return res.badRequest("Oops! Anda telah terdaftar pada salah satu jadwal.");
                    }

                    Shift
                      .find({
                        doctor: doctor.id
                      })
                      .then(function (fshift) {
                        Schedule
                          .find({
                            shift: _.flatten(_.pluck(fshift, 'id'))
                          })
                          .populate('queues')
                          .then(function (fschedule) {
                            var moment = require('moment'),
                                 range = [],
                                   sum = 0;

                            _.map(_.flatten(_.pluck(fschedule, 'queues')), function (v) {
                              if (moment(v.timeIn).isValid() && moment(v.timeOut).isValid()) {
                                range.push(moment.utc(moment(moment(v.timeOut), 'DD/MM/YYYY HH:mm:ss').diff(moment(moment(v.timeIn), 'DD/MM/YYYY HH:mm:ss'))).format('HH:mm:ss'));
                              }
                            });

                            if (range.length === 0) {
                              _.map(_.flatten(_.pluck(fschedule, 'queues')), function (v) {
                                range.push('00:22:22');
                              });
                            }

                            _.map(range, function (v) {
                              sum += moment.duration(v).asSeconds();
                            });
                            // Find or create new user based on email address of the user
                            User
                              .findOrCreate({
                                name: values.name
                              })
                              .then(function (user) {
                                if (!user) {
                                  // Send a 400 response back down to the client indicating that the request is failed
                                  return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                                }

                                var roles = ['admin', 'nurse', 'admin', 'perawat'];
                                if (roles.indexOf(user.role) > -1) {
                                  return res.badRequest("Oops! Anda telah terdaftar sebagai " + roles[roles.indexOf(user.role)+2] + ".");
                                }
                                          // Update data of the user with new values
                                          User
                                            .update({
                                              id: user.id
                                            },{
                                                   name: values.name,
                                                   role: 'patient'
                                            })
                                            .then(function (updated) {
                                              if (!updated) {
                                                // Send a 400 response back down to the client indicating that the request is failed
                                                return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                                              }
                                              var number = 1;
                                              _.map(queue, function (v) {
                                                if (v.schedule.id === schedule.id) {
                                                  number += 1;
                                                }
                                              });
                                              // Create queue for the user
                                              Queue
                                                .create({
                                                     number: number,
                                                   schedule: schedule.id,
                                                  isPresent: false,
                                                      users: user.id
                                                })
                                                .then(function (queue) {
                                                  if (!queue) {
                                                    // Send a 400 response back down to the client indicating that the request is failed
                                                    return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                                                  }
                                                      //Send a 200 response back down to the client with the provided data
                                                      var text = {
                                                        Nama : values.name,
                                                        Klinik : doctor.clinic.name,
                                                        Dokter : doctor.name + ", " + doctor.title,
                                                        Jadwal : moment(shift.startHour).format('HH:mm') + "-" + moment(shift.endHour).format('HH:mm') + "WIB",
                                                        Tanggal : moment(schedule.date).format('DD-MM-YYYY'),
                                                        Antrian : queue.number
                                                      };
                                                      console.log(text);
                                                      return res.json({
                                                        Nama : values.name, 
                                                      Klinik : doctor.clinic.name, 
                                                      Dokter : doctor.name + ", " + doctor.title, 
                                                      Jadwal : moment(shift.startHour).format('HH:mm') + "-" + moment(shift.endHour).format('HH:mm') + "WIB", 
                                                     Tanggal : moment(schedule.date).format('DD-MM-YYYY'),
                                                     Antrian : queue.number
                                                      });
                                                
                                                })
                                                .catch(function (errno) {
                                                  sails.log.error(errno);
                                                  // Send an appropriate error response back down to the client
                                                  return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#11].");
                                                });
                                            })
                                            .catch(function (errno) {
                                              sails.log.error(errno);
                                              // Send an appropriate error response back down to the client
                                              return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#10].");
                                            });
                              })
                              .catch(function (errno) {
                                sails.log.error(errno);
                                // Send an appropriate error response back down to the client
                                return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#7].");
                              });
                          })
                          .catch(function (errno) {
                            sails.log.error(errno);
                            // Send an appropriate error response back down to the client
                            return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#6].");
                          });
                      })
                      .catch(function (errno) {
                        sails.log.error(errno);
                        // Send an appropriate error response back down to the client
                        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#5].");
                      });
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    // Send an appropriate error response back down to the client
                    return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#4].");
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#1].");
      });
    }
    })
    .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#1].");
    });
   },

/*
Find queue for update konsultation

*/
  nurse: function (req, res) {

    Queue
      .findOne({
          schedule : req.param('id'),
          timeOut : null
        })
      .then(function (queue) {
        if (!queue) {
          return res.json({ queue: 'no'});
        }
        return res.json(queue);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@findOne#1].");
      });
  },

  /*
Find queue for stoped konsultation

*/
  stoped: function (req, res) {

    var criteria = {
       id: req.param('id')
     };

     if (isNaN(criteria.id)) {
       criteria.id = 0;
     }

    Queue
      .findOne(criteria)
      .then(function (queue) {

        return res.json(queue);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@findOne#1].");
      });
  },

   /**
    * `QueueController.findOne()`
    *
    * --> GET /queue
    */
   findOne: function (req, res) {
     var criteria = {
       id: req.param('id')
     };

     if (isNaN(criteria.id)) {
       criteria.id = 0;
     }

     Queue
      .findOne(criteria)
      .populate('users')
      .populate('schedule')
      .then(function (queue) {
        if (!queue || queue.users.length === 0 || !queue.schedule) {
          return res.json(queue);
        }

        Shift
          .findOne({
            id: queue.schedule.shift
          })
          .populate('doctor')
          .then(function (shift) {
            if (!shift || !shift.doctor) {
              return res.json(shift);
            }

            Clinic
              .findOne({
                id: shift.doctor.clinic
              })
              .then(function (clinic) {
                if (!clinic) {
                  return res.json(clinic);
                }

                shift.doctor.clinic = clinic;
                queue.schedule.shift = shift;

                return res.json(queue);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@findOne#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@findOne#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@findOne#1].");
      });
   },

  /**
   * `QueueController.create()`
	 *
	 * --> POST /queue
   */
  create: function (req, res) {
    // Assign values
    var values = {
         doctor: req.param('doctor'),
          shift: req.param('shift'),
           date: req.param('date'),
            ktp: req.param('ktp'),
          email: req.param('email'),
           name: req.param('name'),
      birthdate: req.param('birthdate'),
          phone: req.param('phone')
    };
    // Assign validate
    var validate = ['', null, undefined, 'null', 'undefined'];
    // Validate doctor
    if (validate.indexOf(values.doctor) > -1) {
      return res.badRequest("Oops! Dokter harus diisi.");
    }
    // Validate shift
    if (validate.indexOf(values.shift) > -1) {
      return res.badRequest("Oops! Dokter shift harus diisi.");
    }
    // Validate date
    if (validate.indexOf(values.date) > -1) {
      return res.badRequest("Oops! Tanggal harus diisi.");
    }
    // Validate ktp
    if (validate.indexOf(values.ktp) > -1) {
      return res.badRequest("Oops! Nomor KTP harus diisi.");
    }
    // Validate name
    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }
    // Validate birthdate
    if (validate.indexOf(values.birthdate) > -1) {
      return res.badRequest("Oops! Tanggal lahir harus diisi.");
    }
    // Validate phone
    if (validate.indexOf(values.phone) > -1) {
      return res.badRequest("Oops! Nomor telepon harus diisi.");
    }
    if (validate.indexOf(values.email) > -1) {
      values.email = null;
    }
    // Find doctor to make sure that doctor is exist
    Doctor
      .findOne({
        id: values.doctor
      })
      .populate('clinic')
      .then(function (doctor) {
        if (!doctor) {
          // Send a 400 response back down to the client indicating that the request is invalid
          return res.badRequest("Oops! Data dokter tidak ditemukan.");
        }
        // Find shift of the doctor to make sure that shift of doctor is exist
        Shift
          .findOne({
                id: values.shift,
            doctor: doctor.id
          })
          .then(function (shift) {
            if (!shift) {
              // Send a 400 response back down to the client indicating that the request is invalid
              return res.badRequest("Oops! Data shift tidak ditemukan.");
            }
            // Find or create new schedule based on shift of doctor
            Schedule
              .findOrCreate({
                 date: values.date,
                shift: shift.id
              })
              .then(function (schedule) {
                if (!schedule) {
                  // Send a 400 response back down to the client indicating that the request is failed
                  return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                }
                // Find queue(s) based on schedule along with user(s) of queue(s)
                Queue
                  .find()
                  .populate('users')
                  .populate('schedule')
                  .then(function (queue) {
                    // Assign registered
                    var     moment = require('moment'),
                        registered = false;
                    // If queue found, check if the user is already registered based on email address of user
                    if (queue.length > 0) {
                      _.map(queue, function (v) {
                        if (v.users[0].phone == values.phone && !v.isPresent && !moment(v.timeIn).isValid() && !moment(v.timeOut).isValid() && moment(v.schedule.date).isAfter(moment().subtract(1, 'days'))) {
                          registered = true;
                        }
                      });
                    }
                    // If user have been registered
                    if (registered) {
                      // Send a 400 response back down to the client indicating that the request is failed
                      return res.badRequest("Oops! Anda telah terdaftar pada salah satu jadwal.");
                    }

                    Shift
                      .find({
                        doctor: doctor.id
                      })
                      .then(function (fshift) {
                        Schedule
                          .find({
                            shift: _.flatten(_.pluck(fshift, 'id'))
                          })
                          .populate('queues')
                          .then(function (fschedule) {
                            var moment = require('moment'),
                                 range = [],
                                   sum = 0;

                            _.map(_.flatten(_.pluck(fschedule, 'queues')), function (v) {
                              if (moment(v.timeIn).isValid() && moment(v.timeOut).isValid()) {
                                range.push(moment.utc(moment(moment(v.timeOut), 'DD/MM/YYYY HH:mm:ss').diff(moment(moment(v.timeIn), 'DD/MM/YYYY HH:mm:ss'))).format('HH:mm:ss'));
                              }
                            });

                            if (range.length === 0) {
                              _.map(_.flatten(_.pluck(fschedule, 'queues')), function (v) {
                                range.push('00:22:22');
                              });
                            }

                            _.map(range, function (v) {
                              sum += moment.duration(v).asSeconds();
                            });
                            // Find or create new user based on email address of the user
                            User
                              .findOrCreate({
                                phone: values.phone
                              })
                              .then(function (user) {
                                if (!user) {
                                  // Send a 400 response back down to the client indicating that the request is failed
                                  return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                                }

                                var roles = ['admin', 'nurse', 'admin', 'perawat'];
                                if (roles.indexOf(user.role) > -1) {
                                  return res.badRequest("Oops! Anda telah terdaftar sebagai " + roles[roles.indexOf(user.role)+2] + ".");
                                }
                                // Find or create security type (ktp)
                                SecurityType
                                  .findOrCreate({
                                    type: 'ktp'
                                  })
                                  .then(function (type) {
                                    if (!type) {
                                      // Send a 400 response back down to the client indicating that the request is failed
                                      return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                                    }
                                    // Find or create security
                                    setTimeout(function () {
                                      Security
                                        .findOrCreate({
                                                number: values.ktp,
                                                  user: user.id,
                                          securityType: type.id
                                        })
                                        .then(function (security) {
                                          if (!security) {
                                            // Send a 400 response back down to the client indicating that the request is failed
                                            return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                                          }
                                          // Update data of the user with new values
                                          User
                                            .update({
                                              id: user.id
                                            },{
                                                   name: values.name,
                                              birthdate: values.birthdate,
                                                  email: values.email,
                                                   role: 'patient'
                                            })
                                            .then(function (updated) {
                                              if (!updated) {
                                                // Send a 400 response back down to the client indicating that the request is failed
                                                return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                                              }
                                              var number = 1;
                                              _.map(queue, function (v) {
                                                if (v.schedule.id === schedule.id) {
                                                  number += 1;
                                                }
                                              });
                                              var moment = require('moment'),
                                                estimate = moment().format('YYYY-MM-DD') + ' ' + moment(shift.startHour).add(((number-1)*(Math.ceil(sum/range.length))), 'seconds').format('HH:mm'),
                                                registration_code = Math.floor((Math.random() * Math.pow(10, 4) + 1)),
                                                day = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
                                              // Create queue for the user
                                              Queue
                                                .create({
                                                     number: number,
                                          registration_code: registration_code,
                                                   schedule: schedule.id,
                                                  isPresent: false,
                                                      users: user.id,
                                                   estimate: estimate
                                                })
                                                .then(function (queue) {
                                                  if (!queue) {
                                                    // Send a 400 response back down to the client indicating that the request is failed
                                                    return res.badRequest("Oops! Pendaftaran gagal. Silahkan coba lagi.");
                                                  }
                                                  Hospital
                                                    .findOne({id : 1})
                                                    .then(function (hospital) {
                                                  // Send mail for registration confirmation
                                                  Config
                                              			.find()
                                              			.then(function (config) {
                                              				if (config.length === 0) {
                                              					return res.badRequest("Oops! Konfigurasi email tidak ditemukan.");
                                              				}

                                                          var code = moment(schedule.date).format('DDMMYY').concat(doctor.clinic.id.toString().length < 3 ? doctor.clinic.id.toString().length < 2 ? '00' : '0' : '').concat(doctor.clinic.id).concat(doctor.id.toString().length < 3 ? doctor.id.toString().length < 2 ? '00' : '0' : '').concat(doctor.id).concat(queue.number.toString().length < 2 ? '0' : '').concat(queue.number),
                                                              qr = require('qrcode-npm').qrcode(5, 'M');
                                                              qr.addData(code);
                                                              qr.make();

                                                      if (values.email !== null){
                                                      // send mail with defined transport object
                                              				require('nodemailer')
                                                        .createTransport().sendMail({
                                                				       from: config[0].emailName +' <'+ config[0].emailUserDomain + '>', // sender address
                                                				         to: values.email, // list of receivers
                                                				    subject: "Konfirmasi Pendaftaran - " + hospital.name, // Subject line
                                                               html: "<br>Yth Bpk/Ibu " + values.name +
                                                                     "<br><br>Pendaftaran jadwal konsultasi Anda di " + hospital.name +" berhasil." +
                                                                     "<br>Berikut adalah detail dari pendaftaran anda:" +
                                                                     "<br>- Nama : " + values.name +
                                                                     "<br>- Klinik : " + doctor.clinic.name +
                                                                     "<br>- Dokter : " + doctor.name + ", " + doctor.title +
                                                                     "<br>- Jadwal Dokter : " + moment(shift.startHour).format('HH:mm') + "-" + moment(shift.endHour).format('HH:mm') + " WIB" +
                                                                     "<br>- Hari : " + day[shift.day] +
                                                                     "<br>- Tanggal : " + moment(schedule.date).format('DD-MM-YYYY') +
                                                                     "<br>- Nomor Antrian : " + queue.number +
                                                                     "<br>- Estimasi Waktu Konsultasi : " + moment(shift.startHour).add(((queue.number-1)*(Math.ceil(sum/range.length))), 'seconds').format('HH:mm') + ' WIB' +
                                                                     "<br>- Lokasi RS : " + doctor.clinic.location +
                                                                     "<br>- Kode Pendaftaran : " + registration_code +
                                                                     "<br><br>Selanjutnya, mohon simpan dan tunjukkan kode berikut ke petugas pendaftaran kami." +
                                                                     "<br>" + qr.createImgTag() +
                                                                     "<br><small><strong>" + code + "</strong></small>" +
                                                                     "<br>" +
                                                                     "<br>Catatan:" +
                                                                     "<i><li>Anda tidak perlu mencetak / mem-print email ini.</li>" + 
                                                                     "<li>Mohon datang 30 menit lebih awal untuk daftar ulang.</li>" + 
                                                                     "<li>Jika ada keterlambatan atau pembatalan jadwal, kami akan memberitahukan melalui email ini.</li></i>" +
                                                                     "<br><br>" +
                                                                     "Terima kasih," +
                                                                     "<br><br>" + hospital.name 
                                                				}, function (err, info) {
                                                          if (err) {
                                                            sails.log.error(err);
                                                          } else {
                                                            sails.log.info(info);
                                                          }
                                                				});
                                                      }
  

                                                      //Send message with zenziva (https://zenziva.net/dokumentasi/)
                                              				require('request')
                                              					.post({
                                              						 url: config[0].smsUrl,
                                              						form: {
                                              							userkey: config[0].smsUsername,
                                              							passkey: config[0].smsPassword,
                                              							   nohp: values.phone,
                                              							  pesan: "Rincian pendaftaran " + hospital.name + ": No Ref. " + registration_code + "/" + doctor.name + ", " + doctor.title + "/" + doctor.clinic.name + "/" + moment(schedule.date).format('DD-MM-YYYY') + "/" + moment(shift.startHour).add(((queue.number-1)*(Math.ceil(sum/range.length))), 'seconds').format('HH:mm') + ". Harap datang 30 menit lebih awal"
                                              						}
                                              					}, function (error, response, body) {
                                              						if (error || response.statusCode != 200) {
                                              							sails.log.error(error);
                                              						}
                                              						sails.log.info(body);
                                              					});



                                                      //Send a 200 response back down to the client with the provided data
                                                      return res.json(queue);
                                              			})
                                              			.catch(function (errno) {
                                              				sails.log.error(errno);
                                              				return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#12].");
                                              			});
                                                    })
                                                    .catch(function (errno) {
                                                      sails.log.error(errno);
                                                      return res.negotiate("Oops! Terjadi kesalahan sistem [Hospital@findOne].");
                                                    });
                                                })
                                                .catch(function (errno) {
                                                  sails.log.error(errno);
                                                  // Send an appropriate error response back down to the client
                                                  return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#11].");
                                                });
                                            })
                                            .catch(function (errno) {
                                              sails.log.error(errno);
                                              // Send an appropriate error response back down to the client
                                              return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#10].");
                                            });
                                        })
                                        .catch(function (errno) {
                                          sails.log.error(errno);
                                          // Send an appropriate error response back down to the client
                                          return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#9].");
                                        });
                                    }, 1500);
                                  })
                                  .catch(function (errno) {
                                    sails.log.error(errno);
                                    // Send an appropriate error response back down to the client
                                    return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#8].");
                                  });
                              })
                              .catch(function (errno) {
                                sails.log.error(errno);
                                // Send an appropriate error response back down to the client
                                return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#7].");
                              });
                          })
                          .catch(function (errno) {
                            sails.log.error(errno);
                            // Send an appropriate error response back down to the client
                            return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#6].");
                          });
                      })
                      .catch(function (errno) {
                        sails.log.error(errno);
                        // Send an appropriate error response back down to the client
                        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#5].");
                      });
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    // Send an appropriate error response back down to the client
                    return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#4].");
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@create#1].");
      });
  },

  /**
   * `QueueController.update`
   *
   * --> PUT /queue/:id
   */
  update: function (req, res) {
    var criteria = {
      id: req.param('id')
    };

    var values = {
        timeIn: req.param('timeIn'),
        timeOut: req.param('timeOut')
     };

    Queue
      .findOne(criteria)
      .populate('users')
      .populate('schedule')
      .then(function (queue) {
        if (!queue || queue.users.length === 0 || !queue.schedule) {
          return res.badRequest("Oops! Antrian tidak ditemukan.");
        }

        Shift
          .findOne({
            id: queue.schedule.shift
          })
          .then(function (shift) {
            if (!shift) {
              return res.badRequest("Oops! Antrian tidak ditemukan.");
            }

            Doctor
              .findOne({
                id: shift.doctor
              })
              .populate('clinic')
              .then(function (doctor) {
                if (!doctor || !doctor.clinic) {
                  return res.badRequest("Oops! Antrian tidak ditemukan.");
                }

                Queue
                  .update(criteria, values)
                  .then(function (updated) {
                    if (!updated) {
                      return res.badRequest("Oops! Antrian tidak dapat diperbarui.");
                    }

                  if (req.param('timeOut') === undefined){
                    return res.json(updated);
                  }
                  else{
                    Log
                      .create({
                        patient: queue.users[0].name,
                         doctor: doctor.title.concat(" ").concat(doctor.name),
                         clinic: doctor.clinic.name,
                           date: queue.schedule.date,
                         timeIn: values.timeIn,
                        timeOut: values.timeOut
                      })
                      .then(function (created) {
                        return res.json(created);
                      })
                      .catch(function (errno) {
                        sails.log.error(errno);
                        // Send an appropriate error response back down to the client
                        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@update#5].");
                      });
                  }
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    // Send an appropriate error response back down to the client
                    return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@update#4].");
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@update#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@update#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@update#1].");
      });
  },

  /**
   * `QueueController.confirmation`
   *
   * --> POST /queue/confirmation
   */
  confirmation: function (req, res) {
    var values = {
        doctor: req.param('doctor'),
         shift: req.param('shift'),
      schedule: req.param('schedule')
    };

    var validate = ['', 'null', 'undefined', null, undefined];

    if (req.method.toUpperCase() !== 'POST') {
      return res.badRequest("Oops! Permintaan tidak diperbolehkan.");
    }

    if (isNaN(values.doctor)) {
      values.doctor = 0;
    }

    if (validate.indexOf(values.doctor) > -1) {
      return res.badRequest("Oops! Dokter tidak ditemukan.");
    }

    if (validate.indexOf(values.shift) > -1) {
      return res.badRequest("Oops! Shift tidak ditemukan.");
    }

    if (validate.indexOf(values.schedule) > -1) {
      return res.badRequest("Oops! Jadwal tidak ditemukan.");
    }

    Doctor
      .findOne({
        id: values.doctor
      })
      .populate('clinic')
      .then(function (doctor) {
        if (!doctor) {
          return res.json(doctor);
        }

        Shift
          .find({
            doctor: doctor.id
          })
          .then(function (shifts) {
            if (shifts.length === 0) {
              return res.json(doctor);
            }

            _.map(shifts, function (v) {
              if (v.id == values.shift) {
                var tmp = {};
                    tmp['date'] = values.schedule,
                    tmp['queue'] = 1;
                    tmp['time'] = v.startHour

                _.extend(tmp, v);
                doctor['shift'] = tmp;
              }
            });

            Schedule
              .find({
                 shift: _.pluck(shifts, 'id')
              })
              .populate('notification')
              .then(function (schedule) {
                if (schedule.length === 0) {
                  return res.json(doctor);
                }

                Queue
                  .find({
                    schedule: _.pluck(schedule, 'id')
                  })
                  .then(function (queue) {
                    if (queue.length === 0) {
                      return res.json(doctor);
                    }

                    var moment = require('moment'),
                         range = [],
                           sum = 0;

                    _.map(queue, function (v) {
                      if (moment(v.timeIn).isValid() && moment(v.timeOut).isValid()) {
                        range.push(moment.utc(moment(moment(v.timeOut), 'DD/MM/YYYY HH:mm:ss').diff(moment(moment(v.timeIn), 'DD/MM/YYYY HH:mm:ss'))).format('HH:mm:ss'));
                      }
                    });

                    if (range.length === 0) {
                      _.map(queue, function (v) {
                        range.push('00:22:22');
                      });
                    }

                    _.map(range, function (v) {
                      sum += moment.duration(v).asSeconds();
                    });

                    _.map(schedule, function (v) {
                      if (doctor.shift.id === v.shift && doctor.shift.date === moment(v.date).format('YYYY-MM-DD')) {
                        doctor.shift.queue = _.where(queue, {schedule: v.id}).length+1;
                        doctor.shift.time = moment(doctor.shift.startHour).add(((doctor.shift.queue-1)*(Math.ceil(sum/range.length))), 'seconds').format('HH:mm');
                        doctor.shift.notif = v.notification[0];
                      }
                    });

                    return res.json(doctor);
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@confirmation#4].");
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@confirmation#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@confirmation#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [Queue@confirmation#1].");
      });
  }

};
