/**
 * ClinicController
 *
 * @description :: Server-side logic for managing clinics
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * ClinicController.find()
   *
   * --> GET /clinic
   */
  find: function (req, res) {
    // Find all clinic
    Clinic
      .find()
      .then(function (clinic) {
        // Send a 200 response back down to the client with the provided data
        return res.json(clinic);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@find#1].");
      });
  },

  /**
   * ClinicController.findOne()
   *
   * --> GET /clinic/:id
   */
  findOne: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Find clinic based on criteria
    Clinic
      .findOne(criteria)
      .populate('doctors')
      .populate('nurses')
      .then(function (clinic) {
        if (!clinic) {
          // Send a 200 response back down to the client with the provided data
          return res.json(clinic);
        }
        if (clinic.doctors.length === 0 && clinic.nurses.length === 0) {
          return res.json(clinic);
        }
        // Find doctor(s) shift(s) on that clinic
        if (clinic.doctors.length > 0) {
          Shift
            .find({
              doctor: _.pluck(clinic.doctors, 'id')
            })
            .populateAll()
            .then(function (shifts) {
              if (shifts.length > 0) {
                clinic.shifts = shifts;
              }

              if (clinic.nurses.length > 0) {
                User
                  .find({
                    id: _.pluck(clinic.nurses, 'user')
                  })
                  .then(function (users) {
                    if (users) {
                      _.map(clinic.nurses, function (v) {
                        _.map(users, function (vv) {
                          if (v.user === vv.id) {
                            v.user = vv;
                          }
                        });
                      });
                    }

                    return res.json(clinic);
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    // Send an appropriate error response back down to the client
                    return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@findOne#4].")
                  });
              } else {
                return res.json(clinic);
              }
            })
            .catch(function (errno) {
              sails.log.error(errno);
              // Send an appropriate error response back down to the client
              return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@findOne#3].")
            });
        } else if (clinic.nurses.length > 0) {
          User
            .find({
              id: _.pluck(clinic.nurses, 'user')
            })
            .then(function (users) {
              if (users) {
                _.map(clinic.nurses, function (v) {
                  _.map(users, function (vv) {
                    if (v.user === vv.id) {
                      v.user = vv;
                    }
                  });
                });
              }

              return res.json(clinic);
            })
            .catch(function (errno) {
              sails.log.error(errno);
              // Send an appropriate error response back down to the client
              return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@findOne#2].")
            });
        } else {
          return res.json(clinic);
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@findOne#1]");
      });
  },

  /**
   * ClinicController.create()
   *
   * --> POST /clinic
   */
  create: function (req, res) {
    // Assign values
    var values = {
             name: req.param('name'),
         location: req.param('location'),
      description: req.param('description')
    };
    // Assign validate
    var validate = ['', 'null', 'undefined', null, undefined];
    // Validate `name`
    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }
    // Validate `location`
    if (validate.indexOf(values.location) > -1) {
      return res.badRequest("Oops! Lokasi harus diisi.");
    }
    // Find clinic based on name of the provided values to make sure that the clinic didn't exist
    Clinic
      .findOne({
        name: values.name
      })
      .then(function (found) {
        if (found) {
          // Send a 400 response back down to the client indicating that the request is failed
          return res.badRequest("Oops! Nama klinik sudah ada.");
        }
        // Save clinic with the provided values
    		Clinic
          .create(values)
          .then(function (created) {
    				if (!created) {
    					// Send a 400 response back down to the client indicating that the request is failed
    					return res.badRequest("Oops! Data gagal disimpan. Silahkan coba lagi.");
    				}
            // Send a 200 response back down to the client with the provided data
            return res.json(created);
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@create#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@create#1].");
      });
  },

  /**
   * ClinicController.update()
   *
   * --> PUT /clinic/:id
   */
  update: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };
    // Assign values
    var values = {
             name: req.param('name'),
         location: req.param('location'),
      description: req.param('description')
    };
    // Assign validate
    var validate = ['', 'null', 'undefined', null, undefined];

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Validate `name`
    if (validate.indexOf(values.name) > -1) {
      return res.badRequest("Oops! Nama harus diisi.");
    }
    // Validate `location`
    if (validate.indexOf(values.location) > -1) {
      return res.badRequest("Oops! Lokasi harus diisi.");
    }
    // Find clinic based on criteria to make sure that the clinic is exist
    Clinic
      .findOne(criteria)
      .then(function (found) {
        if (!found) {
          // Send a 400 response back down to the client indicating that the request is invalid
          return res.badRequest('Oops! Data klinik tidak ditemukan.');
        }
        // Find clinic based on name of the provided values to make sure that the clinic didn't exist
        Clinic
          .findOne({
            name: values.name
          })
          .then(function (found) {
            if (found && found.id !== criteria.id) {
              // Send a 400 response back down to the client indicating that the request is failed
              return res.badRequest("Oops! Nama klinik sudah ada.");
            }
            // Update clinic based on criteria with the provided values
            Clinic
              .update(criteria, values)
              .then(function (updated) {
    						if (!updated) {
    							// Send a 400 response back down to the client indicating that the request is failed
    							return res.badRequest('Oops! Data gagal diperbaharui. Silahkan coba lagi.');
    						}
                // Send a 200 response back down to the client
                return res.json(updated);
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@update#3].");
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@update#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@update#1].");
      });
  },

  /**
   * ClinicController.destroy()
   *
   * --> DELETE /clinic/:id
   */
  destroy: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Find clinic based on criteria to make sure that the clinic is exist
    Clinic
      .findOne(criteria)
      .then(function (found) {
        if (!found) {
          // Send a 400 response back down to the client indicating that the request is invalid
          return res.badRequest('Oops! Data klinik tidak ditemukan.');
        }
        // Delete clinic based on criteria
        Clinic
          .destroy(criteria)
          .then(function (destroyed) {
            if (!destroyed) {
							// Send a 400 response back down to the client indicating that the request is failed
              return res.badRequest('Oops! Data gagal dihapus. Silahkan coba lagi.')
            }
            // Send a 200 response back down to the client
            return res.json(destroyed);
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@destroy#2].");
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@destroy#1].");
      });
  },

  /**
   * ClinicController.schedule()
   *
   * --> POST /clinic/schedule
   */
  schedule: function (req, res) {
    // Assign criteria
    var criteria = {
      id: req.param('id')
    };
    // Assign values
    var values = {
      start: req.param('start'),
        end: req.param('end')
    };
    // Assign moment
    var moment = require('moment');
    // Validate request method
    if (req.method.toUpperCase() !== 'POST') {
      return res.status(405).send("Oops! Permintaan tidak diperbolehkan.");
    }

    if (isNaN(criteria.id)) {
      criteria.id = 0;
    }
    // Validate start of the date
    if (!moment(values.start).isValid()) {
      return res.badRequest("Oops! Tanggal awal tidak valid.");
    }
    // Validate end of the date
    if (!moment(values.end).isValid()) {
      return res.badRequest("Oops! Tanggal akhir tidak valid.");
    }
    // Find clinic based on criteria
    Clinic
      .findOne(criteria)
      .then(function (clinic) {
        if (!clinic) {
          // Send a 200 response back down to the client
          return res.json(clinic);
        }
        // Find doctor(s) of the clinic
        Doctor
          .find({
            clinic: clinic.id
          })
          .populate('shifts')
          .then(function (doctors) {
            if (doctors.length === 0) {
              // Send a 200 response back down to the client with the provided data
              return res.json(clinic);
            }
            // Find shift(s) of the doctor(s) on that clinic
            Shift
              .find({
                doctor: _.pluck(doctors, 'id')
              })
              .populate('doctor')
              .populate('schedules')
              .then(function (shifts) {
                if (shifts.length === 0) {
                  // Send a 200 response back down to the client with the provided data
                  return res.json(clinic);
                }
                // Assign start, end, dates and shift
                var start = moment(values.start).format('YYYY-MM-DD'),
                      end = moment(values.end).add(1, 'days').format('YYYY-MM-DD'),
                    dates = [],
                    shift = [];
                // Create date from `start` to `end`
                do {
                  dates.push(start);
                  start = moment(start).add(1, 'days').format('YYYY-MM-DD');
                } while (moment(start).isBefore(end, 'day'));
                // Add date to doctor shift if have the same day
                _.map(dates, function (v) {
                  _.map(shifts, function (vv) {
                    if (moment(v).day() === vv.day) {
                      // Assign tmp
                      var tmp = {};
                          tmp['date'] = v;
                          tmp['queue'] = 0;
                          tmp['time'] = vv.startHour;
                      // Copy value of currently shifts to tmp
                      _.extend(tmp, vv);
                      // Add tmp to shift array
                      shift.push(tmp);
                    }
                  });
                });
                // Add shift to clinic object
                clinic.shift = shift;
                // Find schedule(s) based on doctor(s) shift(s)
                Schedule
                  .find({
                    shift: _.pluck(shifts, 'id')
                  })
                  .populate('queues')
                  .populate('notification')
                  .then(function (schedules) {
                    if (schedules.length === 0) {
                      // Send a 200 response back down to the client with the provided data
                      return res.json(clinic);
                    }

                    Queue
                      .find({
                        schedule: _.flatten(_.pluck(schedules), 'id')
                      })
                      .populate('users')
                      .then(function (queues) {
                        _.map(_.flatten(_.pluck(schedules, 'queues')), function (v) {
                          _.map(queues, function (vv) {
                            if (v.id === vv.id) {
                              v.user = vv.users[0];
                            }
                          });
                        });

                        _.map(_.flatten(_.pluck(shifts, 'schedules')), function (v) {
                          _.map(schedules, function (vv) {
                            if (v.id === vv.id) {
                              v.queue = vv.queues;

                              if (vv.notification.length > 0) {
                                v.notif = vv.notification[vv.notification.length-1].topic;
                                v.notif_duration = vv.notification[vv.notification.length-1].duration;
                              }
                            }
                          });
                        });

                        _.map(_.flatten(_.pluck(doctors, 'shifts')), function (v) {
                          _.map(shifts, function (vv) {
                            if (v.id === vv.id) {
                              v.schedule = vv.schedules;
                            }
                          });
                        });

                        _.map(clinic.shift, function (v) {
                          _.map(doctors, function (vv) {
                            if (v.doctor.id === vv.id) {
                              var queue = _.flatten(_.pluck(_.flatten(_.pluck(vv.shifts, 'schedule')), 'queue')),
                                  range = [],
                                    sum = 0;

                              _.map(queue, function (vvv) {
                                if (moment(vvv.timeIn).isValid() && moment(vvv.timeOut).isValid()) {
                                  range.push(moment.utc(moment(moment(vvv.timeOut), 'DD/MM/YYYY HH:mm:ss').diff(moment(moment(vvv.timeIn), 'DD/MM/YYYY HH:mm:ss'))).format('HH:mm:ss'));
                                }
                              });

                              if (range.length === 0) {
                                _.map(queue, function (vvv) {
                                  range.push('00:22:22');
                                });
                              }

                              _.map(range, function (vvv) {
                                sum += moment.duration(vvv).asSeconds();
                              });

                              _.map(_.flatten(_.pluck(vv.shifts, 'schedule')), function (vvv) {
                                if (v.id === vvv.shift && v.date === moment(vvv.date).format('YYYY-MM-DD')) {
                                  v.queue = vvv.queue.length;
                                  v.users = _.flatten(_.pluck(_.flatten(_.pluck(vvv.queue, 'user')), 'id'));
                                  v.notif = vvv.notif;
                                  v.duration = vvv.notif_duration;
                                  if(moment(v.startHour).add(((v.queue)*(Math.ceil(sum/range.length))), 'seconds').isAfter(moment(v.endHour))){
                                    v.estimate = 'over';
                                  }
                                }
                              });
                              
                                if(v.notif === 'delay'){
                                  v.time = moment(v.startHour).add(((v.queue)*(Math.ceil(sum/range.length))), 'seconds').add(v.duration, 'minutes').format('HH:mm');
                                }
                                else{
                                 v.time = moment(v.startHour).add(((v.queue)*(Math.ceil(sum/range.length))), 'seconds').format('HH:mm');
                                }
                            }
                          });
                        });
                        // Send a 200 response back down to the client with the provided data
                        return res.json(clinic);
                      })
                      .catch(function (errno) {
                        sails.log.error(errno);
                        // Send an appropriate error response back down to the client
                        return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@schedule#5].")
                      });
                  })
                  .catch(function (errno) {
                    sails.log.error(errno);
                    // Send an appropriate error response back down to the client
                    return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@schedule#4].")
                  });
              })
              .catch(function (errno) {
                sails.log.error(errno);
                // Send an appropriate error response back down to the client
                return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@schedule#3].")
              });
          })
          .catch(function (errno) {
            sails.log.error(errno);
            // Send an appropriate error response back down to the client
            return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@schedule#2].")
          });
      })
      .catch(function (errno) {
        sails.log.error(errno);
        // Send an appropriate error response back down to the client
        return res.negotiate("Oops! Terjadi kesalahan sistem [Clinic@schedule#1].")
      });
  }

};
