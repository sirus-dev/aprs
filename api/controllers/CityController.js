/**
 * CityController
 *
 * @description :: Server-side logic for managing cities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	find: function (req, res) {
	  City
      .find()
      .then(function (founds) {
        return res.json(founds);
      })
      .catch(function (errno) {
        sails.log.error(errno);
        return res.negotiate("Oops! Terjadi kesalahan sistem [City@find#1].");
      });
	}

};
