/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {

  if (!req.isAuthenticated()) {
    return res.status(403).send("Session telah berakhir. Silahkan login.");
  }

  if (!(req.headers && req.headers.authorization)) {
    return res.status(409).send("Token tidak ditemukan. Silahkan login.");
  }

  var     jwt = require('jwt-simple'),
       moment = require('moment'),
       header = req.headers.authorization.split(' '),
        token = header[1],
      payload = jwt.decode(token, sails.config.secret.token),
          now = moment().unix();

  if (now > payload.exp) {
    return res.status(419).send("Token telah kadaluarsa. Silahkan login.");
  }

  User
    .findOne({
        id: payload.sub,
      role: 'admin'
    })
    .then(function (found) {
      if (!found) {
        return res.status(410).send("User tidak ditemukan. Silahkan login.");
      }

      req.user = found;
      return next();
    })
    .catch(function (errno) {
      sails.log.error(errno);
    });

  // User is not allowed
  // (default res.forbidden() behavior can be overridden in `config/403.js`)
  // return res.forbidden('You are not permitted to perform this action.');
};
