/**
* Shift.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    day: {
      type: 'integer',
      required: true
    },
    startHour: {
      type: 'datetime',
      required: true
    },
    endHour: {
      type: 'datetime',
      required: true
    },
    doctor: {
      model: 'Doctor',
      columnName: 'doctor_id',
      type: 'integer',
      required: true
    },
    nurses: {
      collection: 'NurseShift',
      via: 'shift'
    },
    schedules: {
      collection: 'Schedule',
      via: 'shift'
    }
  },

  afterDestroy: function (destroyed, callback) {
    NurseShift
      .find({
        shift: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          NurseShift
            .destroy({
              shift: _.pluck(destroyed, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    // find schedule(s) of the shift
    Schedule
      .find({
        shift: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          // delete schedule(s) of the shift
          Schedule
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  }

};
