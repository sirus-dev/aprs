/**
* Notification.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    creator: {
      type: 'string',
      required: true,
      size: 45
    },
    topic: {
      type: 'string',
      in: ['delay','cancel','other'],
      required: true
    },
    duration: {
      type: 'integer'
    },
    content: {
      type: 'text',
      required: true
    },
    status: {
      type: 'boolean'
    },
    schedule: {
      model: 'Schedule',
      columnName: 'schedule_id',
      type: 'integer',
      required: true
    }
  }
};
