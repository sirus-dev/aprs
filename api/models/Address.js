/**
* Address.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    address: {
      type: 'text',
      defaultsTo: '-'
    },
    user: {
      model: 'User',
      columnName: 'user_id',
      type: 'integer',
      required: true
    },
    city: {
      model: 'City',
      columnName: 'city_id',
      type: 'integer',
      required: true
    }
  }
};
