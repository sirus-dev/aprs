/**
* Synchronize.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    domain: {
      type: 'string',
      size: 60,
      required: true
    },
    key: {
      type: 'string',
      size: 24,
      required: true
    }
   }
};

