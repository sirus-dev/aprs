/**
* Security.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    number: {
      type: 'string',
      required: true
    },
    user: {
      model: 'User',
      columnName: 'user_id',
      type: 'integer',
      required: true
    },
    securityType: {
      model: 'SecurityType',
      columnName: 'securityType_id',
      type: 'integer',
      required: true
    }
  }

};
