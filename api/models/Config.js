/**
* Config.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    smsUrl: {
      type: 'string',
      size: 45,
      required: true
    },
    smsUsername: {
      type: 'string',
      size: 45,
      required: true
    },
    smsPassword: {
      type: 'string',
      size: 100,
      required: true
    },
    emailName: {
      type: 'string',
      size: 45,
      required: true
    },
    emailUserDomain: {
      type: 'email',
      size: 45,
      required: true
    }
  }
};
