/**
* Queue.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    number: {
      type: 'integer'
    },
    registration_code: {
      type: 'string',
      size: 4
    },
    estimate: {
      type: 'datetime'
    },
    isPresent: {
      type: 'boolean'
    },
    timeIn: {
      type: 'datetime'
    },
    timeOut: {
      type: 'datetime'
    },
    schedule: {
      model: 'Schedule',
      columnName: 'schedule_id',
      type: 'integer',
      required: true
    },
    users: {
      collection: 'User',
      via: 'queues'
    }
  },

  afterDestroy: function (destroyed, callback) {
    Schedule
      .find({
        id: _.pluck(destroyed, 'schedule_id')
      })
      .populate('queues')
      .then(function (schedules) {
        if (schedules.length > 0) {
          var _schedules = [];

          _.map(schedules, function (v) {
            if (v.queues.length === 0) {
              _schedules.push(v.id);
            }
          });

          if (_schedules.length > 0) {
            Schedule
              .destroy({
                id: _schedules
              })
              .then(function (destroyed) {
                //
              })
              .catch(function (errno) {
                sails.log.error(errno);
              });
          }
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  }

};
