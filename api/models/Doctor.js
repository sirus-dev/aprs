/**
* Doctor.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    title: {
      type: 'string',
      size: 10,
      required: true
    },
    name: {
      type: 'string',
      size: 45,
      required: true
    },
    desc: {
      type: 'text'
    },
    picture: {
      type: 'string',
      size: 45
    },
    clinic: {
      model: 'Clinic',
      columnName: 'clinic_id',
      type: 'integer',
      required: true
    },
    shifts: {
      collection: 'Shift',
      via: 'doctor'
    }
  },

  // beforeValidate: function (values, callback) {
  //   if (values.title) {
  //     values.title += values.title.charAt(values.title.length-1) === '.' ? '' : '.';
  //   }
  //
  //   if (values.name) {
  //     values.name = (values.name.toLowerCase() + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
  //       return $1.toUpperCase();
  //     });
  //   }
  //
  //   return callback();
  // },

  afterDestroy: function (destroyed, callback) {
    // find shift(s) of the doctor
    Shift
      .find({
        doctor: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          // delete shift(s) of the doctor
          Shift
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  }

};
