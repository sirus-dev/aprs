/**
* Clinic.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    name: {
      type: 'string',
      size: 45,
      required: true
    },
    location: {
      type: 'text',
      required: true
    },
    description: {
      type: 'text'
    },
    doctors: {
      collection: 'Doctor',
      via: 'clinic'
    },
    nurses: {
      collection: 'Nurse',
      via: 'clinic'
    }
  },

  beforeValidate: function (values, callback) {
    if (values.name) {
      values.name = (values.name.toLowerCase() + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
        return $1.toUpperCase();
      });
    }

    return callback();
  },

  afterDestroy: function (destroyed, callback) {
    // find doctor(s) on that clinic
    Doctor
      .find({
        clinic: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          // delete picture(s) of the doctor(s)
          _.map(_.pluck(founds, 'picture'), function (filename) {
            try {
              require('fs')
                .unlinkSync(sails.config.file.path + filename);
              require('fs')
                .unlinkSync(sails.config.file.temp + filename);
            } catch (errno) {
              sails.log.warn(errno);
            }
          });
          // delete data doctor(s)
          Doctor
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    // find nurse(s) on that clinic
    Nurse
      .find({
        clinic: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          // delete data nurse(s)
          Nurse
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  }

};
