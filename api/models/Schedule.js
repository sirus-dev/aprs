/**
* Schedule.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    date: {
      type: 'date'
    },
    status: {
      type: 'string',
      in: ['ok', 'canceled', 'late', 'etc']
    },
    shift: {
      model: 'Shift',
      columnName: 'shift_id',
      type: 'integer',
      required: true
    },
    queues: {
      collection: 'Queue',
      via: 'schedule'
    },
    notification: {
      collection: 'Notification',
      via: 'schedule'
    }
  },

  afterDestroy: function (destroyed, callback) {
    // find queue(s) of the schedule
    Queue
      .find({
        schedule: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          // delete queue(s) of the schedule
          Queue
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    Notification
      .find({
        schedule: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          Notification
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  }

};
