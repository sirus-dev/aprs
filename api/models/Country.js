/**
* Country.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    name: {
      type: 'string'
    },
    provinces: {
      collection: 'Province',
      via: 'country'
    }
  },

  beforeValidate: function (values, callback) {
    if (values.name) {
      values.name = (values.name.toLowerCase() + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
        return $1.toUpperCase();
      });
    }

    return callback();
  }
};
