/**
* SecurityType.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    type: {
      type: 'string',
      size: 45,
      required: true
    },
    security: {
      collection: 'Security',
      via: 'securityType'
    }
  }

};
