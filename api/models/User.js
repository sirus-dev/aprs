/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    email: {
      type: 'string'
    },
    name: {
      type: 'string',
      size: 45
    },
    birthdate: {
      type: 'date',
      defaultsTo: new Date(1980,0,1)
    },
    age: {
      type: 'integer'
    },
    gender: {
      type: 'string',
      in: ['M','F'],
      defaultsTo: 'F'
    },
    phone: {
      type: 'string',
      size: 45,
      defaultsTo: '-'
    },
    isRegistered: {
      type: 'boolean',
      defaultsTo: false
    },
    role: {
      type: 'string',
      in: ['admin', 'nurse', 'patient']
    },
    auth: {
      collection: 'Auth',
      via: 'user'
    },
    address: {
      collection: 'Address',
      via: 'user'
    },
    queues: {
      collection: 'Queue',
      via: 'users'
    },
    security: {
      collection: 'Security',
      via: 'user'
    }
  },

  beforeValidate: function (values, callback) {
    if (values.name) {
      values.name = (values.name.toLowerCase() + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
        return $1.toUpperCase();
      });
    }

    return callback();
  },

  afterCreate: function (newlyInsertedRecord, callback) {
    
    setTimeout(function () {
      SecurityType
        .findOrCreate({
          type: 'ktp'
        })
        .then(function (type) {
          if (type) {
            Security
              .findOne({
                securityType: type.id,
                        user: newlyInsertedRecord.id
              })
              .then(function (security) {
                if (!security) {
                  Security
                    .create({
                            number: '-',
                      securityType: type.id,
                              user: newlyInsertedRecord.id
                    })
                    .then(function (created) {
                      //
                    })
                    .catch(function (errno) {
                      sails.log.error(errno);
                    });
                }
              })
              .catch(function (errno) {
                sails.log.error(errno);
              });
          }
        })
        .catch(function (errno) {
          sails.log.error(errno);
        });

      City
        .find()
        .then(function (cities) {
          Address
            .findOrCreate({
              address: '-',
                 city: cities[Math.floor((Math.random() * cities.length + 1))].id,
                 user: newlyInsertedRecord.id
            })
            .then(function (created) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        })
        .catch(function (errno) {
          sails.log.error(errno);
        });

      var moment = require('moment');

      User
        .update({
          id: newlyInsertedRecord.id
        },{
          age: moment().diff(moment(newlyInsertedRecord.birthdate), 'years')
        })
        .then(function (updated) {
          //
        })
        .catch(function (errno) {
          sails.log.error(errno);
        });
    }, 3000);

    return callback();
  },

  afterUpdate: function (updatedRecord, callback) {
    var moment = require('moment');

    User
      .update({
        id: updatedRecord.id
      },{
        age: moment().diff(moment(updatedRecord.birthdate), 'years')
      })
      .then(function (updated) {
        //
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  },

  beforeDestroy: function (criteria, callback) {
    User
      .findOne(criteria)
      .populate('queues')
      .then(function (found) {
        if (found) {
          var queues = _.pluck(found.queues, 'id');

          if (queues.length > 0) {
            Queue
              .destroy({
                id: queues
              })
              .then(function (deleted) {
                //
              })
              .catch(function (errno) {
                sails.log.error(errno);
              });
          }
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  },

  afterDestroy: function (destroyed, callback) {
    Auth
      .find({
        user: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          Auth
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    Address
      .find({
        user: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          Address
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    Security
      .find({
        user: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          Security
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  }

};
