/**
* NurseShift.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  tableName: 'nurse_has_shift',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    nurse: {
      model: 'Nurse',
      columnName: 'nurse_id',
      type: 'integer',
      required: true
    },
    shift: {
      model: 'Shift',
      columnName: 'shift_id',
      type: 'integer',
      required: true
    }
  }
  
};
