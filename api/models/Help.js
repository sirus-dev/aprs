/**
* Help.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    email: {
      type: 'string'
    },
  	title: {
      type: 'string',
      required: true
    },
    content: {
      type: 'string',
      required: true
    },
    answer: {
      type: 'text'
    },
    status: {
      type: 'string',
      size: 20,
    },
  }
};