/**
* Log.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    patient: {
      type: 'string',
      size: 45,
      required: true
    },
    doctor: {
      type: 'string',
      size: 45,
      required: true
    },
    clinic: {
      type: 'string',
      size: 45,
      required: true
    },
    date: {
      type: 'datetime',
      required: true
    },
    timeIn: {
      type: 'datetime'
    },
    timeOut: {
      type: 'datetime'
    }
  }
};
