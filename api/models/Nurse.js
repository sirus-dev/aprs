/**
* Nurse.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    user: {
      model: 'User',
      columnName: 'user_id',
      type: 'integer',
      required: true
    },
    clinic: {
      model: 'Clinic',
      columnName: 'clinic_id',
      type: 'integer',
      required: true
    },
    shifts: {
      collection: 'NurseShift',
      via: 'nurse'
    }
  },

  afterCreate: function (newlyInsertedRecord, callback) {
    setTimeout(function () {
      Config
        .find()
        .then(function (config) {
          if (config.length > 0) {
            User
              .findOne({
                id: newlyInsertedRecord.user
              })
              .then(function (user) {
                if (user && user.role === 'nurse') {
                  var password;

                  do {
                    password = Math.floor((Math.random() * Math.pow(10, 6) + 1));
                  } while (password.length < 6);

                  Auth
                    .create({
                          user: newlyInsertedRecord.user,
                      password: password
                    })
                    .then(function (auth) {
                      if (auth) {
                        require('nodemailer')
                          .createTransport().sendMail({
                                 from: config[0].emailName +' <'+ config[0].emailUserDomain + '>', // sender address
                                   to: user.email, // list of receivers
                              subject: "Akun Dijital Perawat Anda - RS MARY Cileungsi Hijau", // Subject line
                                 html: "<br>Yth " + user.name +
                                       "<br><br>Rumah sakit Anda telah menerapkan sistem IT baru untuk manajemen antrian pasien. Anda diharapkan untuk ikut berpartisipasi menggunakan sistem ini demi kemudahan bersama." +
                                       "<br><br>Mulailah dengan masuk ke " + sails.getBaseurl() + '/perawat' + " melalui smartphone / tablet Anda, kemudian login menggunakan data berikut." +
                                       "<br><br><b>Email: " + user.email +
                                       "<br>Password : " + password +
                                       "</b><br><br>Demi keamanan, mohon segera mengganti password Anda setelah masuk ke dalam sistem." +
                                       "<br><br>Jika masih ada pertanyaan, silahkan hubungi petugas IT Anda di [technical support email] atau hubungi [technical support number]." +
                                       "<br><br>Terima kasih," +
                                       "<br><br>RS MARY Cileungsi Hijau"
                                 }, function (err, info) {
                            if (err) {
                              sails.log.error(err);
                            } else {
                              sails.log.info(info);
                            }
                          });
                      }
                    })
                    .catch(function (errno) {
                      sails.log.error(errno);
                    });
                }
              })
              .catch(function (errno) {
                sails.log.error(errno);
              });
          }
        })
        .catch(function (errno) {
          sails.log.error(errno);
        });
    }, 2000);

    return callback();
  },

  afterDestroy: function (destroyed, callback) {
    // find shift(s) of the nurse
    NurseShift
      .find({
        nurse: _.pluck(destroyed, 'id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          // delete shift(s) of the nurse
          NurseShift
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    User
      .find({
        id: _.pluck(destroyed, 'user_id')
      })
      .then(function (founds) {
        if (founds.length > 0) {
          User
            .destroy({
              id: _.pluck(founds, 'id')
            })
            .then(function (deleted) {
              //
            })
            .catch(function (errno) {
              sails.log.error(errno);
            });
        }
      })
      .catch(function (errno) {
        sails.log.error(errno);
      });

    return callback();
  }

};
