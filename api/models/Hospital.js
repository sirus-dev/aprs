/**
* Hospital.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  	attributes: {
    name: {
      type: 'string',
      size: 45,
      required: true
    },
    address: {
      type: 'text'
    },
    description: {
      type: 'text'
    },
    email: {
      type: 'email',
      size: 45,
      unique: true
    },
    phone: {
      type: 'string',
      size: 45
    },
    favicon: {
      type: 'string',
      size: 45
    },
    logo: {
      type: 'string',
      size: 45
    },
    banner: {
      type: 'string',
      size: 45
    }
   }
};

