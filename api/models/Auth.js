/**
* Auth.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    password: {
      type: 'string',
      size: 100
    },
    activationCode: {
      type: 'string',
      size: 45
    },
    resetPassCode: {
      type: 'string',
      size: 45
    },
    user: {
      model: 'User',
      columnName: 'user_id',
      type: 'integer',
      required: true
    },
    toJSON: function() {
      var obj = this.toObject();

      delete obj.password;
      return obj;
    }
  },

  beforeCreate: function (values, callback) {
    if (!values.password) {
      return callback();
    }

    // Encrypt password
    require('bcrypt').genSalt(10, function (err, salt) {
      if (err) {
        return callback(err);
      }

      require('bcrypt').hash(values.password, salt, function(err, hash) {
        if (err) {
          // Calling callback() with an argument returns an error.
          // Useful for canceling the entire operation if some criteria fails.
          return callback(err);
        }

        values.password = hash;
        callback();
      });
    });
  },

  // afterCreate: function (newlyInsertedRecord, callback) {
  //   setTimeout(function () {
  //     Config
  //       .find()
  //       .then(function (config) {
  //         if (config.length > 0) {
  //           User
  //             .findOne({
  //               id: newlyInsertedRecord.user
  //             })
  //             .then(function (user) {
  //               if (user && user.role === 'patient') {
  //                 var code = Math.floor((Math.random() * Math.pow(10, 20) + 1));

  //                 Auth
  //                   .update({
  //                     id: newlyInsertedRecord.id
  //                   },{
  //                     activationCode: code
  //                   })
  //                   .then(function (auth) {
  //                     if (auth) {
  //                       require('nodemailer')
  //                         .createTransport().sendMail({
  //                                from: config[0].emailName +' <'+ config[0].emailUserDomain + '>', // sender address
  //                                  to: user.email, // list of receivers
  //                             subject: "Aktivasi Akun Pasien", // Subject line
  //                                html: "<h3>Selamat, Anda telah berhasil terdaftar</h3>" +
  //                                      "<br>Untuk mengaktifkan akun Anda, silahkan klik pada link berikut:" +
  //                                      "<br>" +
  //                                      "<br>" +
  //                                      sails.getBaseurl() + '/#/register/' + code + '$' + user.id +
  //                                      "<br>" +
  //                                      "<br>" +
  //                                      "Terima Kasih"
  //                         }, function (err, info) {
  //                           if (err) {
  //                             sails.log.error(err);
  //                           } else {
  //                             sails.log.info(info);
  //                           }
  //                         });
  //                     }
  //                   })
  //                   .catch(function (errno) {
  //                     sails.log.error(errno);
  //                   });
  //               }
  //             })
  //             .catch(function (errno) {
  //               sails.log.error(errno);
  //             });
  //         }
  //       })
  //       .catch(function (errno) {
  //         sails.log.error(errno);
  //       });
  //   }, 2000);

  //   return callback();
  // },

  beforeUpdate: function (values, callback) {
    if (!values.password) {
      return callback();
    }

    // Encrypt password
    require('bcrypt').genSalt(10, function (err, salt) {
      if (err) {
        return callback(err);
      }

      require('bcrypt').hash(values.password, salt, function(err, hash) {
        if (err) {
          // Calling callback() with an argument returns an error.
          // Useful for canceling the entire operation if some criteria fails.
          return callback(err);
        }

        values.password = hash;
        callback();
      });
    });
  }

};
