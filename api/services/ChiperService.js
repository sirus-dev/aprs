module.exports = {

  createToken: function (user) {
    var     jwt = require('jwt-simple'),
         moment = require('moment'),
        payload = {
          sub: user.id,
          iat: moment().unix(),
          exp: moment().add(3, 'days').unix()
        };

    return jwt.encode(payload, sails.config.secret.token);
  }

};
