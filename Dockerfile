FROM node:8.15.0-alpine
# RUN mkdir /app
# WORKDIR /app
# COPY package*.json ./
COPY ./ ./ 
RUN apk add --no-cache make gcc g++ python git
RUN npm install
RUN npm install -g bower
RUN bower cache clean --allow-root 
RUN bower install --allow-root 
EXPOSE 3000
ENTRYPOINT [ "npm", "run"]
CMD [ "start"]