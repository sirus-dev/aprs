module.exports.file = {
  size: 2000000, // ~2Mb
  path: require('path').resolve(__dirname, '../assets/images/doctors') + '/',
  // temp: require('path').resolve(__dirname, (process.env.NODE_ENV === 'production' ? '../www' : '../.tmp/public') + '/images/doctors') + '/'
  temp: require('path').resolve(__dirname, '../.tmp/public/images/doctors') + '/'
};

module.exports.files = {
  size: 1500000, // ~1,5Mb
  path: require('path').resolve(__dirname, '../assets/images/RS') + '/',
  // temp: require('path').resolve(__dirname, (process.env.NODE_ENV === 'production' ? '../www' : '../.tmp/public') + '/images/RS') + '/'
  temp: require('path').resolve(__dirname, '../.tmp/public/images/RS') + '/'
};