/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions (`true` allows public     *
  * access)                                                                  *
  *                                                                          *
  ***************************************************************************/

  '*': true,

  // Controllers
  'AuthController':{
    'find' : 'isAdmin'
  },
  'LogController':{
    'find' : ['isAdmin','isNurse','isNurse']
  },
  'AddressController':{
    'find' : ['isAdmin','isNurse','isNurse']
  },
  'UserController':{
    'find' : 'isAdmin',
    'queue': 'isPatient'
  },
  'SynchronizeController':{
    'find': 'isAdmin'
  },
  'ClinicController': {
     'create': 'isAdmin',
     'update': 'isAdmin',
    'destroy': 'isAdmin'
  },
  'ConfigController': {
     'create': 'isAdmin',
     'update': 'isAdmin',
    'destroy': 'isAdmin',
      'email': 'isAdmin',
       'find': 'isAdmin'
  },
  'DoctorController': {
     'create': 'isAdmin',
     'update': 'isAdmin',
    'destroy': 'isAdmin'
  },
  'NotificationController': {
     'nurse': 'isNurse',
      'user': 'isPatient',
    'create': 'isNurse'
  },
  'NurseController': {
     'create': 'isAdmin',
     'update': 'isAdmin',
    'destroy': 'isAdmin',
       'find': 'isAdmin'
  },
  'QueueController': {
     'update': 'isNurse',
     'find' : 'isAdmin'
  },
  'ScheduleController': {
      'queue': 'isNurse',
      'find' : ['isAdmin','isNurse','isNurse']
  },
  'ShiftController': {
     'create': 'isAdmin',
     'update': 'isAdmin',
    'destroy': 'isAdmin'
  }
};
