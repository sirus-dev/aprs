/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

  var admin = {
            name: 'Administrator',
           email: 'aprs@lussa.net',
        password: 'aprsadmin',
            role: 'admin',
           phone: '-',
    isRegistered: true
  };

  var hospital = {
    name: 'APRS',
    description : 'Booking jadwal di APRS. Anda bisa memesan jadwal konsultasi berdasarkan waktu yang anda harapkan, anda juga bisa memilih klinik/poli bahkan dokter yang anda inginkan.',
    email: 'aprs@lussa.net',
    phone: '081220516169',
    favicon: 'favicon.png',
    logo: 'logo.png',
    banner: 'banner.png',
    address: 'Jl. Sulanjana no 17, Bandung'
  };

  var config = {
          emailName: 'RS MARY Cileungsi Hijau',
    emailUserDomain: 'noreply-aprs@lussa.net',
             smsUrl: 'https://reguler.zenziva.net/apps/smsapi.php',
        smsUsername: '0eshok',
        smsPassword: 'odongodong'
  };

  Config.find(function (errno, found) {
    if (!errno && found.length === 0) {
      Config.create(config, function (errno, created) {
        if (!errno && created) {
          sails.log.info("Bootstrap :: sms and email configuration created.");
        }
      });
    }
  });

  Hospital.find(function (errno, found) {
    if (!errno && found.length === 0) {
      Hospital.create(hospital, function (errno, created) {
        if (!errno && created) {
          sails.log.info("Bootstrap :: Description RS created.");
        }
      });
    }
  });

  User.findOne({role: 'admin'}, function (errno, found) {
    if (!errno && !found) {
      User.create(admin, function (errno, created) {
        if (!errno && created) {
          sails.log.info("Bootstrap :: admin information created.");
          Auth.findOne({id: created.id}, function (errno, found) {
            if (!errno && !found) {
              admin.user = created.id;
              Auth.create(admin, function (errno, created) {
                if (!errno && created) {
                  sails.log.info("Bootstrap :: admin authentication created.");
                }
              });
            }
          });
        }
      });
    }
  });

  Country.findOrCreate({name: 'Indonesia'}, function (errno, country) {
    if (!errno && country) {
      var        fs = require('fs'),
                csv = require('fast-csv'),
               path = require('path').resolve(__dirname, '../assets/csv') + '/',
          provinces = [],
             cities = [];

      csv
        .fromStream(fs.createReadStream(path + 'province.csv'), {headers: true})
        .on('data', function (data) {
          data.country = country.id;
          provinces.push(data);
        })
        .on('end', function () {
          Province
            .findOrCreate(provinces, function (err, created) {
              sails.log.info("Bootstrap :: " + created.length + " provinces created.");
            });
        });

      csv
        .fromStream(fs.createReadStream(path + 'city.csv'), {headers: true})
        .on('data', function (data) {
          cities.push(data);
        })
        .on('end', function () {
          City
            .findOrCreate(cities, function (err, created) {
              sails.log.info("Bootstrap :: " + created.length + " cities created.");
            });
        });
      
    }
  });

      var        fs = require('fs'),
                csv = require('fast-csv'),
               path = require('path').resolve(__dirname, '../assets/csv') + '/',
              helps = [];
              csv
                  .fromStream(fs.createReadStream(path + 'help.csv'), {headers: true})
                  .on('data', function (data) {
                    helps.push(data);
                  })
                  .on('end', function () {
                    Help
                      .findOrCreate(helps, function (err, help) {
                        sails.log.info("Bootstrap :: helps created.");
                      });
                  });

  return cb();
};
