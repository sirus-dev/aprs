var      passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
           bcrypt = require('bcrypt');

passport
  .serializeUser(function(user, done) {
    done(null, user.id);
  });

passport
  .deserializeUser(function(id, done) {
    User
      .findOne({
        id: id
      }, function (err, user) {
        if (err || !user) {
          done(err, null);
        } else {
          done(null, user);
        }
      });
  });

/**
 * Sign in using Email and Password.
 */
passport
  .use(new LocalStrategy ({
        usernameField: 'email',
        passwordField: 'password',
    passReqToCallback: true
  }, function(req, email, password, done) {
    
    User
      .findOne({
        email: email
      }, function (err, patient) {
        if (err) {
          return done(err);
        }

        if (!patient || !patient.isRegistered) {

        User
          .findOne({
            phone: email
          }, function (err, user){
            var no = email * 0;
            console.log(no);
            if ((!user || !user.isRegistered) && no === 0){
                return done(null, false, 'Oops! No hp belum terdaftar.');
            } else if (!user || !user.isRegistered){
              return done(null, false, 'Oops! Email belum terdaftar.');
            }

            else{
        Auth
          .findOne({
            user: user.id
          }, function (err, auth) {
            if (err) {
              return done(err);
            }

            if (!auth || auth.activationCode !== null) {
              return done(null, false, 'Oops! Akun anda belum aktif. Silahkan aktivasi.');
            }

            bcrypt
              .compare(password, auth.password, function (err, res) {
                if (!res) {
                  return done(null, false, 'Oops! No hp dan Password tidak sesuai.');
                }

                return done(null, user, 'Login berhasil.');
            });
          });
            }
          });
          
        }
        else{

        var roles = ['admin', 'nurse', 'patient', 'admin', 'perawat', 'pasien'];

        if (roles.indexOf(roles[req.param('role')]) === -1) {
          return done(null, false, 'Oops! Role tidak ditemukan.');
        }

        if (patient.role !== roles[req.param('role')]) {
          return done(null, false, 'Oops! Anda tidak terdaftar sebagai ' + roles[Number(req.param('role'))+3] + '.');
        }

        Auth
          .findOne({
            user: patient.id
          }, function (err, auth) {
            if (err) {
              return done(err);
            }

            if (!auth || auth.activationCode !== null) {
              return done(null, false, 'Oops! Akun anda belum aktif. Silahkan aktivasi.');
            }

            bcrypt
              .compare(password, auth.password, function (err, res) {
                if (!res) {
                  return done(null, false, 'Oops! Email dan Password tidak sesuai.');
                }

                return done(null, patient, 'Login berhasil.');
            });
          });
        }
      });
    }
));

module.exports.secret = {
  token: process.env.token || "02fc291523f98923e9d7285a82889d480bb889c8"
};
