'use strict';

/**
 * Angular controller for dashboard nurse
 */
angular
	.module('nurseApp')
	.controller('HomeNotificationCtrl', ['$rootScope', '$scope', '$route', '$location', '$window', '$filter', 'TableParams', 'api',
		function ($rootScope, $scope, $route, $location, $window, $filter, TableParams, api) {
			$rootScope.title = "Notifikasi";

			if (!angular.isDefined($window.localStorage.nurse)) {
        $location.path('/login');
      }

			$scope.idate = new Date($route.current.params.date) == 'Invalid Date' ? new Date() : new Date($route.current.params.date);

			var data = new FormData();
			    data.append('date', $scope.idate);

			api.notification.nurse(data)
				.then(function (result) {
					var data = result.data.schedules;

					if (angular.isDefined(data)) {
						$scope.param = new TableParams({
							   // default page
							   page: 1,
							  // default per page
							  count: 10,
							 // default filtering
							 filter: {},
							// default sorting
							sorting: {}
						},{
							  total: data.length,
							getData: function ($defer, params) {
								var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
										 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

								params.total(orderedData.length);
								$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
							}
						});
					}
				});

			$scope.submit = function () {
				$location.path('/notification/' + $filter('date')($scope.idate, 'yyyy-MM-dd'));
			};
		}
	]);
