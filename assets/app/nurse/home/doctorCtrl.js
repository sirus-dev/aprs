'use strict';

/**
 * Angular controller for dashboard nurse
 */
angular
	.module('nurseApp')
	.controller('HomeDoctorCtrl', ['$rootScope', '$scope', '$route', '$location', '$window', '$filter', 'toast', 'api',
		function ($rootScope, $scope, $route, $location, $window, $filter, toast, api) {
      $rootScope.title = 'Shift Dokter';

			if (!angular.isDefined($window.localStorage.nurse)) {
        $location.path('/login');
      }

			$scope.idate = new Date($route.current.params.date) == 'Invalid Date' ? new Date() : new Date($route.current.params.date);

			var data = new FormData();
			    data.append('date', $scope.idate);

			api.nurse.queue(data)
				.then(function (result) {
					$scope.nurse = result.data;
					calcHeight();
				});

      $scope.goto = function (id) {
        $location.path('/' + $filter('date')($scope.idate, 'yyyy-MM-dd') + '/shift/' + id);
      };

			$scope.submit = function () {
				$location.path('/' + $filter('date')($scope.idate, 'yyyy-MM-dd'));
			}

			function calcHeight () {
				setTimeout(function () {
					heightsEqualizer('.equal-height-image');
					setTimeout(function () {
						heightsEqualizer('.equal-height');
					}, 1000);
				}, 1000);
			}

      function heightsEqualizer(selector) {
        var   elements = document.querySelectorAll(selector),
            max_height = 0,
                   len = 0,
                     i;

        if ((elements) && (elements.length > 0)) {
          len = elements.length;

          for (i = 0; i < len; i++) { // get max height
            elements[i].style.height = ''; // reset height attr
            if (elements[i].clientHeight > max_height) {
              max_height = elements[i].clientHeight;
            }
          }

          for (i = 0; i < len; i++) { // set max height to all elements
            elements[i].style.height = max_height + 'px';
            elements[i].style.cursor = 'pointer';
          }
        }
      }

			$(window).on('resize', function () {
				calcHeight();
			});
    }
  ]);
