'use strict';

/**
 * Angular controller for browse notification
 */
angular
  .module('nurseApp')
  .controller('CreateNotificationCtrl', ['$rootScope', '$window', '$scope', '$route', '$filter', '$location', 'TableParams', 'toast', 'api',
    function ($rootScope, $window, $scope, $route, $filter, $location, TableParams, toast, api) {
			// dummy patient data
      $scope.condition = true;
      $scope.notif={};
      $scope.user = JSON.parse($window.localStorage.nurse);
      
      api.nurse.clinic()
        .then(
          function (result) {
            $scope.nurse = result.data;
          });

      api.shift.notif()
        .then(
          function (result) {
            $scope.shifts = result.data;
          });

      api.doctor.notif()
        .then(
          function (result) {
            $scope.doctors = result.data;
          });

    $scope.submit = function () {

      var data = new FormData();
            data.append('creator', $scope.user.email);
            data.append('doctor', $scope.notif.doctor);
            data.append('day', $scope.notif.day);
            data.append('date', $scope.notif.date);
            data.append('shift', $scope.notif.shift);
            data.append('topic', $scope.notif.topic);
            data.append('content', $scope.notif.content);
            data.append('duration', $scope.notif.duration);

        api.notif.create(data)
          .then(function (result) {
            var data = result.data;
            toast.create({
                content: "Notifikasi berhasil dibuat.",
              className: 'success'
            });
            $location.path('/notification');
            },
            function (errno) {}
          );
      };

      $scope.reset = function () {
        $route.reload();
      };

  }]);
