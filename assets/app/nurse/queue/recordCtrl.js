'use strict';

/**
 * Angular controller for dashboard nurse
 */
angular
	.module('nurseApp')
	.controller('QueueRecordCtrl', ['$rootScope', '$scope', '$route', '$timeout', '$window', 'toast', 'api',
    function ($rootScope, $scope, $route, $timeout, $window, toast, api) {
			$rootScope.title = "Periksa Antrian";

			if (!angular.isDefined($window.localStorage.nurse)) {
        $location.path('/login');
      }

      var timeStart = 0,
          tmPromise,
					done = false;

      $scope.timer = '00:00:00';

      api.queue.findOne($route.current.params.queue)
        .then(function (result) {
					if (result.data) {
						init();
						$scope.queue = result.data;
            
            var data = new FormData();
            data.append('timeIn', new Date(timeStart));

          api.queue.update($route.current.params.queue, data)
            .then(function (result) {
              //
          });

					} else {
						toast.create({
							content: "Antrian tidak ditemukan.",
							className: 'error'
						});
					}
				});

      $scope.stop = function () {
        clearInterval(myVar);
        var data = new FormData();
            data.append('timeIn', new Date(timeStart));
            data.append('timeOut', new Date(getCurrentTime()));

        api.queue.update($route.current.params.queue, data)
          .then(function (result) {
            $timeout.cancel(tmPromise);
            toast.create({
                content: "Antrian " + $scope.queue.number + " telah selesai.",
              className: 'success'
            });
						done = true;
						setTimeout(function () {
							$window.history.back();
						},100);
          });
      };

      $scope.$on('$routeChangeStart', function (event) {
        if (!done) {
          var answer = confirm("Batalkan pencatatan waktu konsultasi...?");
          if (!answer) {
            event.preventDefault();
            $scope.$applyAsync();
          }
        }
      });

      function checkTime(i) {
        i = (i < 1) ? 0 : i;
        if (i < 10) { i = "0" + i; }  // add zero in front of numbers < 10
        return i;
      }

      function getCurrentTime(){
        return (new Date()).getTime();
      }

      function startTimer() {
        var h, m, s, ms;
        ms = Math.floor( ( getCurrentTime() - timeStart ) / 1000);
         h =  checkTime(Math.floor(ms / 3600));
        ms = Math.floor(ms % 3600);
         m = checkTime(Math.floor(ms / 60));
        ms = Math.floor(ms % 60);
         s = checkTime(Math.floor(ms));

        $scope.timer = h + ":" + m + ":" + s;

        // timer expired, restart timer
        tmPromise = $timeout(function () {
          startTimer();
        }, 500);
      }

      function init() {
        timeStart = (new Date()).getTime();
        startTimer();
      }

      var page = $route.current.params.queue;
      var myVar = setInterval(function(){
      api.queue.stoped($route.current.params.queue)
        .then(
          function (result) {
            $scope.stoped = result.data;
            if ($scope.stoped.timeOut === null || $scope.stoped.timeOut === undefined)
            { 
              if (page !== $route.current.params.queue){
                clearInterval(myVar);
              }
            }
            else {
              clearInterval(myVar);
              var data = new FormData();
                data.append('timeIn', $scope.stoped.timeIn);
                data.append('timeOut', $scope.stoped.timeOut);

            api.queue.update($route.current.params.queue, data)
              .then(function (result) {
                $timeout.cancel(tmPromise);
                toast.create({
                    content: "Antrian " + $scope.queue.number + " telah selesai.",
                  className: 'success'
                });
                done = true;
                setTimeout(function () {
                  $window.history.back();
                },100);
              });
              
            }
          });
      }, 7000);


    }
  ]);
