'use strict';

/**
 * Angular controller for dashboard nurse
 */
angular
	.module('nurseApp')
	.controller('QueuePatientCtrl', ['$rootScope', '$scope', '$location', '$route', '$window', '$filter', 'TableParams', 'api',
		function ($rootScope, $scope, $location, $route, $window, $filter, TableParams, api) {
			$rootScope.title = "Antrian";

			if (!angular.isDefined($window.localStorage.nurse)) {
        $location.path('/login');
      }

			var data = new FormData();
			    data.append('date', $route.current.params.date);

			$scope.value = 0; 

			api.nurse.queue(data)
				.then(function (result) {
					var data = null || [];
					angular.forEach(result.data.shifts, function (v) {
						if (v.id == $route.current.params.id) {
							data = v.schedule.queues.sort(function (a, b) {
		            return a.number-b.number;
		          });

							var   check = [],
							    checked = [];


							angular.forEach(data, function (v) {
								angular.isDefined(v.duration) ? checked.push(v) : check.push(v);
							});

							data = check;

							angular.forEach(checked, function (v) {
								data.push(v);
							});

							$scope.shift = v;
							$scope.notif = v.schedule.notification[0];
						}
					});
					$scope.id_schedule = data[0].schedule;
					var date = new Date($route.current.params.date);
					$scope.tdate = $filter('toDay')(date.getDay()) + ', ' + date.getDate() + ' ' + $filter('toMonth')(date.getMonth()) + ' ' + date.getFullYear();

					$scope.param = new TableParams({
						   // default page
						   page: 1,
						  // default per page
						  count: 100,
						 // default filtering
						 filter: {},
						// default sorting
						sorting: {}
					},{
						  total: data.length,
						getData: function ($defer, params) {
							var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
									 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

							params.total(orderedData.length);
							$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
						}
					});

		$scope.stop = function () {
				clearInterval(myVar);
		};

		var page = $route.current.params.id;
		var myVar = setInterval(function(){
			api.queue.nurse($scope.id_schedule)
        .then(
          function (result) {
            $scope.nurse = result.data;
            $scope.queue_id = $scope.nurse.id;
            if ($scope.nurse.queue === 'no' || $scope.nurse.timeIn === null)
            { 
            	if (page !== $route.current.params.id){
            		clearInterval(myVar);
            	}
            }
            else {
            	$location.path('/shift/' + $route.current.params.id + '/queue/' + $scope.queue_id)
            	clearInterval(myVar);
            }
          });
      }, 7000);

				});

		}
	]);
