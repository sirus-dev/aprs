'use strict';

/**
 * Angular module for dashboard nurse
 */
angular
  .module('nurseApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'lussa.ui',
    'satellizer'
  ])
  .config(['$routeProvider', '$authProvider', '$httpProvider',
    function ($routeProvider, $authProvider, $httpProvider) {
      $routeProvider
        .when('/', {
          templateUrl: '/app/nurse/home/doctor.html',
           controller: 'HomeDoctorCtrl'
        })
        .when('/login', {
          templateUrl: '/app/nurse/auth/login.html',
           controller: 'AuthLoginCtrl'
        })
        .when('/profile', {
          templateUrl: '/app/nurse/auth/profile.html',
           controller: 'AuthProfileCtrl'
        })
        .when('/shift/:shift/queue/:queue', {
          templateUrl: '/app/nurse/queue/record.html',
           controller: 'QueueRecordCtrl'
        })
        .when('/create/notification', {
          templateUrl: '/app/nurse/home/create.html',
           controller: 'CreateNotificationCtrl'
        })
        .when('/notification', {
          templateUrl: '/app/nurse/home/notification.html',
           controller: 'HomeNotificationCtrl'
        })
        .when('/notification/:date', {
          templateUrl: '/app/nurse/home/notification.html',
           controller: 'HomeNotificationCtrl'
        })
        .when('/:date', {
          templateUrl: '/app/nurse/home/doctor.html',
           controller: 'HomeDoctorCtrl'
        })
        .when('/:date/shift/:id', {
          templateUrl: '/app/nurse/queue/patient.html',
           controller: 'QueuePatientCtrl'
        })
        .otherwise({
          redirectTo: '/'
        });

        $authProvider.loginUrl = '/auth/login';
        $httpProvider.interceptors.push('httpRequestInterceptor');
     }
])
.factory('httpRequestInterceptor', ['$q', '$window', '$location', 'toast',
  function ($q, $window, $location, toast) {
    return {
      request: function (config) {
        $('.loading').show();
        return config || $q.when(config);
      },
      requestError: function (rejection) {
        return $q.reject(rejection);
      },
      response: function (response) {
        $('.loading').hide();
        return response || $q.when(response);
      },
      responseError: function (rejection) {
        $('.loading').hide();

        if (rejection.status === 500) {
          toast.create({
              content: "Terjadi kesalahan sistem. Silahkan coba beberapa saat lagi.",
            className: 'error'
          });
        }

        if (rejection.status === 400) {
          toast.create({
              content: rejection.data.replace("Oops! ",""),
            className: 'error'
          });
        }

        if ([403, 409, 410, 419].indexOf(rejection.status) > -1) {
          toast.create({
              content: rejection.data,
            className: 'error'
          });
          $location.path('/login');
        }

        return $q.reject(rejection);
      }
    };
  }
])
.factory('api', ['$http',
  function($http) {
    return {
      auth: {
        activation: function (data) {
          return $http({
                         url: '/auth/activation',
                      method: 'post',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
                        data: data
          });
        },
        forget_admin: function (data) {
          return $http({
                         url: '/auth/forget_admin',
                      method: 'post',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
                        data: data
          });
        },
        reset_admin: function (data) {
          return $http({
                         url: '/auth/reset_admin',
                      method: 'post',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
                        data: data
          });
        },
        change: function (data) {
          return $http({
                         url: '/auth/change',
                      method: 'post',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
                        data: data
          });
        }
      },
      doctor: {
          notif: function () {
            return $http({
                           url: '/doctor/notif',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
      },
      shift: {
          notif: function () {
            return $http({
                           url: '/shift/notif',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
      },
      notif: {
          create: function (data) {
            return $http({
                           url: '/notification',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
        }
      },
      notification: {
        nurse: function (data) {
          return $http({
                         url: '/notification/nurse',
                      method: 'post',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
                        data: data
          });
        }
      },
      queue: {
        findOne: function (id) {
          return $http({
                         url: '/queue/' + id,
                      method: 'get',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity
          });
        },
        update: function (id, data) {
          return $http({
                         url: '/queue/' + id,
                      method: 'put',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
                        data: data
          });
        },
        nurse: function (id) {
            return $http({
                           url: '/queue/nurse/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
        stoped: function (id) {
            return $http({
                           url: '/queue/stoped/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
      },
      nurse: {
        clinic: function () {
            return $http({
                           url: '/nurse/clinic',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
        queue: function (data) {
          return $http({
                         url: '/nurse/queue',
                      method: 'post',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
                        data: data
          });
        }
      },
      user: {
        change: function (data) {
          return $http({
                         url: '/user/change',
                      method: 'post',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
                        data: data
          });
        },
        profile: function () {
          return $http({
                         url: '/user/profile',
                      method: 'get',
             withCredentials: true,
                     headers: {'Content-Type': undefined},
            transformRequest: angular.identity
          });
        }
      }
    }
  }
])
.constant('IMAGE', {
  PATH: '/images/doctors/',
  SIZE: '2000000', // ~2Mb
  TYPE: [
    'image/png',
    'image/jpg',
    'image/jpeg'
  ]
})
.constant('IMAGE_RS', {
    PATH: '/images/RS/',
    SIZE: '80000', // ~80 kb
    TYPE: [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]
  })
.constant('DAYS', [
    'Minggu',
    'Senin',
    'Selasa',
    'Rabu',
    'Kamis',
    'Jumat',
    'Sabtu'
])
.constant('MONTHS', [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
])
.filter('noImage',
  function () {
    return function (input) {
      if (angular.isUndefined(input) || input === null || input === '') {
        return '-.png';
      }

      return input;
    }
  }
)
.filter('toDay', ['DAYS',
  function (DAYS) {
    return function (input) {
      if (angular.isNumber(input)) {
        return DAYS[input];
      }

      return input;
    }
  }
])
.filter('toHour', ['$filter',
    function ($filter) {
      return function (input) {
        return $filter('date')(input, 'HH:mm');
      }
    }
])
.filter('toMonth', ['MONTHS',
  function (MONTHS) {
    return function (input) {
      if (angular.isNumber(input)) {
        return MONTHS[input];
      }

      return input;
    }
  }
])
.run(['$rootScope', '$window', '$auth', '$location', '$http', 'IMAGE_RS', 'IMAGE', 'DAYS', 'MONTHS',
  function ($rootScope, $window, $auth, $location, $http, IMAGE_RS, IMAGE, DAYS, MONTHS) {
    $rootScope.$on('$routeChangeStart', function () {
      $window.scrollTo(0, 0);
      if ($auth.isAuthenticated() && angular.isDefined($window.localStorage.nurse)) {
        $rootScope.USER = JSON.parse($window.localStorage.nurse);
        $rootScope.IMAGE = IMAGE;
        $rootScope.DAYS = DAYS;
        $rootScope.IMAGE_RS = IMAGE_RS;
        $rootScope.MONTHS = MONTHS;
        $http({
                        url: '/hospital',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
      }).then(
        function (rs) {
          $rootScope.hospital = rs.data[0];
        });
      } else {
        $location.path('/login');
      }
    });
    $('.logout').on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
      $http.get('/auth/logout')
        .then(function (result) {
          $location.path('/login');
        });
    });
  }
]);
