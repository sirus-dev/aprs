'use strict';

/**
 * Angular controller for auth login
 */
angular
  .module('nurseApp')
  .controller('AuthProfileCtrl', ['$rootScope', '$scope', '$window', '$auth', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $location, $route, toast, api) {
      $rootScope.title = "Profil";

      if (!angular.isDefined($window.localStorage.nurse)) {
        $location.path('/login');
      }

      api.user.profile()
        .then(function (result) {
          if (!result.data) {
            $location.path('/login');
          }
          $scope.nurse = result.data;
        });

      var temp = [];

      // edit action
      $scope.edit = function (field, form) {
        angular.copy(field, temp);
        form.edit = true;
      };

      // cancel action
      $scope.cancel = function (field, form) {
        angular.extend(field, temp);
        form.edit = false;
        form.$setPristine();
        form.$setUntouched();
      };

      // save action
      $scope.save = function (field) {
        var data = new FormData();
            data.append('name', field.name);
            data.append('email', field.email);
            data.append('phone', field.phone);
            data.append('ktp', field.ktp);
            data.append('birthdate', field.birthdate);
            data.append('gender', field.gender);
            data.append('address', field.address[0].address);
            data.append('city', field.address[0].city.id);
            data.append('role', 'nurse');

        api.user.change(data)
          .then(function (result) {
            var nurse = {
                 id: result.data[0].id,
               name: result.data[0].name,
              email: result.data[0].email
            };
            $window.localStorage.nurse = JSON.stringify(nurse);
            $rootScope.USER = JSON.parse($window.localStorage.nurse);
            toast.create({
                content: "Profil berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      $scope.asave = function (field) {
        if (!angular.equals(field.password, field.confirm)) {
          toast.create({
              content: "Konfirmasi password tidak sesuai dengan password baru.",
            className: 'error'
          });

          return;
        }

        var data = new FormData();
            data.append('current', field.current);
            data.append('password', field.password);
            data.append('confirm', field.confirm);
            data.append('role', 'nurse');

        api.auth.change(data)
          .then(function (result) {
            $auth.login({
                 email: field.email,
              password: field.password,
                  role: 1
            })
            .then(function (result) {
              $window.localStorage.nurse = JSON.stringify(result.data.user);
              $rootScope.USER = JSON.parse($window.localStorage.nurse);
              toast.create({
                  content: "Profil berhasil diperbarui.",
                className: 'success'
              });
              $route.reload();
            })
            .catch(function (errno) {
              //
            });
          });
      };
    }
  ]);
