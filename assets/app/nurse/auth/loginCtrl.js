'use strict';

/**
 * Angular controller for auth login
 */
angular
  .module('nurseApp')
  .controller('AuthLoginCtrl', ['$rootScope', '$scope', '$window', '$auth', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $location, $route, toast, api) {
      $rootScope.title = "Login";

      $auth.logout();
      delete $window.localStorage.admin;
      delete $window.localStorage.nurse;
      delete $window.localStorage.patient;
      $rootScope.USER = null;

      angular.element(document).find('body').css({'padding-top':'70px','background-color':'#08252a'});
      angular.element(document).find('nav').hide();

      $scope.login = function (field) {
        $auth.login({
             email: field.email,
          password: field.password,
              role: 1
        })
        .then(function (result) {
          delete $window.localStorage.admin;
          delete $window.localStorage.patient;
          $window.localStorage.nurse = JSON.stringify(result.data.user);
          $rootScope.USER = JSON.parse($window.localStorage.nurse);
          toast.create({
              content: "Selamat datang, " + $rootScope.USER.name,
            className: 'success'
          });
          angular.element(document).find('body').css({'padding-top':'0px','background-color':'#FFF'});
          angular.element(document).find('nav').show();
          $location.path('/');
        })
        .catch(function (errno) {
          //
        });
      };

      $scope.send = function (field) {
        var data = new FormData();
            data.append('email', field.email);
            data.append('role', 1);

        api.auth.forget_admin(data)
          .then(function (result) {
            $('form[name="forgetForm"]').addClass('sr-only');
            $('form[name="resetForm"]').removeClass('sr-only');
            toast.create({
                content: "Permintaan berhasil. Silahkan cek email.",
              className: 'success'
            });
          });
      };

      $scope.reset = function (field) {
        var data = new FormData();
            data.append('email', field.email);
            data.append('resetPassCode', field.code);
            data.append('role', 1);

        api.auth.reset_admin(data)
          .then(function (result) {
            $('form[name="resetForm"]').addClass('sr-only');
            $('form[name="submitForm"]').removeClass('sr-only');
          });
      };

      $scope.submit = function (field) {
        var data = new FormData();
            data.append('email', field.email);
            data.append('resetPassCode', field.code);
            data.append('password', field.password);
            data.append('passwordd', field.passwordd);
            data.append('role', 1);

        api.auth.reset_admin(data)
          .then(function (result) {
            $('form[name="loginForm"]').removeClass('sr-only');
            $('form[name="submitForm"]').addClass('sr-only');
            toast.create({
                content: "Reset password berhasil. Silahkan login.",
              className: 'success'
            });
          });
      };

      $('#forget').on('click', function (e) {
        e.preventDefault();e.stopPropagation();
        $('form[name="loginForm"]').addClass('sr-only');
        $('form[name="forgetForm"]').removeClass('sr-only');
      });
      $('#reset').on('click', function (e) {
        e.preventDefault();e.stopPropagation();
        $('form[name="forgetForm"]').addClass('sr-only');
        $('form[name="resetForm"]').removeClass('sr-only');
      });
      $('form[name="forgetForm"] .login').on('click', function (e) {
        e.preventDefault();e.stopPropagation();
        $('form[name="loginForm"]').removeClass('sr-only');
        $('form[name="forgetForm"]').addClass('sr-only');
      });
      $('form[name="resetForm"] .login').on('click', function (e) {
        e.preventDefault();e.stopPropagation();
        $('form[name="loginForm"]').removeClass('sr-only');
        $('form[name="resetForm"]').addClass('sr-only');
      });
      $('form[name="submitForm"] .login').on('click', function (e) {
        e.preventDefault();e.stopPropagation();
        $('form[name="loginForm"]').removeClass('sr-only');
        $('form[name="submitForm"]').addClass('sr-only');
      });
    }
  ]);
