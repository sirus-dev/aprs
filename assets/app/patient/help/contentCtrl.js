'use strict';

/**
 * Angular controller for help center
 */
angular
  .module('patientApp')
  .controller('contentCtrl', ['$rootScope', '$scope', '$route', 'api',
    function ($rootScope, $scope, $route, api) {
     api.help.findOne($route.current.params.query)
        .then(function (result) {
          $scope.help = result.data; 
    });

    }
  ]);
