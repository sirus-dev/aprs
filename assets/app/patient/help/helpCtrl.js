'use strict';

/**
 * Angular controller for help center
 */
angular
  .module('patientApp')
  .controller('helpCtrl', ['$rootScope', '$scope', '$window', '$auth', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $location, $route, toast, api) {
      
      $scope.patient = {};

      api.hospital.find()
        .then(function (result) {
          $scope.setting = result.data[0];
        });

      api.help.answer()
        .then(
          function (result) {
            $scope.helps = result.data;
          });	

      $scope.submit = function (field) {
      	if (!angular.isDefined($window.localStorage.patient)) {
        $location.path('/login');
        toast.create({
                content: "Harap login terlebih dahulu.",
              className: 'warning'
            });
      } else{
      	var data = new FormData();
            data.append('title', field.title);
            data.append('content', field.content);

        api.help.create(data)
          .then(function (result) {
            toast.create({
                content: "Pesan anda telah terkirim, terima kasih.",
              className: 'success'
            });

          });
          $route.reload();

      }
        
      };

    }
  ]);
