'use strict';

/**
 * Angular controller for booking confirmation
 */
angular
  .module('patientApp')
  .controller('BookingBrowseCtrl', ['$scope', '$filter', 'TableParams', 'api',
    function ($scope, $filter, TableParams, api) {
      api.user.queue()
        .then(function (result) {
          var data = result.data.queues;

          if (angular.isDefined(data)) {
            $scope.param = new TableParams({
  						   // default page
  						   page: 1,
  						  // default per page
  						  count: 10,
  						 // default filtering
  						 filter: {},
  						// default sorting
  						sorting: {
                createdAt: 'desc'
              }
  					},{
  						  total: data.length,
  						getData: function ($defer, params) {
  							var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
  									 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

  							params.total(orderedData.length);
  							$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
  						}
  					});
          }
        });
    }
  ]);
