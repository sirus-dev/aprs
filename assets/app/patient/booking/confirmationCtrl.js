'use strict';

/**
 * Angular controller for booking confirmation
 */
angular
  .module('patientApp')
  .controller('BookingConfirmationCtrl', ['$scope', '$route','$filter','$modal', '$location', '$auth', '$window', 'toast', 'api',
    function ($scope, $route, $filter, $modal, $location, $auth, $window, toast, api) {
      $scope.isDisabled = false;

      var    param = String($route.current.params.id).split('$', 3),
          schedule = param[2].substr(0,4) + '-' + param[2].substr(4,2) + '-' + param[2].substr(6,2);

      var data = new FormData();
          data.append('doctor', param[0]);
          data.append('shift', param[1]);
          data.append('schedule', schedule);

      api.queue.confirmation(data)
        .then(function (result) {
          $scope.doctor = result.data;
          $scope.tdate = new Date(schedule);
          $scope.today = new Date();
        });

      api.queue.riset()
          .then(function (result) {
            // result
        });

      $scope.patient = {};

      if ($auth.isAuthenticated() && angular.isDefined($window.localStorage.patient)) {
        api.user.profile()
          .then(function (result) {
            var patient = result.data;
                patient.birthdate = new Date(result.data.birthdate);

            if (angular.isDefined(patient)) {
              angular.forEach(patient.security, function (v) {
                if (angular.isDefined(v.securityType) && v.securityType.type === 'ktp') {
                  patient.ktp = v.number;
                }
              });

              $scope.patient = patient;
              $('button[type="submit"],button[type="reset"]').attr('disabled',false);
            }
          });
      }

      // confirmation Nomor HP
      $scope.submit = function (field) {
        $scope.modal = {
          title: 'Konfirmasi No HP',
           body: '<p class="help-block margin-vertical-xs"> Apakah no HP Anda ini aktif ? </p>' +
                 '<p class="text-justify margin-top-large">' + field.phone + '</p>'
        };

        $scope.modalInstance = $modal.open({
            templateUrl: 'myModalConfirmation.html',
                   size: 'small',
                  scope: $scope
        });

        $scope.ok = function () {
          var data = new FormData();
            data.append('doctor', param[0]);
            data.append('shift', param[1]);
            data.append('date', schedule);
            data.append('ktp', field.ktp);
            data.append('name', field.name);
            data.append('email', field.email);
            data.append('phone', field.phone);
            data.append('birthdate', field.birthdate);
        $scope.isDisabled = true;
        api.queue.create(data)
          .then(function (result) {
            toast.create({
              content: "Pendaftaran berhasil.",
              className: 'success'
            });
            $location.path('/booking/' + result.data.id);
          });

          $scope.modalInstance.close();
        };

        $scope.cancel = function () {
          $scope.modalInstance.dismiss();
        };
      };

      $scope.reset = function () {
        $route.reload();
      };

      $('input[name="name"]').on('change', function () {
        $scope.patient.birthdate = new Date(1980,0,1);
      });
    }
  ]);
