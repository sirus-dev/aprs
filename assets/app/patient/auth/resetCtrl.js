'use strict';

/**
 * Angular controller for booking confirmation
 */
angular
  .module('patientApp')
  .controller('AuthResetCtrl', ['$rootScope', '$scope', '$window', '$auth', '$route', '$location', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $route, $location, toast, api) {
      $auth.logout();
      delete $window.localStorage.admin;
      delete $window.localStorage.nurse;
      delete $window.localStorage.patient;
      $rootScope.USER = null;

      
      $scope.submit = function (field) {
        var data = new FormData();
            data.append('phone', field.phone);
            data.append('resetPassCode', field.code);
            data.append('password', field.password);
            data.append('passwordd', field.passwordd);
            data.append('role', 2);

        api.auth.reset(data)
          .then(function (result) {
            $location.path('/login');
            toast.create({
                content: "Reset password berhasil. Silahkan login.",
              className: 'success'
            });
          });
      };

    }
  ]);
