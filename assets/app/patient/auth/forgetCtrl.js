'use strict';

/**
 * Angular controller for booking confirmation
 */
angular
  .module('patientApp')
  .controller('AuthForgetCtrl', ['$rootScope', '$scope', '$window', '$auth', '$route', '$location', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $route, $location, toast, api) {
      $auth.logout();
      delete $window.localStorage.admin;
      delete $window.localStorage.nurse;
      delete $window.localStorage.patient;
      $rootScope.USER = null;

      $scope.send = function (field) {
        var data = new FormData();
            data.append('phone', field.phone);
            data.append('role', 2);

        api.auth.forget(data)
          .then(function (result) {
            toast.create({
                content: "Permintaan berhasil. Silahkan cek sms.",
              className: 'success'
            });
          });

        $location.path('/reset-password');
      };

    }
  ]);
