'use strict';

/**
 * Angular controller for booking confirmation
 */
angular
  .module('patientApp')
  .controller('AuthActivationCtrl', ['$rootScope', '$scope', '$window', '$auth', '$route', '$location', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $route, $location, toast, api) {
      $auth.logout();
      delete $window.localStorage.admin;
      delete $window.localStorage.nurse;
      delete $window.localStorage.patient;
      $rootScope.USER = null;

      $scope.activate = function (field) {
        var data = new FormData();
            data.append('phone', field.phone);
            data.append('activationCode', field.code);
            data.append('role', 2);

        api.auth.activation(data)
          .then(function (result) {
            $location.path('/login');
            toast.create({
                content: "Aktivasi berhasil. Silahkan login.",
              className: 'success'
            });
          });
      };
    }
  ]);