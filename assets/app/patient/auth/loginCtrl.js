'use strict';

/**
 * Angular controller for booking confirmation
 */
angular
  .module('patientApp')
  .controller('AuthLoginCtrl', ['$rootScope', '$scope', '$location', '$auth', '$window', 'toast', 'api',
    function ($rootScope, $scope, $location, $auth, $window, toast, api) {
      $auth.logout();
      delete $window.localStorage.admin;
      delete $window.localStorage.nurse;
      delete $window.localStorage.patient;
      $rootScope.USER = null;

      api.hospital.find()
        .then(function (result) {
          $scope.hospital = result.data[0];
        });
        
      $scope.login = function (field) {
        $auth.login({
             email: field.email,
          password: field.password,
              role: 2
        })
        .then(function (result) {
          delete $window.localStorage.admin;
          delete $window.localStorage.nurse;
          $window.localStorage.patient = JSON.stringify(result.data.user);
          $rootScope.USER = JSON.parse($window.localStorage.patient);
          toast.create({
              content: "Selamat datang, " + $rootScope.USER.name,
            className: 'success'
          });
          $location.path('/');
        })
        .catch(function (errno) {
          //
        });
      };
    }
  ]);
