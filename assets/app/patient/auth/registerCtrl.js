'use strict';

/**
 * Angular controller for booking confirmation
 */
angular
  .module('patientApp')
  .controller('AuthRegisterCtrl', ['$rootScope', '$scope', '$window', '$route', '$location', '$auth', 'toast', 'api',
    function ($rootScope, $scope, $window, $route, $location, $auth, toast, api) {
      $auth.logout();
      delete $window.localStorage.admin;
      delete $window.localStorage.nurse;
      delete $window.localStorage.patient;
      $rootScope.USER = null;

      if (angular.isDefined($route.current.params.code)) {
        var params = String($route.current.params.code).split('$', 2);

        var data = new FormData();
            data.append('activationCode', params[0]);
            data.append('id', params[1]);
            data.append('role', 2);

        api.auth.activation(data)
          .then(function (result) {
            $route.current.params = null;
            toast.create({
                content: "Aktivasi berhasil. Silahkan login.",
              className: 'success'
            });
            $location.path('/login');
          });
      }

      $scope.register = function (field) {
        var data = new FormData();
            data.append('name', field.name);
            data.append('phone', field.phone);
            data.append('password', field.password);
            data.append('passwordd', field.passwordd);
            data.append('role', 'patient');

        api.auth.register(data)
          .then(function (result) {
            toast.create({
                content: "Pendaftaran berhasil. Silahkan cek sms.",
              className: 'success'
            });
            $location.path('/activation');
          });
      };
    }
  ]);
