'use strict';

/**
 * Angular controller for view patient
 */
angular
  .module('patientApp')
  .controller('HomeProfileCtrl', ['$rootScope', '$scope', '$window', '$auth', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $location, $route, toast, api) {
      $scope.patient = {};

      if (!angular.isDefined($window.localStorage.patient)) {
        $location.path('/login');
      }

      api.user.profile()
        .then(function (result) {
          if (!result.data) {
            $location.path('/login');
          }

          var patient = result.data;
              patient.birthdate = new Date(result.data.birthdate);

          if (angular.isDefined(patient.security)) {
            angular.forEach(patient.security, function (v) {
              if (angular.isDefined(v.securityType) && v.securityType.type === 'ktp') {
                patient.ktp = v.number;
              }
            });
          }

          $scope.patient = patient;
        });

      api.province.find()
        .then(function (result) {
          var city = [];

          angular.forEach(result.data, function(value, key) {
            angular.forEach(value.cities, function (_value, _key) {
              _value.province = value.name;
              _value.country = value.country.name;
              city[city.length] = _value;
            });
          });

          $scope.province = result.data;
          $scope.city = city;
        });

      $scope.fcity = function (data) {
        setTimeout(function () {
          if (data.name) {
            $scope.$evalAsync(function () {
              $scope.patient.address[0].city.province.name = data.name.originalObject.province;
              $scope.patient.address[0].city.province.country.name = data.name.originalObject.country;
            });
          }
        }, 500);
      };

      var temp = [];

      // edit action
      $scope.edit = function (field, form) {
        angular.copy(field, temp);
        form.edit = true;
      };

      // cancel action
      $scope.cancel = function (field, form) {
        angular.extend(field, temp);
        form.edit = false;
        form.$setPristine();
        form.$setUntouched();
      };

      // save action
      $scope.save = function (field) {
        var data = new FormData();
            data.append('name', field.name);
            data.append('email', field.email);
            data.append('phone', field.phone);
            data.append('ktp', field.ktp);
            data.append('birthdate', field.birthdate);
            data.append('gender', field.gender);
            data.append('address', field.address[0].address);
            data.append('city', angular.isObject(field.address[0].city.name) ? field.address[0].city.name.originalObject.id : field.address[0].city.id);
            data.append('role', 'patient');

        api.user.change(data)
          .then(function (result) {
            var patient = {
                 id: result.data[0].id,
               name: result.data[0].name,
              email: result.data[0].email
            };
            $window.localStorage.patient = JSON.stringify(patient);
            $rootScope.USER = JSON.parse($window.localStorage.patient);
            toast.create({
                content: "Profil berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      $scope.asave = function (field) {
        if (!angular.equals(field.password, field.confirm)) {
          toast.create({
              content: "Konfirmasi password tidak sesuai dengan password baru.",
            className: 'error'
          });

          return;
        }

        var data = new FormData();
            data.append('current', field.current);
            data.append('password', field.password);
            data.append('confirm', field.confirm);
            data.append('role', 'patient');

        api.auth.change(data)
          .then(function (result) {
            $auth.login({
                 email: field.email,
              password: field.password,
                  role: 2
            })
            .then(function (result) {
              $window.localStorage.patient = JSON.stringify(result.data.user);
              $rootScope.USER = JSON.parse($window.localStorage.patient);
              toast.create({
                  content: "Profil berhasil diperbarui.",
                className: 'success'
              });
              $route.reload();
            })
            .catch(function (errno) {
              //
            });
          });
      };
    }
  ]);
