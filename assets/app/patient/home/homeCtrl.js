'use strict';

/**
 * Angular controller for dashboard patient
 */
angular
  .module('patientApp')
  .controller('HomeCtrl', ['$scope', '$location', 'api',
    function ($scope, $location, api) {

      var doctors = {};

      $scope.doctors = {};
      $scope.clinics = {};

      api.hospital.find()
        .then(function (result) {
          $scope.hospital = result.data[0];
        });

      api.clinic.find()
        .then(function (result) {
          $scope.clinics = result.data;
        });

      api.doctor.find()
        .then(function (result) {
          doctors = result.data;
          $scope.doctors = result.data;
        });

      $scope.change = function (field) {
        var data = [];
        angular.forEach(doctors, function (value, key) {
          field.clinic == value.clinic.id ? this.push(value) : null;
        }, data);
        $scope.doctors = data;
      };

      $scope.search = function (field) {
        if (field.doctor) {
          $location.path('/doctor/schedule/' + Number(field.doctor.originalObject.id));
        } else if (field.clinic) {
          $location.path('/clinic/schedule/' + Number(field.clinic));
        }
      };
    }
  ]);
