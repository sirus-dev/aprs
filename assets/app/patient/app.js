'use strict';
/**
 * Angular module for patient
 */

angular
  .module('patientApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'lussa.ui',
    'satellizer'
  ])
  .config(['$routeProvider', '$authProvider', '$httpProvider', '$locationProvider',
    function ($routeProvider, $authProvider, $httpProvider, $locationProvider) {
      $routeProvider
        .when('/', {
          templateUrl: '/app/patient/home/home.html',
           controller: 'HomeCtrl'
        })
        .when('/doctor/schedule/confirmation/:id', {
          templateUrl: '/app/patient/booking/confirmation.html',
           controller: 'BookingConfirmationCtrl'
        })
        .when('/notification', {
          templateUrl: '/app/patient/notification/browse.html',
           controller: 'NotificationBrowseCtrl'
        })
        .when('/notification/:date', {
          templateUrl: '/app/patient/notification/browse.html',
           controller: 'NotificationBrowseCtrl'
        })
        .when('/clinic/schedule/:id', {
          templateUrl: '/app/patient/clinic/schedule.html',
           controller: 'ClinicScheduleCtrl'
        })
        .when('/doctor/schedule/:id', {
          templateUrl: '/app/patient/doctor/schedule.html',
           controller: 'DoctorScheduleCtrl'
        })
        .when('/help', {
          templateUrl: '/app/patient/help/help.html',
           controller: 'helpCtrl'
        })
        .when('/help/:query', {
          templateUrl: '/app/patient/help/content.html',
           controller: 'contentCtrl'
        })
        .when('/booking', {
          templateUrl: '/app/patient/booking/browse.html',
           controller: 'BookingBrowseCtrl'
        })
        .when('/booking/:id', {
          templateUrl: '/app/patient/booking/view.html',
           controller: 'bookingCtrl'
        })
        .when('/profile', {
          templateUrl: '/app/patient/home/profile.html',
           controller: 'HomeProfileCtrl'
        })
        .when('/search/clinic/:query', {
          templateUrl: '/app/patient/clinic/search.html',
           controller: 'ClinicSearchCtrl'
        })
        .when('/search/doctor/:query', {
          templateUrl: '/app/patient/doctor/search.html',
           controller: 'DoctorSearchCtrl'
        })
        .when('/register', {
          templateUrl: '/app/patient/auth/register.html',
           controller: 'AuthRegisterCtrl'
        })
        .when('/register/:code', {
          templateUrl: '/app/patient/auth/register.html',
           controller: 'AuthRegisterCtrl'
        })
        .when('/activation', {
          templateUrl: '/app/patient/auth/activation.html',
           controller: 'AuthActivationCtrl'
        })
        .when('/login', {
          templateUrl: '/app/patient/auth/login.html',
           controller: 'AuthLoginCtrl'
        })
        .when('/forget-password', {
          templateUrl: '/app/patient/auth/forget.html',
           controller: 'AuthForgetCtrl'
        })
        .when('/reset-password', {
          templateUrl: '/app/patient/auth/reset.html',
           controller: 'AuthResetCtrl'
        })
        .otherwise({
          redirectTo: '/'
        });

        $authProvider.loginUrl = '/auth/login';
        $httpProvider.interceptors.push('httpRequestInterceptor');
                
    }
  ])
  .factory('httpRequestInterceptor', ['$q', '$window', '$location', 'toast',
    function ($q, $window, $location, toast) {
      return {
        request: function (config) {
          $('.loading').show();
          return config || $q.when(config);
        },
        requestError: function (rejection) {
          return $q.reject(rejection);
        },
        response: function (response) {
          $('.loading').hide();
          return response || $q.when(response);
        },
        responseError: function (rejection) {
          $('.loading').hide();

          if (rejection.status === 500) {
            toast.create({
                content: "Terjadi kesalahan sistem. Silahkan coba beberapa saat lagi.",
              className: 'error'
            });
          }

          if (rejection.status === 400) {
            toast.create({
                content: rejection.data.replace("Oops! ",""),
              className: 'error'
            });
          }

          if ([403, 409, 410, 419].indexOf(rejection.status) > -1) {
            toast.create({
                content: rejection.data,
              className: 'error'
            });
            $location.path('/login');
          }

          return $q.reject(rejection);
        }
      };
    }
  ])
  .factory('api', ['$http',
    function($http) {
      return {
        auth: {
          register: function (data) {
            return $http({
                           url: '/auth/register',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          code: function (data) {
            return $http({
                           url: '/auth/code',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          activation: function (data) {
            return $http({
                           url: '/auth/activation',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          forget: function (data) {
            return $http({
                           url: '/auth/forget',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          reset: function (data) {
            return $http({
                           url: '/auth/reset',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          change: function (data) {
            return $http({
                           url: '/auth/change',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        city: {
          find: function () {
            return $http({
                           url: '/city',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        hospital: {
          find: function () {
            return $http({
                           url: '/hospital',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          meta: function (data) {
            return $http({
                           url: '/hospital/meta',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        clinic: {
          find: function () {
            return $http({
                           url: '/clinic',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          schedule: function (data) {
            return $http({
                           url: '/clinic/schedule',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        country: {
          find: function () {
            return $http({
                           url: '/country',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        patient: {
          find: function () {
            return $http({
                 url: '/user',
              method: 'get'
            });
          },
          findOne: function (id) {
            return $http({
                 url: '/user/' + id,
              method: 'get'
            });
          },
          create: function (data) {
            return $http({
                           url: '/user',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          update: function (id, data) {
            return $http({
                           url: '/user/' + id,
                        method: 'put',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          destroy: function (id) {
            return $http({
                 url: '/user/' + id,
              method: 'delete'
            });
          }
        },
        doctor: {
          find: function () {
            return $http({
                            url: '/doctor',
                         method: 'get',
                withCredentials: true,
                        headers: {'Content-Type': undefined},
               transformRequest: angular.identity
            });
          },
          schedule: function (data) {
            return $http({
                           url: '/doctor/schedule',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        notification: {
          user: function (data) {
            return $http({
                           url: '/notification/user',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        province: {
          find: function () {
            return $http({
                           url: '/province',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        queue: {
          create: function (data) {
            return $http({
                           url: '/queue',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          riset: function () {
            return $http({
                           url: '/queue/riset',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          confirmation: function (data) {
            return $http({
                           url: '/queue/confirmation',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        help: {
          create: function (data) {
            return $http({
                           url: '/help',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          answer: function () {
            return $http({
                           url: '/help/answer',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          findOne: function (id) {
            return $http({
                 url: '/help/' + id,
              method: 'get'
            });
          },
          find: function () {
            return $http({
                           url: '/help',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        user: {
          create: function (data) {
            return $http({
                           url: '/user',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          profile: function () {
            return $http({
                           url: '/user/profile',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          queue: function () {
            return $http({
                           url: '/user/queue',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          change: function (data) {
            return $http({
                           url: '/user/change',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        booking: {
          findOne: function (id) {
            return $http({
                           url: '/queue/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        }
      }
    }
  ])
  .constant('IMAGE', {
    PATH: '/images/doctors/',
    SIZE: '2000000', // ~2Mb
    TYPE: [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]
  })
  .constant('IMAGE_RS', {
    PATH: '/images/RS/',
    SIZE: '2000000', // ~2Mb
    TYPE: [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]
  })
  .constant('DAYS', [
      'Minggu',
      'Senin',
      'Selasa',
      'Rabu',
      'Kamis',
      'Jumat',
      'Sabtu'
  ])
  .constant('MONTHS', [
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
  ])
  .filter('noImage',
		function () {
			return function (input) {
				if (angular.isUndefined(input) || input === null || input === '') {
					return '-.png';
				}

				return input;
			}
		}
	)
  .filter('toDay', ['DAYS',
		function (DAYS) {
			return function (input) {
				if (angular.isNumber(input)) {
					return DAYS[input];
				}

				return input;
			}
		}
	])
  .filter('toMonth', ['MONTHS',
		function (MONTHS) {
			return function (input) {
				if (angular.isNumber(input)) {
					return MONTHS[input];
				}

				return input;
			}
		}
	])
  .filter('toHour', ['$filter',
		function ($filter) {
			return function (input) {
				return $filter('date')(input, 'HH:mm');
			}
		}
	])
  .filter('toDate', ['$filter',
		function ($filter) {
			return function (input) {
				return $filter('date')(input, 'HH:mm:ss dd/MM/yyyy');
			}
		}
	])
  .filter('lowercase', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toLowerCase() + input.substr(1) : '';
    }
  })
  .filter('Date', ['$filter',
    function ($filter) {
      return function (input) {
        return $filter('date')(input, 'dd/MM/yyyy');
      }
    }
  ])
  .run(['$rootScope', '$window', '$auth', '$location', '$http', 'IMAGE', 'IMAGE_RS', 'DAYS',
    function ($rootScope, $window, $auth, $location, $http, IMAGE, IMAGE_RS, DAYS) {
      
      $rootScope.$on('$routeChangeStart', function (next, current) {
        $rootScope.IMAGE = IMAGE;
        $rootScope.DAYS = DAYS;
        $rootScope.IMAGE_RS = IMAGE_RS;
        $rootScope.isMenuCollapsed = false;

        if ($auth.isAuthenticated() && angular.isDefined($window.localStorage.patient)) {
          $rootScope.USER = JSON.parse($window.localStorage.patient);
        }
      });
      
      $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        
        $window.scrollTo(0, 0);
        setTimeout(function () {
          bottomed();
        }, 1500);
      });
      $(window).on('resize', function () {
        setTimeout(function () {
          bottomed();
        }, 1500);
      });
      $('.logout').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $http.get('/auth/logout')
          .then(function (result) {
            $location.path('/login');
          });
      });
      function bottomed () {
        $('.bg-light,.wrapper').removeAttr('style');

        var hscreen = $(window).height(),
            hfooter = ($('footer').position().top)+($('footer').height());

        if (hfooter < hscreen) {
          if ($('.bg-light').length) {
            var helem = $('.bg-light').height();
            $('.bg-light').height(helem+hscreen-hfooter);
          } else {
            var helem = $('.wrapper').height();
            $('.wrapper').height(helem+hscreen-hfooter+15);
          }
        }
      }
    }
  ]);