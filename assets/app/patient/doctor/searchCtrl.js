'use strict';

/**
 * Angular controller for browse doctor
 */
angular
  .module('patientApp')
  .controller('DoctorSearchCtrl', ['$scope', '$route', '$location', 'api',
    function ($scope, $route, $location, api) {
      var params = String($route.current.params.query).split('+').join(' '),
          search = angular.equals(params,'all') ? false : true;

      $scope.currentPage = 0;
      $scope.pageSize = 6;

      api.doctor.find()
        .then(function (result) {
          var data = result.data.sort(function (a, b) {
            return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : a.name.toLowerCase() > b.name.toLowerCase() ? 1 : 0;
          });
          $scope.query = search ? params : null;
          $scope.doctors = search ? filter(data, params) : data;
          $scope.numberOfPages = Math.ceil($scope.doctors.length/$scope.pageSize);

          setTimeout(function () {
            heightsEqualizer('.equal-height');
          }, 1000);
        });

      $scope.goto = function (id) {
        $location.path('/doctor/schedule/' + id);
      };

      $scope.search = function (q) {
        $location.path('/search/doctor/' + q.split(' ').join('+'));
      };

      if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', function() {
          heightsEqualizer('.equal-height');
        });
        window.addEventListener('resize', function(){
          heightsEqualizer('.equal-height');
        });
      }

      $("button[type='button']").on('click', function () {
        setTimeout(function () {
          heightsEqualizer('.equal-height');
        }, 1000);
      });

      function heightsEqualizer(selector) {
        var   elements = document.querySelectorAll(selector),
            max_height = 0,
                   len = 0,
                     i;

        if ((elements) && (elements.length > 0)) {
          len = elements.length;

          for (i = 0; i < len; i++) { // get max height
            elements[i].style.height = ''; // reset height attr
            if (elements[i].clientHeight > max_height) {
              max_height = elements[i].clientHeight;
            }
          }

          for (i = 0; i < len; i++) { // set max height to all elements
            elements[i].style.height = max_height + 'px';
            elements[i].style.cursor = 'pointer';
          }
        }
      }

      function filter (data, word) {
        var  result = [],
            pattern = new RegExp(word, 'i');

        angular.forEach(data, function (v) {
          pattern.test(String(v.name)) || pattern.test(String(v.desc)) || pattern.test(String(v.clinic.name)) ? result.indexOf(v) > -1 ? undefined : this.push(v) : undefined;
        }, result);

        return result;
      }
    }
  ]);
