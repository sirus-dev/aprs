'use strict';

/**
 * Angular controller for view doctor schedules
 */
angular
  .module('patientApp')
  .controller('DoctorScheduleCtrl', ['$scope', '$route', '$filter', '$location', 'TableParams', 'api',
    function ($scope, $route, $filter, $location, TableParams, api) {
      var param = String($route.current.params.id).split('$', 3),
          start = new Date(),
            now = new Date(),
            end = new Date(now.setDate(now.getDate() + 7));

      if (angular.isDefined(param[1]) && angular.isDefined(param[2])) {
        start = new Date(param[1].substr(0,4) + '-' + param[1].substr(4,2) + '-' + param[1].substr(6,2));
          end = new Date(param[2].substr(0,4) + '-' + param[2].substr(4,2) + '-' + param[2].substr(6,2));
      }

      $scope.today = new Date();
      $scope.start = start;
      $scope.end = end;

      var data = new FormData();
          data.append('id', param[0]);
          data.append('start', start);
          data.append('end', end);

			api.doctor.schedule(data)
        .then(function (result) {
          $scope.doctor = result.data;
					var data = result.data.shift;

					if (!(angular.isUndefined(data) || data === null || data === '')) {
						$scope.param = new TableParams({
							// default page
							page: 1,
							// default per page
							count: 10,
							// default filtering
							filter: {},
							// default sorting
							sorting: {
								date: 'asc'
							}
						},{
							total: data.length,
							getData: function ($defer, params) {
								var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
										 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

								params.total(orderedData.length);
								$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
							}
						});
					}
        });

      $scope.submit = function () {
        $location.path('/doctor/schedule/' + param[0] + '$' + $filter('date')($scope.start, 'yyyyMMdd') + '$' + $filter('date')($scope.end, 'yyyyMMdd'));
      };
    }
  ]);
