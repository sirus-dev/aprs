'use strict';

/**
 * Angular controller for browse clinic
 */
angular
  .module('adminApp')
  .controller('ClinicBrowseCtrl', ['$rootScope', '$scope', '$filter', '$route', '$modal', 'TableParams', 'toast', 'api',
    function ($rootScope, $scope, $filter, $route, $modal, TableParams, toast, api) {
      $rootScope.title = "Browse Data Klinik";

			api.clinic.find()
        .then(function (result) {
					var data = result.data;

					$scope.param = new TableParams({
						   // default page
						   page: 1,
						  // default per page
						  count: 10,
						 // default filtering
						 filter: {},
						// default sorting
						sorting: {
							name: 'asc'
						}
					},{
						  total: data.length,
						getData: function ($defer, params) {
							var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
									 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

							params.total(orderedData.length);
							$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
						}
					});
				});

      var temp = [];

      // edit action
      $scope.edit = function (field) {
        angular.copy(field, temp);
        field.isEditing = true;
      };

      // cancel action
      $scope.cancel = function (field, form) {
        angular.extend(field, temp);
        field.isEditing = false;
        form.$setPristine();
        form.$setUntouched();
      };

      // save action
      $scope.save = function (field) {
        var data = new FormData();
            data.append('name', field.name);
            data.append('location', field.location);
            data.append('description', field.description);

        api.clinic.update(field.id, data)
          .then(function (result) {
            toast.create({
                content: "Data klinik berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      // delete action
      $scope.delete = function (field) {
        $scope.modal = {
          title: 'Hapus Data Klinik',
           body: '<h3 class="margin-vertical-xs">' + field.name + '</h3>' +
                 '<p class="help-block margin-vertical-xs">' + field.location + '</p>' +
                 '<p class="text-justify margin-top-large">' + field.description + '</p>'
        };

        $scope.modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
                   size: 'small',
                  scope: $scope
        });

				$scope.ok = function () {
          api.clinic.destroy(field.id)
            .then(function (result) {
              toast.create({
                  content: "Data klinik berhasil dihapus.",
                className: 'success'
              });
              $route.reload();
            });

          $scope.modalInstance.close();
        };

        $scope.cancel = function () {
          $scope.modalInstance.dismiss();
        };
      };
    }
  ]);
