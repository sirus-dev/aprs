'use strict';

/**
 * Angular controller for create clinic
 */
angular
  .module('adminApp')
  .controller('ClinicCreateCtrl', ['$rootScope', '$scope', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $location, $route, toast, api) {
      $rootScope.title = "Tambah Data Klinik";

      $scope.submit = function (field) {
        var data = new FormData();
            data.append('name', field.name);
            data.append('location', field.location);
            data.append('description', field.description);

        api.clinic.create(data)
          .then(function (result) {
            toast.create({
                content: "Data klinik berhasil disimpan.",
              className: 'success'
            });
            $location.path('/clinic/' + result.data.id);
          });
      };

      $scope.reset = function () {
        $route.reload();
      };
    }
  ]);
