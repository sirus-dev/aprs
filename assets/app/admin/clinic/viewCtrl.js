'use strict';

/**
 * Angular controller for view clinic
 */
angular
  .module('adminApp')
  .controller('ClinicViewCtrl', ['$rootScope', '$scope', '$route', '$filter', 'TableParams', 'api',
    function ($rootScope, $scope, $route, $filter, TableParams, api) {
      $rootScope.title = "Lihat Data Klinik";

      api.clinic.findOne($route.current.params.id)
        .then(function (result) {
          $scope.clinic = result.data;

          calcHeight();

          var data = result.data.shifts;

          if (!(angular.isUndefined(data) || data === null || data === '')) {
						$scope.param = new TableParams({
							   // default page
							   page: 1,
							  // default per page
							  count: 10,
							 // default filtering
							 filter: {},
							 // default sorting
							sorting: {
                day: 'asc'
							}
						},{
							  total: data.length,
							getData: function ($defer, params) {
								var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
										 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

								params.total(orderedData.length);
								$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
							}
						});
					}
        });

        function calcHeight () {
          setTimeout(function () {
            heightsEqualizer('.equal-height-image');
            setTimeout(function () {
              heightsEqualizer('.equal-height-doctor');
              setTimeout(function () {
                heightsEqualizer('.equal-height-nurse');
              }, 1000);
            }, 1000);
          }, 1000);
        }

        function heightsEqualizer(selector) {
          var   elements = document.querySelectorAll(selector),
              max_height = 0,
                     len = 0,
                       i;

          if ( (elements) && (elements.length > 0) ) {
              len = elements.length;

              for (i = 0; i < len; i++) { // get max height
                elements[i].style.height = ''; // reset height attr
                if (elements[i].clientHeight > max_height) {
                  max_height = elements[i].clientHeight;
                }
              }

              for (i = 0; i < len; i++) { // set max height to all elements
                elements[i].style.height = max_height + 'px';
              }
          }
        }

        $(window).on('resize', function () {
          calcHeight();
        });

        $('.sidebar-toggle').on('click', function () {
          calcHeight();
        });
    }
  ]);
