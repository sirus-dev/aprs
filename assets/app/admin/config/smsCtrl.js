'use strict';

/**
 * Angular controller for config sms
 */
angular
  .module('adminApp')
  .controller('ConfigSmsCtrl', ['$rootScope', '$scope', '$route', 'toast', 'api',
    function ($rootScope, $scope, $route, toast, api) {
      $rootScope.title = "SMS";

      var config;

      api.config.find()
        .then(function (result) {
          config = result.data[0];
          $scope.sms = result.data[0];
        });

      $scope.save = function (field) {
        var data = new FormData();
            data.append('smsUrl', field.smsUrl);
            data.append('smsUsername', field.smsUsername);
            data.append('smsPassword', field.smsPassword);
            data.append('emailName', config.emailName);
            data.append('emailUserDomain', config.emailUserDomain);

        api.config.update(config.id, data)
          .then(function (result) {
            toast.create({
                content: "Data sms berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      $scope.send = function (field) {
        var data = new FormData();
            data.append('id', config.id);
            data.append('to', field.to);
            data.append('content', field.content);

        api.config.sms(data)
          .then(function (result) {
            if (result.data.toString().replace(' ','').search(/success/i) > -1) {
              toast.create({
                  content: "Pesan berhasil dikirim.",
                className: 'success'
              });
              $route.reload();
            } else {
              toast.create({
                  content: "Pesan gagal dikirim.",
                className: 'error'
              });
            }
          });
      };
    }
  ]);
