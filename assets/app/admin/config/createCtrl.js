'use strict';

/**
 * Angular controller for create clinic
 */
angular
  .module('adminApp')
  .controller('createCtrl', ['$rootScope', '$scope', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $location, $route, toast, api) {
      $rootScope.title = "Tambah Data Domain";

      $scope.submit = function (field) {
        var data = new FormData();
            data.append('domain', field.domain);

        api.synchronize.create(data)
          .then(function (result) {
            toast.create({
                content: "Data Domain berhasil disimpan.",
              className: 'success'
            });
            $location.path('/synchronize');
          });
      };

      $scope.reset = function () {
        $route.reload();
      };
    }
  ]);
