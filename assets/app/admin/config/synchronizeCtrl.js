'use strict';

/**
 * Angular controller for browse clinic
 */
angular
  .module('adminApp')
  .controller('synchronizeCtrl', ['$rootScope', '$scope', '$filter', '$route', '$modal', 'TableParams', 'toast', 'api',
    function ($rootScope, $scope, $filter, $route, $modal, TableParams, toast, api) {
      $rootScope.title = "Browse Data Sinkronisasi Domain";

      api.synchronize.find()
        .then(function (result) {
          var data = result.data;

          $scope.param = new TableParams({
               // default page
               page: 1,
              // default per page
              count: 10,
             // default filtering
             filter: {},
            // default sorting
            sorting: {
              name: 'asc'
            }
          },{
              total: data.length,
            getData: function ($defer, params) {
              var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
                   orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

              params.total(orderedData.length);
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
        });

      var temp = [];

      // delete action
      $scope.delete = function (field) {
        $scope.modal = {
          title: 'Hapus Data Domain',
           body: '<h3 class="margin-vertical-xs">' + field.domain + '</h3>' +
                 '<p class="text-justify margin-top-large">' + field.key + '</p>'
        };

        $scope.modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
                   size: 'small',
                  scope: $scope
        });

        $scope.ok = function () {
          api.synchronize.destroy(field.id)
            .then(function (result) {
              toast.create({
                  content: "Domain berhasil dihapus.",
                className: 'success'
              });
              $route.reload();
            });

          $scope.modalInstance.close();
        };

      };
    }
  ]);
