'use strict';

/**
 * Angular controller for config email
 */
angular
  .module('adminApp')
  .controller('settingCtrl', ['$rootScope', '$scope', '$route', 'toast', 'IMAGE_RS', 'IMAGE_banner', 'api', 
    function ($rootScope, $scope, $route, toast, IMAGE_RS, IMAGE_banner, api) {
      $rootScope.title = "Setting Deskripsi";

      var temp = [];

      api.hospital.find()
        .then(function (result) {
          $scope.setting = result.data[0];
        });

      $scope.favicon = function (fav) {
        var file = fav.files[0],
              fv = new FileReader();

        $scope.nofile_favicon = false;
        $scope.size_favicon = false;
        $scope.format_favicon = false;

        if (angular.isUndefined(file) || file === null || file === '') {
          $scope.nofile_favicon = true;
          return;
        }

        if (IMAGE_RS.TYPE.indexOf(file.type) > -1) {
          if (IMAGE_RS.SIZE < file.size) {
            $scope.size_favicon = true;
          } else {
            fv.onload = function (event) {
              $scope.$apply(function () {
                $scope.fav_rs = event.target.result;
              });
            };

            fv.readAsDataURL(file);
          }
        } else {
          $scope.format_favicon = true;
        }
      };

      $scope.logo = function (log) {
        var logo = log.files[0],
              lg = new FileReader();

        $scope.nofile_logo = false;
        $scope.size_logo = false;
        $scope.format_logo = false;

        if (angular.isUndefined(logo) || logo === null || logo === '') {
          $scope.nofile_logo = true;
          return;
        }

        if (IMAGE_RS.TYPE.indexOf(logo.type) > -1) {
          if (IMAGE_RS.SIZE < logo.size) {
            $scope.size_logo = true;
          } else {
            lg.onload = function (logo) {
              $scope.$apply(function () {
                $scope.logo_rs = logo.target.result;
              });
            };

            lg.readAsDataURL(logo);
          }
        } else {
          $scope.format_logo = true;
        }
      };

      $scope.banner = function (ban) {
        var banner = ban.files[0],
              bnn = new FileReader();

        $scope.nofile_banner = false;
        $scope.size_banner = false;
        $scope.format_banner = false;

        if (angular.isUndefined(banner) || banner === null || banner === '') {
          $scope.nofile_banner = true;
          return;
        }

        if (IMAGE_banner.TYPE.indexOf(banner.type) > -1) {
          if (IMAGE_banner.SIZE < banner.size) {
            $scope.size_banner = true;
          } else {
            bnn.onload = function (banner) {
              $scope.$apply(function () {
                $scope.banner_rs = banner.target.result;
              });
            };

            bnn.readAsDataURL(banner);
          }
        } else {
          $scope.format_banner = true;
        }
      };

      $scope.save = function (field) {
        var data = new FormData();
            data.append('name', field.name);
            data.append('email', field.email);
            data.append('phone', field.phone);
            data.append('address', field.address);
            data.append('description', field.description);
            data.append('favicon', field.favicon);
            data.append('logo', field.logo);
            data.append('banner', field.banner);

        api.hospital.update(field.id, data)
          .then(function (result) {
            toast.create({
                content: "Deskripsi RS berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

    }
  ]);