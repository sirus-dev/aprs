'use strict';

/**
 * Angular controller for config email
 */
angular
  .module('adminApp')
  .controller('ConfigEmailCtrl', ['$rootScope', '$scope', '$route', 'toast', 'api',
    function ($rootScope, $scope, $route, toast, api) {
      $rootScope.title = "Email";

      var config;

      api.config.find()
        .then(function (result) {
          config = result.data[0];
          $scope.email = result.data[0];
        });

      $scope.save = function (field) {
        var data = new FormData();
            data.append('smsUrl', config.smsUrl);
            data.append('smsUsername', config.smsUsername);
            data.append('smsPassword', config.smsPassword);
            data.append('emailName', field.emailName);
            data.append('emailUserDomain', field.emailUserDomain);

        api.config.update(config.id, data)
          .then(function (result) {
            toast.create({
                content: "Data email berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      $scope.send = function (field) {
        var data = new FormData();
            data.append('id', config.id);
            data.append('to', field.to);
            data.append('subject', field.subject);
            data.append('contents', field.content);

        api.config.email(data)
          .then(function (result) {
            toast.create({
                content: "Email berhasil dikirim.",
              className: 'success'
            });
            $route.reload();
          });
      };
    }
  ]);