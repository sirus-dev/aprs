'use strict';

/**
 * Angular controller for view patient
 */
angular
  .module('adminApp')
  .controller('patientViewCtrl', ['$rootScope', '$scope', '$route', '$filter', 'TableParams', 'api',
    function ($rootScope, $scope, $route, $filter, TableParams, api) {
      $rootScope.title = "Lihat Data Pasien";

      api.patient.findOne($route.current.params.id)

        .then(
          function (result) {
            $scope.patient = result.data;

            var data = result.data;
          },
          function (errno) {}
        );

       api.schedule.findOne($route.current.params.id)
        .then(function (result) {
          var data = result.data;
          // send data to view
          $scope.param = new TableParams({
               // default page
               page: 1,
              // default per page
              count: 10,
             // default filtering
             filter: {},
            // default sorting
            sorting: {
              createdAt: 'desc'
            }
          },{
              total: data.length,
            getData: function ($defer, params) {
              var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
                   orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

              params.total(orderedData.length);
              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
        });
    }
  ]);
