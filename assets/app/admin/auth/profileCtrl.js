'use strict';

/**
 * Angular controller for browse clinic
 */
angular
  .module('adminApp')
  .controller('AuthProfileCtrl', ['$rootScope', '$scope', '$window', '$auth', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $location, $route, toast, api) {
      $rootScope.title = "Profil";

      api.user.profile()
        .then(function (result) {
          if (!result.data || !angular.isDefined($window.localStorage.admin)) {
            $location.path('/login');
          }
          $scope.admin = result.data;
        });

      var temp = [];

      // edit action
      $scope.edit = function (field, form) {
        angular.copy(field, temp);
        form.edit = true;
      };

      // cancel action
      $scope.cancel = function (field, form) {
        angular.extend(field, temp);
        form.edit = false;
        form.$setPristine();
        form.$setUntouched();
      };

      // save action
      $scope.save = function (field) {
        var data = new FormData();
            data.append('name', field.name);
            data.append('email', field.email);
            data.append('phone', field.phone);
            data.append('ktp', field.ktp);
            data.append('birthdate', field.birthdate);
            data.append('gender', field.gender);
            data.append('address', field.address[0].address);
            data.append('city', field.address[0].city.id);
            data.append('role', 'admin');

        api.user.change(data)
          .then(function (result) {
            var admin = {
                 id: result.data[0].id,
               name: result.data[0].name,
              email: result.data[0].email
            };
            $window.localStorage.admin = JSON.stringify(admin);
            $rootScope.USER = JSON.parse($window.localStorage.admin);
            toast.create({
                content: "Profil berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      $scope.asave = function (field) {
        if (!angular.equals(field.password, field.confirm)) {
          toast.create({
              content: "Konfirmasi password tidak sesuai dengan password baru.",
            className: 'error'
          });

          return;
        }

        var data = new FormData();
            data.append('current', field.current);
            data.append('password', field.password);
            data.append('confirm', field.confirm);
            data.append('role', 'admin');

        api.auth.change(data)
          .then(function (result) {
            $auth.login({
                 email: field.email,
              password: field.password,
                  role: 0
            })
            .then(function (result) {
              $window.localStorage.admin = JSON.stringify(result.data.user);
              $rootScope.USER = JSON.parse($window.localStorage.admin);
              toast.create({
                  content: "Profil berhasil diperbarui.",
                className: 'success'
              });
              $route.reload();
            })
            .catch(function (errno) {
              //
            });
          });
      };
    }
  ]);
