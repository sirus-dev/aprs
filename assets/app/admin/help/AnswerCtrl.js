'use strict';

/**
 * Angular controller for view patient
 */
angular
  .module('adminApp')
  .controller('AnswerCtrl', ['$rootScope', '$scope', '$window', '$auth', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $window, $auth, $location, $route, toast, api) {
      $rootScope.title = "Kirim Email";

      var config;

      api.config.find()
        .then(function (result) {
          config = result.data[0];
          $scope.email = result.data[0];
        });

      api.help.findOne($route.current.params.id)
        .then(
          function (result) {
            $scope.help = result.data;
            $scope.help.subject = 'Tanggapan';

          },
          function (errno) {}
        );



      $scope.send = function (field) {
        var data = new FormData();
            data.append('id', config.id);
            data.append('to', field.email);
            data.append('subject', field.subject);
            data.append('contents', field.send);
            data.append('answer', field.send);

        api.config.email(data)
          .then(function (result) {
        api.help.update($route.current.params.id, data)
          .then(function (result){
            toast.create({
                content: "Email berhasil dikirim.",
              className: 'success'
            });
            $location.path('/help');
          });
          });
      };
    }
  ]);
