'use strict';

/**
 * Angular controller for browse patient
 */
angular
  .module('adminApp')
  .controller('HelpCtrl', ['$rootScope', '$scope', '$filter', '$route', '$modal', 'TableParams', 'toast', 'api',
    function ($rootScope, $scope, $filter, $route, $modal, TableParams, toast, api) {
      $rootScope.title = "Browse Data Bantuan";

      api.help.find()
        .then(
          function (result) {
            var data = result.data;

            $scope.param = new TableParams({
                 // default page
                 page: 1,
                // default per page
                count: 10,
               // default filtering
               filter: {},
              // default sorting
              sorting: {
                id: 'desc'
              }
            },{
                total: data.length,
              getData: function ($defer, params) {
                var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
                     orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

                params.total(orderedData.length);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              }
            });
          },
          function (errno) {}
        );

      var temp = [];

      // edit action
      $scope.edit = function (field) {
        angular.copy(field, temp);
        field.isEditing = true;
      };

      // cancel action
      $scope.cancel = function (field, form) {
        angular.extend(field, temp);
        field.isEditing = false;
        form.$setPristine();
        form.$setUntouched();
      };

      // save action
      $scope.save = function (field) {
        var data = new FormData();
            data.append('title', field.title);
            data.append('content', field.content);
            data.append('answer', field.answer);
            data.append('status', field.status);

        api.help.update(field.id, data)
          .then(function (result) {
            toast.create({
                content: "Data klinik berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      // delete action
      $scope.delete = function (field) {
        $scope.modal = {
          title: 'Hapus Data Pesan',
           body: '<h3 class="margin-vertical-xs"> Judul : ' + field.title + '</h3>'
        };

        $scope.modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
                   size: 'small',
                  scope: $scope
        });

        $scope.ok = function () {
          api.help.destroy(field.id)
            .then(function (result) {
              toast.create({
                  content: "Data pesan berhasil dihapus.",
                className: 'success'
              });
              $route.reload();
            });

          $scope.modalInstance.close();
        };

        $scope.cancel = function () {
          $scope.modalInstance.dismiss();
        };

      };
    }
  ]);
