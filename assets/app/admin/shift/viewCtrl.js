'use strict';

/**
 * Angular controller for view shift
 */
angular
  .module('adminApp')
  .controller('ShiftViewCtrl', ['$rootScope', '$scope', '$route', 'toast', 'api',
    function ($rootScope, $scope, $route, toast, api) {
      $rootScope.title = "Lihat Data Shift";

      var params = String($route.current.params.id).split('$',3);

      var data = new FormData();
          data.append('day', params[0]);
          data.append('startHour', new Date(params[1].substr(0,2),params[1].substr(2,2)-1,params[1].substr(4,2),params[1].substr(6,2),params[1].substr(8,2)));
          data.append('endHour', new Date(params[2].substr(0,2),params[2].substr(2,2)-1,params[2].substr(4,2),params[2].substr(6,2),params[2].substr(8,2)));

      api.shift.search(data)
        .then(function (result) {
          $scope.shift = result.data;
          calcHeight();
        });

      function calcHeight () {
        setTimeout(function () {
          heightsEqualizer('.equal-height-image');
          setTimeout(function () {
            heightsEqualizer('.equal-height-doctor');
            setTimeout(function () {
              heightsEqualizer('.equal-height-nurse');
            }, 1000);
          }, 1000);
        }, 1000);
      }

      function heightsEqualizer(selector) {
        var   elements = document.querySelectorAll(selector),
            max_height = 0,
                   len = 0,
                     i;

        if ( (elements) && (elements.length > 0) ) {
            len = elements.length;

            for (i = 0; i < len; i++) { // get max height
              elements[i].style.height = ''; // reset height attr
              if (elements[i].clientHeight > max_height) {
                max_height = elements[i].clientHeight;
              }
            }

            for (i = 0; i < len; i++) { // set max height to all elements
              elements[i].style.height = max_height + 'px';
            }
        }
      }

      $(window).on('resize', function () {
        calcHeight();
      });

      $('.sidebar-toggle').on('click', function () {
        calcHeight();
      });
    }
  ]);
