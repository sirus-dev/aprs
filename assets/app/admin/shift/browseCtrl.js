'use strict';

/**
 * Angular controller for browse shift
 */
angular
  .module('adminApp')
  .controller('ShiftBrowseCtrl', ['$rootScope', '$scope', '$filter', 'TableParams', 'api',
    function ($rootScope, $scope, $filter, TableParams, api) {
      $rootScope.title = "Browse Data Shift";

			api.shift.find()
        .then(function (result) {
					var data = [];

          for (var i = 0; i < result.data.length; i++) {
            var available = false;
            for (var j = 0; j < data.length; j++) {
              if (angular.equals(result.data[i].day, data[j].day) && angular.equals(result.data[i].startHour, data[j].startHour) && angular.equals(result.data[i].endHour, data[j].endHour)) {
                available = true;
                break;
              }
            }
            available ? undefined : data.push(result.data[i]);
          }

					// send data to view
					$scope.param = new TableParams({
						   // default page
						   page: 1,
						  // default per page
						  count: 10,
						 // default filtering
						 filter: {},
						// default sorting
						sorting: {
							day: 'asc'
						}
					},{
						  total: data.length,
						getData: function($defer, params) {
							var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
									 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

							params.total(orderedData.length);
							$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
						}
					});
				});
    }
  ]);
