'use strict';

/**
 * Angular controller for browse doctor
 */
angular
  .module('adminApp')
  .controller('NurseBrowseCtrl', ['$rootScope', '$scope', '$route', '$filter', '$modal', 'TableParams', 'toast', 'api',
    function ($rootScope, $scope, $route, $filter, $modal, TableParams, toast, api) {
      $rootScope.title = "Browse Data Perawat";

			api.nurse.find()
        .then(function (result) {
					var data = result.data;
					// send data to view
					$scope.param = new TableParams({
						   // default page
						   page: 1,
						  // default per page
						  count: 10,
						 // default filtering
						 filter: {},
						// default sorting
						sorting: {
							name: 'desc'
						}
					},{
						  total: data.length,
						getData: function($defer, params) {
							var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
									 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

							params.total(orderedData.length);
							$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
						}
					});
				});

      var temp = [];

      // edit action
      $scope.edit = function (field) {
        angular.copy(field, temp);
        field.isEditing = true;
        // get data for clinic option
        api.clinic.find()
          .then(function (result) {
            $scope.clinics = result.data;
          });
      };

      // cancel action
      $scope.cancel = function (field, form) {
        angular.extend(field, temp);
        field.isEditing = false;
        form.$setPristine();
        form.$setUntouched();
      };

      // save action
      $scope.save = function (field) {
        var data = new FormData();
            data.append('name', field.user.name);
            data.append('email', field.user.email);
            data.append('clinic', field.clinic.id);

        api.nurse.update(field.user.id, data)
          .then(function (result) {
            toast.create({
                content: "Data perawat berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      // delete action
      $scope.delete = function (field) {
        $scope.modal = {
          title: 'Hapus Data Perawat',
           body: '<h4 class="margin-vertical-xs">' + field.user.name + '</h4>' +
                 '<small class="help-block margin-vertical-xs">' + field.user.email + '</small>' +
                 '<p class="help-block margin-vertical-xs">' + field.clinic.name + '</p>'
        };

        $scope.modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
                   size: 'small',
                  scope: $scope
        });

        $scope.ok = function () {
          api.nurse.destroy(field.id)
            .then(function (result) {
              toast.create({
                  content: "Data perawat berhasil dihapus.",
                className: 'success'
              });
              $route.reload();
            });

          $scope.modalInstance.close();
        };

        $scope.cancel = function () {
          $scope.modalInstance.dismiss();
        };
      };
    }
  ]);
