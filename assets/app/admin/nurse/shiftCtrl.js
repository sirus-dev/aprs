'use strict';

/**
 * Angular controller for create shift
 */
angular
  .module('adminApp')
  .controller('NurseShiftCtrl', ['$rootScope', '$scope', '$route', '$filter', '$modal', 'TableParams', 'toast', 'api',
    function ($rootScope, $scope, $route, $filter, $modal, TableParams, toast, api) {
      $rootScope.title = "Data Shift Perawat";

      api.nurse.findOne($route.current.params.nurseId)
        .then(function (result) {
          $scope.nurse = result.data;

          var data = result.data.shift;

          if (!(angular.isUndefined(data) || data === null || data === '')) {
						// send data to view
						$scope.param = new TableParams({
							   // default page
							   page: 1,
							  // default per page
							  count: 10,
							 // default filtering
							 filter: {},
							// default sorting
							sorting: {
								day: 'asc'
							}
						},{
							  total: data.length,
							getData: function($defer, params) {
								var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
										 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

								params.total(orderedData.length);
								$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
							}
						});
					}
        });

      api.shift.find()
        .then(function (result) {
          $scope.shifts = result.data;
        });

      $scope.save = function () {
        var data = new FormData();
            data.append('nurse', $route.current.params.nurseId);
            data.append('shift', $scope.shift);

        api.nurse.shift(data)
          .then(function (result) {
            toast.create({
                content: "Data shift perawat berhasil disimpan.",
              className: 'success'
            });
            $route.reload();
          });
      };

      $scope.delete = function (field) {
        $scope.modal = {
          title: 'Hapus Data Shift Perawat',
           body: '<p class="text-center"><strong>' + $filter('toDay')(field.day) + ', ' + $filter('toHour')(field.startHour) + ' - ' + $filter('toHour')(field.endHour) + '</strong></p>'
        };

        $scope.modalInstance = $modal.open({
          templateUrl: 'myModalContent.html',
                 size: 'small',
                scope: $scope
        });

        $scope.ok = function () {
          var data = new FormData();
              data.append('nurse', $route.current.params.nurseId);
              data.append('shift', field.id);

          api.shift.nurse(data)
            .then(function (result) {
              toast.create({
                  content: "Data shift perawat berhasil dihapus.",
                className: 'success'
              });
              $route.reload();
            });

          $scope.modalInstance.close();
        };

        $scope.cancel = function () {
          $scope.modalInstance.dismiss();
        };
      };
    }
  ]);
