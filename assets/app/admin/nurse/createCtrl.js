'use strict';

/**
 * Angular controller for create doctor
 */
angular
  .module('adminApp')
  .controller('NurseCreateCtrl', ['$rootScope', '$scope', '$location', '$route', 'toast', 'api',
    function ($rootScope, $scope, $location, $route, toast, api) {
      $rootScope.title = "Tambah Data Perawat";

      api.clinic.find()
        .then(function (result) {
          $scope.clinics = result.data;
        });

      // submit action
      $scope.submit = function (field) {
        var data = new FormData();
            data.append('name', field.name);
            data.append('email', field.email);
            data.append('clinic', field.clinic.originalObject.id);

        api.nurse.create(data)
          .then(function (result) {
              toast.create({
                  content: "Data perawat berhasil disimpan. Silahkan cek email.",
                className: 'success'
              });
              $location.path('/nurse/shift/' + result.data.id);
            });
      };

      $scope.reset = function () {
        $route.reload();
      };
    }
  ]);
