'use strict';

/**
 * Angular controller for view doctor
 */
angular
  .module('adminApp')
  .controller('DoctorViewCtrl', ['$rootScope', '$scope', '$route', 'api',
    function ($rootScope, $scope, $route, api) {
      $rootScope.title = "Lihat Data Dokter";

      api.doctor.findOne($route.current.params.id)
        .then(function (result) {
          $scope.doctor = result.data;
        });

      if (document.addEventListener) {
          document.addEventListener('DOMContentLoaded', function() {
              heightsEqualizer('.equal-height');
          });
      	window.addEventListener('resize', function(){
      		heightsEqualizer('.equal-height');
      	});
      }

      setTimeout(function () {
        heightsEqualizer('.equal-height');
      }, 1000);

      function heightsEqualizer(selector) {
        var   elements = document.querySelectorAll(selector),
            max_height = 0,
                   len = 0,
                     i;

        if ( (elements) && (elements.length > 0) ) {
            len = elements.length;

            for (i = 0; i < len; i++) { // get max height
              elements[i].style.height = ''; // reset height attr
              if (elements[i].clientHeight > max_height) {
                max_height = elements[i].clientHeight;
              }
            }

            for (i = 0; i < len; i++) { // set max height to all elements
              elements[i].style.height = max_height + 'px';
            }
          }
        }
    }
  ]);
