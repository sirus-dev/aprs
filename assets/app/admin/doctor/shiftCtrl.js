'use strict';

/**
 * Angular controller for create shift
 */
angular
  .module('adminApp')
  .controller('DoctorShiftCtrl', ['$rootScope', '$scope', '$route', '$filter', '$modal', 'TableParams', 'toast', 'api',
    function ($rootScope, $scope, $route, $filter, $modal, TableParams, toast, api) {
      $rootScope.title = "Data Shift Dokter";

      api.doctor.findOne($route.current.params.doctorId)
        .then(function (result) {
          $scope.doctor = result.data;

          var data = result.data.shifts;

          if (!(angular.isUndefined(data) || data === null || data === '')) {
						// send data to view
						$scope.param = new TableParams({
							   // default page
							   page: 1,
							  // default per page
							  count: 10,
							 // default filtering
							 filter: {},
							// default sorting
							sorting: {
								createdAt: 'desc'
							}
						},{
							  total: data.length,
							getData: function($defer, params) {
								var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
										 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

								params.total(orderedData.length);
								$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
							}
						});
					}
        });

      $scope.startHour = new Date(10,10,10);
      $scope.endHour = new Date(10,10,10);

      $scope.$watch('endHour', function(newValue, oldValue) {
        if ($scope.startHour > $scope.endHour || $scope.startHour.toString() === $scope.endHour.toString()) {
          $scope.larger = true;
        } else {
          $scope.larger = false;
        }
      });

      $scope.save = function () {
        var data = new FormData();
            data.append('day', $scope.day);
            data.append('startHour', $scope.startHour);
            data.append('endHour', $scope.endHour);
            data.append('doctor', $route.current.params.doctorId);

        api.doctor.shift(data)
          .then(function (result) {
            toast.create({
                content: "Data shift dokter berhasil disimpan.",
              className: 'success'
            });
            $route.reload();
          });
      };

      $scope.delete = function (field) {
        $scope.modal = {
          title: 'Hapus Data Shift Dokter',
           body: '<p class="text-center"><strong>' + $filter('toDay')(field.day) + ', ' + $filter('toHour')(field.startHour) + ' - ' + $filter('toHour')(field.endHour) + '</strong></p>'
        };

        $scope.modalInstance = $modal.open({
          templateUrl: 'myModalContent.html',
                 size: 'small',
                scope: $scope
        });

        $scope.ok = function () {
          var data = new FormData();
              data.append('id', field.id);

          api.shift.doctor(data)
            .then(function (result) {
              toast.create({
                  content: "Data shift dokter berhasil dihapus.",
                className: 'success'
              });
              $route.reload();
            });

          $scope.modalInstance.close();
        };

        $scope.cancel = function () {
          $scope.modalInstance.dismiss();
        };
      };
    }
  ]);
