'use strict';

/**
 * Angular controller for browse doctor
 */
angular
  .module('adminApp')
  .controller('DoctorBrowseCtrl', ['$rootScope', '$scope', '$route', '$filter', '$modal', 'TableParams', 'toast', 'IMAGE', 'api',
    function ($rootScope, $scope, $route, $filter, $modal, TableParams, toast, IMAGE, api) {
      $rootScope.title = "Browse Data Dokter";

			api.doctor.find()
        .then(function (result) {
					var data = result.data;
					// send data to view
					$scope.param = new TableParams({
						   // default page
						   page: 1,
						  // default per page
						  count: 10,
						 // default filtering
						 filter: {},
						// default sorting
						sorting: {
							name: 'asc'
						}
					},{
						  total: data.length,
						getData: function($defer, params) {
							var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
									 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

							params.total(orderedData.length);
							$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
						}
					});
				});

      var temp = [];

      // edit action
      $scope.edit = function (field) {
        angular.copy(field, temp);
        field.isEditing = true;
        // get data for clinic option
        api.clinic.find()
          .then(function (result) {
            $scope.clinics = result.data;
          });
      };

      // cancel action
      $scope.cancel = function (field, form) {
        angular.extend(field, temp);
        field.isEditing = false;
        form.$setPristine();
        form.$setUntouched();
      };

      $scope.preview = function (field) {
        var file = field.files[0],
              fr = new FileReader();

        $scope.nofile = false;
        $scope.size = false;
        $scope.format = false;

        if (angular.isUndefined(file) || file === null || file === '') {
          $scope.nofile = true;
          return;
        }

        if (IMAGE.TYPE.indexOf(file.type) > -1) {
          if (IMAGE.SIZE < file.size) {
            $scope.size = true;
          }
        } else {
          $scope.format = true;
        }
      };

      // save action
      $scope.save = function (field) {
        var data = new FormData();
            data.append('title', field.title);
            data.append('name', field.name);
            data.append('clinic', field.clinic.id);
            data.append('picture', field.picture);
            data.append('desc', field.desc);

        api.doctor.update(field.id, data)
          .then(function (result) {
            toast.create({
                content: "Data dokter berhasil diperbarui.",
              className: 'success'
            });
            $route.reload();
          });
      };

      // delete action
      $scope.delete = function (field) {
        $scope.modal = {
          title: 'Hapus Data Dokter',
           body: '<h4 class="margin-vertical-xs">' + field.name + ' ' + field.title + '</h4>' +
                 '<p class="help-block margin-vertical-xs">' + field.clinic.name + '</p>' +
                 '<p class="text-justify margin-top-large">' + field.desc + '</p>'
        };

        $scope.modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
                   size: 'small',
                  scope: $scope
        });

        $scope.ok = function () {
          api.doctor.destroy(field.id)
            .then(function (result) {
              toast.create({
                  content: "Data dokter berhasil dihapus.",
                className: 'success'
              });
              $route.reload();
            });

          $scope.modalInstance.close();
        };

        $scope.cancel = function () {
          $scope.modalInstance.dismiss();
        };
      };
    }
  ]);
