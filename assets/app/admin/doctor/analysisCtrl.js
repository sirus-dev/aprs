'use strict';

/**
 * Angular controller for browse doctor
 */
angular
  .module('adminApp')
  .controller('DoctorAnalysisCtrl', ['$rootScope', '$scope', '$route', '$filter', '$modal', 'TableParams', 'toast', 'api',
    function ($rootScope, $scope, $route, $filter, $modal, TableParams, toast, api) {
      $rootScope.title = "Analisis Waktu Konsultasi Dokter";

			api.doctor.analysis()
        .then(function (result) {
					var data = result.data;
					// send data to view
					$scope.param = new TableParams({
						   // default page
						   page: 1,
						  // default per page
						  count: 10,
						 // default filtering
						 filter: {},
						// default sorting
						sorting: {
							name: 'asc'
						}
					},{
						  total: data.length,
						getData: function($defer, params) {
							var filteredData = params.filter() ? $filter('filter')(data, params.filter()) : data,
									 orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

							params.total(orderedData.length);
							$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
						}
					});
				});
    }
  ]);
