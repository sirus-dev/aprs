'use strict';

/**
 * Angular controller for create doctor
 */
angular
  .module('adminApp')
  .controller('DoctorCreateCtrl', ['$rootScope', '$scope', '$location', '$route', 'toast', 'IMAGE', 'api',
    function ($rootScope, $scope, $location, $route, toast, IMAGE, api) {
      $rootScope.title = "Tambah Data Dokter";

      api.clinic.find()
        .then(function (result) {
          $scope.clinics = result.data;
        });

      $scope.nofile = true;

      $scope.preview = function (field) {
        var file = field.files[0],
              fr = new FileReader();

        $scope.nofile = false;
        $scope.size = false;
        $scope.format = false;

        if (angular.isUndefined(file) || file === null || file === '') {
          $scope.nofile = true;
          return;
        }

        if (IMAGE.TYPE.indexOf(file.type) > -1) {
          if (IMAGE.SIZE < file.size) {
            $scope.size = true;
          } else {
            fr.onload = function (event) {
              $scope.$apply(function () {
                $scope.img = event.target.result;
              });
            };

            fr.readAsDataURL(file);
          }
        } else {
          $scope.format = true;
        }
      };

      // submit action
      $scope.submit = function (field) {
        var data = new FormData();
            data.append('title', field.title);
            data.append('name', field.name);
            data.append('clinic', field.clinic.originalObject.id);
            data.append('desc', field.desc);
            data.append('picture', field.picture);

        api.doctor.create(data)
          .then(function (result) {
            toast.create({
                content: "Data dokter berhasil disimpan.",
              className: 'success'
            });
            $location.path('/doctor/shift/' + result.data.id);
          });
      };

      $scope.reset = function () {
        $route.reload();
      };
    }
  ]);
