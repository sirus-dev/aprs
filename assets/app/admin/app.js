'use strict';

/**
 * Angular module for admin
 */
angular
  .module('adminApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'lussa.ui',
    'satellizer'
  ])
  .config(['$routeProvider', '$authProvider', '$httpProvider',
    function ($routeProvider, $authProvider, $httpProvider) {
      $routeProvider
        .when('/', {
          templateUrl: '/app/admin/clinic/browse.html',
           controller: 'ClinicBrowseCtrl'
        })
        .when('/login', {
          templateUrl: '/app/admin/auth/login.html',
           controller: 'AuthLoginCtrl'
        })
        .when('/profile', {
          templateUrl: '/app/admin/auth/profile.html',
           controller: 'AuthProfileCtrl'
        })
        .when('/clinic', {
          templateUrl: '/app/admin/clinic/browse.html',
           controller: 'ClinicBrowseCtrl'
        })
        .when('/clinic/create', {
          templateUrl: '/app/admin/clinic/create.html',
           controller: 'ClinicCreateCtrl'
        })
        .when('/clinic/:id', {
          templateUrl: '/app/admin/clinic/view.html',
           controller: 'ClinicViewCtrl'
        })
        .when('/doctor', {
          templateUrl: '/app/admin/doctor/browse.html',
           controller: 'DoctorBrowseCtrl'
        })
        .when('/doctor/create', {
          templateUrl: '/app/admin/doctor/create.html',
           controller: 'DoctorCreateCtrl'
        })
        .when('/doctor/shift/:doctorId', {
          templateUrl: '/app/admin/doctor/shift.html',
           controller: 'DoctorShiftCtrl'
        })
        .when('/analysis', {
          templateUrl: '/app/admin/doctor/analysis.html',
           controller: 'DoctorAnalysisCtrl'
        })
        .when('/help', {
          templateUrl: '/app/admin/help/help.html',
           controller: 'HelpCtrl'
        })
        .when('/help/answer/:id', {
          templateUrl: '/app/admin/help/answer.html',
           controller: 'AnswerCtrl'
        })
        .when('/doctor/:id', {
          templateUrl: '/app/admin/doctor/view.html',
           controller: 'DoctorViewCtrl'
        })
        .when('/nurse', {
          templateUrl: '/app/admin/nurse/browse.html',
           controller: 'NurseBrowseCtrl'
        })
        .when('/nurse/create', {
          templateUrl: '/app/admin/nurse/create.html',
           controller: 'NurseCreateCtrl'
        })
        .when('/nurse/shift/:nurseId', {
          templateUrl: '/app/admin/nurse/shift.html',
           controller: 'NurseShiftCtrl'
        })
        .when('/shift', {
          templateUrl: '/app/admin/shift/browse.html',
           controller: 'ShiftBrowseCtrl'
        })
        .when('/shift/:id', {
          templateUrl: '/app/admin/shift/view.html',
           controller: 'ShiftViewCtrl'
        })
        .when('/patient', {
          templateUrl: '/app/admin/patient/browse.html',
           controller: 'PatientBrowseCtrl'
        })
        .when('/patient/:id', {
          templateUrl: '/app/admin/patient/view.html',
           controller: 'patientViewCtrl'
        })
        .when('/booking', {
          templateUrl: '/app/admin/booking/browse.html',
           controller: 'BookingBrowseCtrl'
        })
        .when('/notif', {
          templateUrl: '/app/admin/notification/browse.html',
           controller: 'NotificationBrowseCtrl'
        })
        .when('/notif/:id', {
          templateUrl: '/app/admin/notification/patient.html',
          controller: 'NotificationPatientCtrl'
        })
        .when('/email', {
          templateUrl: '/app/admin/config/email.html',
           controller: 'ConfigEmailCtrl'
        })
        .when('/sms', {
          templateUrl: '/app/admin/config/sms.html',
           controller: 'ConfigSmsCtrl'
        })
        .when('/setting', {
          templateUrl: '/app/admin/config/setting.html',
           controller: 'settingCtrl'
        })
        .when('/synchronize', {
          templateUrl: '/app/admin/config/synchronize.html',
           controller: 'synchronizeCtrl'
        })
        .when('/synchronize/create', {
          templateUrl: '/app/admin/config/create.html',
           controller: 'createCtrl'
        })
        .otherwise({
          redirectTo: '/'
        });

        $authProvider.loginUrl = '/auth/login';
        $httpProvider.interceptors.push('httpRequestInterceptor');
  }])
  .factory('httpRequestInterceptor', ['$q', '$window', '$location', 'toast',
    function ($q, $window, $location, toast) {
      return {
        request: function (config) {
          $('.loading').show();
          return config || $q.when(config);
        },
        requestError: function (rejection) {
          return $q.reject(rejection);
        },
        response: function (response) {
          $('.loading').hide();
          return response || $q.when(response);
        },
        responseError: function (rejection) {
          $('.loading').hide();

          if (rejection.status === 500) {
            toast.create({
                content: "Terjadi kesalahan sistem. Silahkan coba beberapa saat lagi.",
              className: 'error'
            });
          }

          if (rejection.status === 400) {
            toast.create({
                content: rejection.data.replace("Oops! ",""),
              className: 'error'
            });
          }

          if ([403, 409, 410, 419].indexOf(rejection.status) > -1) {
            toast.create({
                content: rejection.data,
              className: 'error'
            });
            $location.path('/login');
          }

          return $q.reject(rejection);
        }
      };
    }
  ])
  .factory('api', ['$http',
    function($http) {
      return {
        auth: {
          forget_admin: function (data) {
            return $http({
                           url: '/auth/forget_admin',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          reset_admin: function (data) {
            return $http({
                           url: '/auth/reset_admin',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          change: function (data) {
            return $http({
                           url: '/auth/change',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        clinic: {
          find: function () {
            return $http({
                           url: '/clinic',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          findOne: function (id) {
            return $http({
                           url: '/clinic/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          create: function (data) {
            return $http({
                           url: '/clinic',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          update: function (id, data) {
            return $http({
                           url: '/clinic/' + id,
                        method: 'put',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          destroy: function (id) {
            return $http({
                           url: '/clinic/' + id,
                        method: 'delete',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        config: {
          find: function () {
            return $http({
                           url: '/config',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          update: function (id, data) {
            return $http({
                           url: '/config/' + id,
                        method: 'put',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          email: function (data) {
            return $http({
                           url: '/config/email',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          sms: function (data) {
            return $http({
                           url: '/config/sms',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },synchronize: {
          find: function () {
            return $http({
                           url: '/synchronize',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          create: function (data) {
            return $http({
                           url: '/synchronize',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          destroy: function (id) {
            return $http({
                           url: '/synchronize/' + id,
                        method: 'delete',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        hospital: {
          find: function () {
            return $http({
                           url: '/hospital',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          update: function (id, data) {
            return $http({
                           url: '/hospital/' + id,
                        method: 'put',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        patient: {
          find: function () {
            return $http({
                           url: '/user',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          findOne: function (id) {
            return $http({
                           url: '/user/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          update: function (id, data) {
            return $http({
                           url: '/user/' + id,
                        method: 'put',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          destroy: function (id) {
            return $http({
                           url: '/user/' + id,
                        method: 'delete',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        schedule: {
          find: function () {
            return $http({
                           url: '/schedule',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          findOne: function (id) {
            return $http({
                           url: '/schedule/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }

        },
        help: {
          find: function () {
            return $http({
                           url: '/help',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          update: function (id, data) {
            return $http({
                           url: '/help/' + id,
                        method: 'put',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          destroy: function (id) {
            return $http({
                           url: '/help/' + id,
                        method: 'delete',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          answer: function () {
            return $http({
                           url: '/help/answer',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          findOne: function (id) {
            return $http({
                           url: '/help/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        doctor: {
          find: function () {
            return $http({
                           url: '/doctor',
      				          method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
      			});
          },
          findOne: function (id) {
            return $http({
                           url: '/doctor/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          create: function (data) {
            return $http({
                           url: '/doctor',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          update: function (id, data) {
            return $http({
                           url: '/doctor/' + id,
                        method: 'put',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          destroy: function (id) {
            return $http({
                           url: '/doctor/' + id,
                        method: 'delete',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          shift: function (data) {
            return $http({
                           url: '/doctor/shift',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          analysis: function () {
            return $http({
                           url: '/doctor/analysis',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          notif: function () {
            return $http({
                           url: '/doctor/notif',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        nurse: {
          find: function () {
            return $http({
                           url: '/nurse',
      				          method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
      			});
          },
          findOne: function (id) {
            return $http({
                           url: '/nurse/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          create: function (data) {
            return $http({
                           url: '/nurse',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          update: function (id, data) {
            return $http({
                           url: '/nurse/' + id,
                        method: 'put',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          destroy: function (id) {
            return $http({
                           url: '/nurse/' + id,
                        method: 'delete',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          shift: function (data) {
            return $http({
                           url: '/nurse/shift',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        shift: {
          find: function () {
            return $http({
                           url: '/shift',
      				          method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
      			});
          },
          findOne: function (id) {
            return $http({
                           url: '/shift/' + id,
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          create: function (data) {
            return $http({
                           url: '/shift',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          doctor: function (data) {
            return $http({
                           url: '/shift/doctor',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          nurse: function (data) {
            return $http({
                           url: '/shift/nurse',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          search: function (data) {
            return $http({
                           url: '/shift/search',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
        },
        queue: {
          find: function () {
            return $http({
                           url: '/queue',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        user: {
          change: function (data) {
            return $http({
                           url: '/user/change',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          },
          profile: function () {
            return $http({
                           url: '/user/profile',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          }
        },
        notif: {
          create: function (data) {
            return $http({
                           url: '/notification',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
        },
        find: function () {
            return $http({
                           url: '/notification',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
            });
          },
          patient: function (data) {
            return $http({
                           url: '/notification/patient',
                        method: 'post',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity,
                          data: data
            });
          }
      }
      }
    }
  ])
  .constant('IMAGE', {
    PATH: '/images/doctors/',
    SIZE: '2000000', // ~2Mb
    TYPE: [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]
  })
  .constant('IMAGE_RS', {
    PATH: '/images/RS/',
    SIZE: '80000', // ~80 kb
    TYPE: [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]
  })
  .constant('IMAGE_banner', {
    PATH: '/images/RS/',
    SIZE: '600000', // ~600 kb
    TYPE: [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]
  })
  .constant('DAYS', [
      'Minggu',
      'Senin',
      'Selasa',
      'Rabu',
      'Kamis',
      'Jumat',
      'Sabtu'
  ])
  .filter('noImage',
		function () {
			return function (input) {
				if (angular.isUndefined(input) || input === null || input === '') {
					return '-.png';
				}

				return input;
			}
		}
	)
  .filter('toDay', ['DAYS',
		function (DAYS) {
			return function (input) {
				if (angular.isNumber(input)) {
					return DAYS[input];
				}

				return input;
			}
		}
	])
  .filter('toHour', ['$filter',
		function ($filter) {
			return function (input) {
				return $filter('date')(input, 'HH:mm');
			}
		}
	])
  .filter('lowercase', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toLowerCase() + input.substr(1) : '';
    }
  })
  .filter('toDate', ['$filter',
		function ($filter) {
			return function (input) {
				return $filter('date')(input, 'HH:mm:ss dd/MM/yyyy');
			}
		}
	])
  .filter('Date', ['$filter',
    function ($filter) {
      return function (input) {
        return $filter('date')(input, 'yyyy/MM/dd');
      }
    }
  ])
  .directive('uiActiveLink', ['$location',
		function ($location) {
			return {
				restrict: 'A',
				replace: false,
				link: function (scope, elem) {
					scope.$on('$routeChangeSuccess', function () {
						angular.forEach(elem.find('a'), function (a) {
							if (angular.equals(angular.element(a).attr('href'), ('#/' + $location.path().split('/',2)[1]))) {
								angular.element(a).parent().addClass('active');
							} else {
								angular.element(a).parent().removeClass('active');
							};
						});
					});
				}
			}
		}
	])
  .run(['$rootScope', '$window', '$auth', '$location', '$http', 'IMAGE', 'IMAGE_RS', 'IMAGE_banner', 'DAYS',
    function ($rootScope, $window, $auth, $location, $http, IMAGE, IMAGE_RS, IMAGE_banner, DAYS) {
      $rootScope.$on('$routeChangeStart', function () {
        $window.scrollTo(0, 0);
        if ($auth.isAuthenticated() && angular.isDefined($window.localStorage.admin)) {
          $rootScope.USER = JSON.parse($window.localStorage.admin);
          $rootScope.IMAGE = IMAGE;
          $rootScope.IMAGE_RS = IMAGE_RS;
          $rootScope.IMAGE_banner = IMAGE_banner;
          $rootScope.DAYS = DAYS;
          $http({
                        url: '/hospital',
                        method: 'get',
               withCredentials: true,
                       headers: {'Content-Type': undefined},
              transformRequest: angular.identity
      }).then(
        function (rs) {
          $rootScope.hospital = rs.data[0];
        });
        } else {
          $location.path('/login');
        }
      });
      $('.logout').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $http.get('/auth/logout')
          .then(function (result) {
            $location.path('/login');
          });
      });
    }
  ]);
